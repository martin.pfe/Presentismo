﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Dapper;

namespace Presentismo_2015
{
    public partial class Modificacion_de_personal : myCustomForm
    {
        public string DNI = String.Empty;
        private string employeeDNI = String.Empty;
        Employee logedEmployee;
        public Modificacion_de_personal(string DNI,Employee actual)
        {
            InitializeComponent();
            logedEmployee = actual;
            employeeDNI = DNI;
            int accessType = logedEmployee.accessType;

            if (accessType != 1)
            {
                labelAccessType.Visible = false;
                employeeTypeCb.Visible = false;
                redLabelAccessType.Visible = false;
            }
            //MessageBox.Show(_DNI);
            try
            {
                //string connectionString = "Server=tcp:oer8py276v.database.windows.net,1433;Database=Presentismo;User ID=proyecto2015@oer8py276v;Password=Colegiojuan23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                string select = "SELECT * FROM VW_Employees WITH(NOLOCK) WHERE DNI = '" + employeeDNI + "'";
                //Connection c = new Connection();
                var cnn = new SqlConnection(Conection.connectionString);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(select, Conection.connectionString); //c.con is the connection string

                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
                DataTable dt = new DataTable();
                dataAdapter.Fill(dt);
                
                string name             = dt.Rows[0]["name"].ToString();
                string lastname         = dt.Rows[0]["lastname"].ToString();
                string birthDate        = dt.Rows[0]["birthDate"].ToString();
                string birthCountry     = dt.Rows[0]["birthCountry"].ToString();
                string homePhone        = dt.Rows[0]["homePhone"].ToString();
                string cellPhone        = dt.Rows[0]["cellPhone"].ToString();
                string personalMail     = dt.Rows[0]["personalMail"].ToString();
                string schoolMail       = dt.Rows[0]["schoolMail"].ToString();
                string employeeAddress  = dt.Rows[0]["employeeAddress"].ToString();
                string employeeLocality = dt.Rows[0]["employeeLocality"].ToString();
                string employeeState    = dt.Rows[0]["employeeState"].ToString();
                string employeeZipCode  = dt.Rows[0]["employeeZipCode"].ToString();
                string legajo           = dt.Rows[0]["legajo"].ToString();
                int accessTypeID        = Convert.ToInt32(dt.Rows[0]["accessType"].ToString());

                accessTypeID = accessTypeID - 1;

                DNITxt.Text              = employeeDNI;
                nameTxt.Text             = name;
                lastNameTxt.Text         = lastname;
                dateDTP.Text             = birthDate;
                countryTxt.Text          = birthCountry;
                phoneTxt.Text            = homePhone;
                cellPhoneTxt.Text        = cellPhone;
                schoolMailTxt.Text       = schoolMail;
                personalMailTxt.Text     = personalMail;
                addressTxt.Text          = employeeAddress;
                cityTxt.Text             = employeeLocality;
                stateTxt.Text            = employeeState;
                zipCodeTxt.Text          = employeeZipCode;
                legajoTxt.Text           = legajo;
                employeeTypeCb.SelectedIndex = accessTypeID;         
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        
        private void Label38_Click(object sender, EventArgs e)
        {

        }

        private void CerrarBT_Click(object sender, EventArgs e)
        {
            
        }

        private void legajoTxt_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(legajoTxt.Text, "  ^ [0-9]"))
            {
                legajoTxt.Text = "";
            }
        }
        private void legajoTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void DNITxt_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(DNITxt.Text, "  ^ [0-9]"))
            {
                DNITxt.Text = "";
            }
        }
        private void DNITxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void zipCodeTxt_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(zipCodeTxt.Text, "  ^ [0-9]"))
            {
                zipCodeTxt.Text = "";
            }
        }
        private void zipCodeTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void AgregarBT_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Seguro que desea editar el empleado?", "Edición de empleado",
            MessageBoxButtons.YesNo, MessageBoxIcon.Question,
            MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
            {
                string DNI          = DNITxt.Text;
                string name         = nameTxt.Text;
                string lastName     = lastNameTxt.Text;
                string dateEmployee = dateDTP.Value.Date.ToString();
                string address      = addressTxt.Text;
                string city         = cityTxt.Text;
                string stateID      = stateTxt.Text;
                string zipCode      = zipCodeTxt.Text;
                string country      = countryTxt.Text;
                string phone        = phoneTxt.Text;
                string cellPhone    = cellPhoneTxt.Text;
                string schoolMail   = schoolMailTxt.Text;
                string personalMail = personalMailTxt.Text;
                string legajo       = legajoTxt.Text;

                //IPV 2016-02-09: Levantamos el employeeID del usuario logueado en el sistema
                int userID          = logedEmployee.employeeID;

                string ingressDateStr = ingressDate.Text;

                int employeeTypeID = 0;

                if (logedEmployee.accessType == 1)
                {
                    employeeTypeID = employeeTypeCb.SelectedIndex;
                    //Le sumamos un uno porque el index empieza en 0, villero pero rápido
                    employeeTypeID = employeeTypeID + 1;
                }
                else
                {
                    //Mandamos el param en 0 y en SQL le dejamos el mismo que tenía antes...
                    employeeTypeID = 0;
                }

                //string connectionString = null;
                //connectionString = "Server=tcp:oer8py276v.database.windows.net,1433;Database=Presentismo;User ID=proyecto2015@oer8py276v;Password=Colegiojuan23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                if (DNI != "" && name != "" && lastName != "" && dateEmployee != "" && address != "" && city != "" && stateID != "" && zipCode != "" && country != ""
                    && phone != "" && cellPhone != "" && schoolMail != "" && personalMail != "" && legajo != "")
                {
                    try
                    {
                        var spParams = new DynamicParameters();
                        spParams.Add("@EmployeeDni", DNI, dbType: DbType.String, direction: ParameterDirection.Input, size: 15);
                        spParams.Add("@name", name, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                        spParams.Add("@lastName", lastName, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                        spParams.Add("@birthdate", dateEmployee, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                        spParams.Add("@birthCountry", country, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                        spParams.Add("@homePhone", phone, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                        spParams.Add("@cellPhone", cellPhone, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                        spParams.Add("@personalMail", personalMail, dbType: DbType.String, direction: ParameterDirection.Input, size: 100);
                        spParams.Add("@schoolMail", schoolMail, dbType: DbType.String, direction: ParameterDirection.Input, size: 100);
                        spParams.Add("@hasLicense", 0, dbType: DbType.Int32, direction: ParameterDirection.Input);
                        spParams.Add("@userID", userID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                        spParams.Add("@employeeAddress", address, dbType: DbType.String, direction: ParameterDirection.Input, size: 100);
                        spParams.Add("@employeeLocality", city, dbType: DbType.String, direction: ParameterDirection.Input, size: 100);
                        spParams.Add("@employeeState", stateID, dbType: DbType.String, direction: ParameterDirection.Input, size: 100);
                        spParams.Add("@employeeZipCode", zipCode, dbType: DbType.String, direction: ParameterDirection.Input, size: 6);
                        spParams.Add("@legajo", legajo, dbType: DbType.String, direction: ParameterDirection.Input, size: 15);
                        spParams.Add("@ingressDate", ingressDateStr, dbType: DbType.String, direction: ParameterDirection.Input, size: 10);
                        spParams.Add("@accessType", employeeTypeID, dbType: DbType.Int32, direction: ParameterDirection.Input);

                        spParams.Add("@errorDesc", dbType: DbType.String, direction: ParameterDirection.Output, size: 500);
                        spParams.Add("@RETURN_VALUE", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                                
                        using (var sqlConnection = new SqlConnection(Conection.connectionString))
                        {

                            sqlConnection.Open();

                            sqlConnection.Execute("SP_Employee_Edit", spParams, commandType: CommandType.StoredProcedure);
                            string errorDesc = spParams.Get<string>("@errorDesc");
                            int RC = spParams.Get<int>("@RETURN_VALUE");

                            sqlConnection.Close();
                            if (RC != 1)
                            {
                                MessageBox.Show(String.Format("Error: {0} - Error N° :{1}", errorDesc, RC.ToString()));
                            }
                            else
                            {
                                MessageBox.Show("El empleado ha sido editado satisfactoriamente!");
                                BuscarPersonal form = new BuscarPersonal(logedEmployee);
                                this.Close();
                                form.Show();
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
            else{
                MessageBox.Show("Por favor complete todos los campos.");
            }

        }

        private void Label16_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void CerrarBT_Click_1(object sender, EventArgs e)
        {
            BuscarPersonal form = new BuscarPersonal(logedEmployee);
            this.Close();
            form.Show();
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }
    }
}
