﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;


namespace Presentismo_2015.IngresoFirma
{
    public partial class ingresoFirma : myCustomForm
    {
        private String folderPath = ConfigurationManager.AppSettings["signaturesPath"];

        private List<Point> stroke = null;
        private List<List<Point>> Strokes = new List<List<Point>>();
        private int employeeID { get; set; }
        private bool isValidEmployee { get; set; }
        private String folderDate { get; set; }

        private String fileToSave { get; set; }

        public ingresoFirma()
        {
            InitializeComponent();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left && isValidEmployee)
            {
                stroke = new List<Point>();
                stroke.Add(new Point(e.X, e.Y));
            }
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left && isValidEmployee)
            {
                stroke.Add(new Point(e.X, e.Y));
                if (stroke.Count == 2)
                {
                    Strokes.Add(stroke);
                }
                panel1.Refresh();
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left && isValidEmployee)
            {
                stroke = null;
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            foreach (List<Point> curStroke in Strokes)
            {
                e.Graphics.DrawLines(Pens.Black, curStroke.ToArray());
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Strokes.Clear();
            panel1.Refresh();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void saveSign_Click(object sender, EventArgs e)
        {
            try
            {
                Bitmap bmp = new Bitmap(panel1.Width, panel1.Height);
                panel1.DrawToBitmap(bmp, panel1.ClientRectangle);

                fileToSave = String.Format(@"{0}\{1}\{2}.jpg", folderPath, folderDate, employeeID);

                String folderToCreate = String.Format(@"{0}\{1}", folderPath, folderDate);
                bool exists = System.IO.Directory.Exists(folderToCreate);

                if (System.IO.Directory.Exists(folderToCreate))
                    System.IO.Directory.CreateDirectory(folderToCreate);

                //MessageBox.Show(fileToSave);
                bmp.Save(fileToSave, System.Drawing.Imaging.ImageFormat.Jpeg);

                try
                {
                    //  MessageBox.Show("Connection Open ! ");
                    var spParams = new DynamicParameters();
                    spParams.Add("@employeeID", employeeID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    spParams.Add("@errorDesc", dbType: DbType.String, direction: ParameterDirection.Output, size: 500);

                    spParams.Add("@RETURN_VALUE", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                    using (var sqlConnection = new SqlConnection(Conection.connectionString))
                    {

                        sqlConnection.Open();

                        sqlConnection.Execute("SP_Signature_Save", spParams, commandType: CommandType.StoredProcedure);
                        int RC = spParams.Get<int>("@RETURN_VALUE");
                        string errorDesc = spParams.Get<string>("@errorDesc");

                        //  if (RC == 1)
                        //{
                        MessageBox.Show("Firma guardada correctamente. Puede continuar.");
                        /*}
                        else
                            MessageBox.Show(String.Format("Error: {0} - Error N° :{1}", errorDesc, RC.ToString()));
-+98-+*/
                        sqlConnection.Close();
                    }

                }
                catch
                {
                    MessageBox.Show("Ocurrió un error conectandose a la base de datos.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un error guardando la firma.");
            }
            finally
            {
                //Cuando terminamos todo. Hay que limpiar todo!
                txt_user.Text = String.Empty;
                panel1.BackColor = Color.Gray;
                isValidEmployee = false;
                Strokes.Clear();
                panel1.Refresh();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int employeeDNI = 0;
            if (!Int32.TryParse(txt_user.Text, out employeeDNI) || txt_user.Text.Length != 8)
            {
                MessageBox.Show("El DNI ingresado es incorrecto. Recuerdo incluir solo los números.");
                return;
            }
            //String userDni = txt_user.Text.ToString();

            try
            {
                //  MessageBox.Show("Connection Open ! ");
                var spParams = new DynamicParameters();
                spParams.Add("@employeeDNI", employeeDNI, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@employeeID", dbType: DbType.Int32, direction: ParameterDirection.Output);
                spParams.Add("@folderDate", dbType: DbType.String, direction: ParameterDirection.Output, size: 10);
                spParams.Add("@errorDesc", dbType: DbType.String, direction: ParameterDirection.Output, size: 500);

                spParams.Add("@RETURN_VALUE", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                /*String parameterType;

                foreach (string parameter in spParams.ParameterNames)
                {
                    MessageBox.Show(parameter.ToString());
                    parameterType = parameter.GetType().ToString();
                    MessageBox.Show(spParams.Get<string>("@" + parameter.ToString()).ToString());
                }*/
                using (var sqlConnection = new SqlConnection(Conection.connectionString))
                {

                    sqlConnection.Open();

                    sqlConnection.Execute("SP_Employee_GetData", spParams, commandType: CommandType.StoredProcedure);
                    int RC = spParams.Get<int>("@RETURN_VALUE");
                    string errorDesc = spParams.Get<string>("@errorDesc");

                    if (RC == 1)
                    {
                        MessageBox.Show("DNI correcto. Por favor ingrese la firma para completar el proceso.");
                        employeeID = spParams.Get<int>("@employeeID");
                        folderDate = spParams.Get<String>("@folderDate");
                        isValidEmployee = true;
                        panel1.BackColor = Color.White;
                    }
                    else
                        MessageBox.Show(String.Format("Error: {0} - Error N° :{1}", errorDesc, RC.ToString()));

                    sqlConnection.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un error conectandose con la base de datos.");
            }

        }

        private void ingresoFirma_Load(object sender, EventArgs e)
        {
            isValidEmployee = false;
        }

        private void panel1_MouseEnter(object sender, EventArgs e)
        {
        }

        private void btn_Login_Click(object sender, EventArgs e)
        {
            Login loginFrm = new Login();
            loginFrm.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(Conection.connectionString))
                {
                    MessageBox.Show("Conexión abierta");
                }
            }
            catch
            {
                MessageBox.Show("No se pudo establecer la conexión con la DB.");

            }
        }
    }
}
