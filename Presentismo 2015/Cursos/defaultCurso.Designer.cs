﻿namespace Presentismo_2015
{
    partial class defaultCurso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows myCustomForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_Add = new System.Windows.Forms.Button();
            this.btn_Edit = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_courseNumber = new System.Windows.Forms.TextBox();
            this.txt_courseLetter = new System.Windows.Forms.TextBox();
            this.cmb_turnID = new System.Windows.Forms.ComboBox();
            this.txt_morningClassRoom = new System.Windows.Forms.TextBox();
            this.txt_afternoonClassRoom = new System.Windows.Forms.TextBox();
            this.cmb_dipregep = new System.Windows.Forms.ComboBox();
            this.btn_Search = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(33, 68);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(697, 408);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // btn_Add
            // 
            this.btn_Add.BackColor = System.Drawing.Color.White;
            this.btn_Add.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Add.Image = global::Presentismo_2015.Properties.Resources.add;
            this.btn_Add.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_Add.Location = new System.Drawing.Point(666, 482);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.Size = new System.Drawing.Size(64, 60);
            this.btn_Add.TabIndex = 2;
            this.btn_Add.Text = "Agregar";
            this.btn_Add.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_Add.UseVisualStyleBackColor = false;
            this.btn_Add.Click += new System.EventHandler(this.btn_Add_Click);
            // 
            // btn_Edit
            // 
            this.btn_Edit.BackColor = System.Drawing.Color.White;
            this.btn_Edit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Edit.Image = global::Presentismo_2015.Properties.Resources.pencil;
            this.btn_Edit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_Edit.Location = new System.Drawing.Point(596, 482);
            this.btn_Edit.Name = "btn_Edit";
            this.btn_Edit.Size = new System.Drawing.Size(64, 60);
            this.btn_Edit.TabIndex = 3;
            this.btn_Edit.Text = "Edición";
            this.btn_Edit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_Edit.UseVisualStyleBackColor = false;
            this.btn_Edit.Click += new System.EventHandler(this.btn_Edit_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.BackgroundImage = global::Presentismo_2015.Properties.Resources.door_out;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button3.Location = new System.Drawing.Point(33, 482);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(56, 65);
            this.button3.TabIndex = 314;
            this.button3.Text = "Cerrar";
            this.button3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 315;
            this.label1.Text = "Curso";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(112, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 317;
            this.label2.Text = "División";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(192, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 319;
            this.label3.Text = "Turno";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(297, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 321;
            this.label4.Text = "Aula mañana";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(393, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 323;
            this.label5.Text = "Aula tarde";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(466, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 325;
            this.label6.Text = "DIPREGEP";
            // 
            // txt_courseNumber
            // 
            this.txt_courseNumber.Location = new System.Drawing.Point(33, 42);
            this.txt_courseNumber.Name = "txt_courseNumber";
            this.txt_courseNumber.Size = new System.Drawing.Size(56, 20);
            this.txt_courseNumber.TabIndex = 326;
            this.txt_courseNumber.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            // 
            // txt_courseLetter
            // 
            this.txt_courseLetter.Location = new System.Drawing.Point(115, 43);
            this.txt_courseLetter.MaxLength = 2;
            this.txt_courseLetter.Name = "txt_courseLetter";
            this.txt_courseLetter.Size = new System.Drawing.Size(56, 20);
            this.txt_courseLetter.TabIndex = 327;
            // 
            // cmb_turnID
            // 
            this.cmb_turnID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_turnID.FormattingEnabled = true;
            this.cmb_turnID.Location = new System.Drawing.Point(195, 42);
            this.cmb_turnID.Name = "cmb_turnID";
            this.cmb_turnID.Size = new System.Drawing.Size(88, 21);
            this.cmb_turnID.TabIndex = 328;
            // 
            // txt_morningClassRoom
            // 
            this.txt_morningClassRoom.Location = new System.Drawing.Point(300, 43);
            this.txt_morningClassRoom.Name = "txt_morningClassRoom";
            this.txt_morningClassRoom.Size = new System.Drawing.Size(56, 20);
            this.txt_morningClassRoom.TabIndex = 329;
            // 
            // txt_afternoonClassRoom
            // 
            this.txt_afternoonClassRoom.Location = new System.Drawing.Point(396, 41);
            this.txt_afternoonClassRoom.Name = "txt_afternoonClassRoom";
            this.txt_afternoonClassRoom.Size = new System.Drawing.Size(56, 20);
            this.txt_afternoonClassRoom.TabIndex = 331;
            // 
            // cmb_dipregep
            // 
            this.cmb_dipregep.BackColor = System.Drawing.Color.White;
            this.cmb_dipregep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_dipregep.FormattingEnabled = true;
            this.cmb_dipregep.Location = new System.Drawing.Point(469, 41);
            this.cmb_dipregep.Name = "cmb_dipregep";
            this.cmb_dipregep.Size = new System.Drawing.Size(120, 21);
            this.cmb_dipregep.TabIndex = 332;
            this.cmb_dipregep.SelectedIndexChanged += new System.EventHandler(this.cmb_dipregep_SelectedIndexChanged);
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(631, 37);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(98, 23);
            this.btn_Search.TabIndex = 333;
            this.btn_Search.Text = "Buscar";
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // defaultCurso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(764, 569);
            this.Controls.Add(this.btn_Search);
            this.Controls.Add(this.cmb_dipregep);
            this.Controls.Add(this.txt_afternoonClassRoom);
            this.Controls.Add(this.txt_morningClassRoom);
            this.Controls.Add(this.cmb_turnID);
            this.Controls.Add(this.txt_courseLetter);
            this.Controls.Add(this.txt_courseNumber);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btn_Edit);
            this.Controls.Add(this.btn_Add);
            this.Controls.Add(this.dataGridView1);
            this.Name = "defaultCurso";
            this.Text = "Cursos";
            this.Load += new System.EventHandler(this.defaultCurso_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_Add;
        private System.Windows.Forms.Button btn_Edit;
        internal System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_courseNumber;
        private System.Windows.Forms.TextBox txt_courseLetter;
        private System.Windows.Forms.ComboBox cmb_turnID;
        private System.Windows.Forms.TextBox txt_morningClassRoom;
        private System.Windows.Forms.TextBox txt_afternoonClassRoom;
        internal System.Windows.Forms.ComboBox cmb_dipregep;
        private System.Windows.Forms.Button btn_Search;
    }
}