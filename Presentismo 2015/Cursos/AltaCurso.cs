﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Dapper;
using System.Data.SqlClient;

namespace Presentismo_2015
{
    public partial class AltaCurso : myCustomForm
    {
        Employee formEmployee;
        public AltaCurso(Employee logedEmployee)
        {
            InitializeComponent();
            formEmployee = logedEmployee;
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void Label5_Click(object sender, EventArgs e)
        {

        }

        private void AgregarBT_Click(object sender, EventArgs e)
        {
            try
            {
                int courseNumber = 0;
                int studentsQty = 0;

                if (!Int32.TryParse(txt_courseNumber.Text, out courseNumber))
                {
                    MessageBox.Show("El número de curso debe ser un valor numérico.");
                    return;
                }

                
                if (!Int32.TryParse(txt_studentsQty.Text, out studentsQty))
                {
                    MessageBox.Show("La cantidad de estudiantes debe ser un valor numérico.");
                    return;
                }

                int morningClassroom = 0;
                if (!Int32.TryParse(txt_morningClassroom.Text, out morningClassroom))
                {
                    MessageBox.Show("El aula debe ser un valor numérico.");
                    return;
                }

                int afternoonClassroom = 0;

               
                if (!Int32.TryParse(txt_afternoonClassroom.Text, out afternoonClassroom))
                {
                    MessageBox.Show("El aula debe ser un valor numérico.");
                    return;
                }

                courseNumber            = int.Parse(txt_courseNumber.Text);
                String courseLetter     = txt_courseLetter.Text;

                if (courseLetter == "")
                {
                    MessageBox.Show("La división es requerida.");
                    return;
                }

                studentsQty             = int.Parse(txt_studentsQty.Text);
                
                morningClassroom        = int.Parse(txt_morningClassroom.Text);
                afternoonClassroom      = int.Parse(txt_afternoonClassroom.Text);
                int dipregep            = int.Parse(cmb_dipregep.SelectedIndex.ToString());
                int turnID              = int.Parse(cmb_turnID.SelectedIndex.ToString());

                if (turnID == 0)
                {
                    MessageBox.Show("El turno es requerido.");
                    return;
                }

                if (dipregep == 0)
                {
                    MessageBox.Show("El DIPREGEP es requerido.");
                    return;
                }
                var spParams = new DynamicParameters();
                spParams.Add("@courseNumber", courseNumber, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@courseLetter", courseLetter, dbType: DbType.String, direction: ParameterDirection.Input, size: 2);
                spParams.Add("@studentsQty", studentsQty, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@morningClassroom", morningClassroom, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@afternoonClassroom", afternoonClassroom, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@dipregep", dipregep, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@turnID", turnID, dbType: DbType.Int32, direction: ParameterDirection.Input);

                spParams.Add("@errorDesc", dbType: DbType.String, direction: ParameterDirection.Output, size: 500);

                spParams.Add("@RETURN_VALUE", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                using (var sqlConnection = new SqlConnection(Conection.connectionString))
                {

                    sqlConnection.Open();

                    sqlConnection.Execute("SP_Courses_Add", spParams, commandType: CommandType.StoredProcedure);
                    int RC = spParams.Get<int>("@RETURN_VALUE");
                    string errorDesc = spParams.Get<string>("@errorDesc");

                    if (RC == 1)
                    {
                        MessageBox.Show("El curso ha sido ha agregado correctamente.");
                    }
                    else
                        MessageBox.Show(String.Format("Error: {0} - Error N° :{1}", errorDesc, RC.ToString()));

                    sqlConnection.Close();
                }
            }

            catch (Exception ex) 
            {
                MessageBox.Show(ex.ToString());
            }
            //char courseLetter = txt
        }

        private void AltaCurso_Load(object sender, EventArgs e)
        {

            //IPV: en mi solución falta ComboboxItem(); descomentar al subir
            ComboboxItem item = new ComboboxItem();

            item.Text = "[Seleccione]";
            item.Value = 0;
            item.Index = 0;

            cmb_dipregep.Items.Add(item);

            item.Text = "Secundaria Basica";
            item.Value = 1;
            item.Index = 1;

            cmb_dipregep.Items.Add(item);

            item.Text = "Secundaria Tecnica";
            item.Value = 2;
            item.Index = 2;


            cmb_dipregep.Items.Add(item);

            cmb_dipregep.SelectedText = "[Selecccione]";
            cmb_dipregep.SelectedIndex = 0;

            ComboboxItem turn = new ComboboxItem();

            turn.Text = "[Seleccione]";
            turn.Value = 0;
            turn.Index = 0;

            cmb_turnID.Items.Add(turn);

            turn.Text = "Mañana";
            turn.Value = 1;
            turn.Index = 1;

            cmb_turnID.Items.Add(turn);

            turn.Text = "Tarde";
            turn.Value = 2;
            turn.Index = 2;

            cmb_turnID.Items.Add(turn);

            cmb_turnID.SelectedText = "[Selecccione]";
            cmb_turnID.SelectedIndex = 0;

        }

        private void CerrarBT_Click(object sender, EventArgs e)
        {
            defaultCurso form = new defaultCurso(formEmployee);

            form.Show();
            this.Hide();
        }

        private void cmb_turnID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmb_dipregep_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txt_courseNumber_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txt_courseLetter_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}
