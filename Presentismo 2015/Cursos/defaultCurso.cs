﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Dapper;
using System.Data.SqlClient;

namespace Presentismo_2015
{
    public partial class defaultCurso : myCustomForm
    {

        Employee formEmployee;
        public defaultCurso(Employee logedEmployee)
        {
            InitializeComponent();
            formEmployee = logedEmployee;
        }
        String cnn = Conection.connectionString;
        public IEnumerable<Course> Course { get; set; }  
        private void defaultCurso_Load(object sender, EventArgs e)
        {
            ComboboxItem item = new ComboboxItem();

            item.Text = "[Seleccione]";
            item.Value = 0;
            item.Index = 0;

            cmb_dipregep.Items.Add(item);

            item.Text = "Secundaria Basica";
            item.Value = 1;
            item.Index = 1;

            cmb_dipregep.Items.Add(item);

            item.Text = "Secundaria Tecnica";
            item.Value = 2;
            item.Index = 2;

            cmb_dipregep.Items.Add(item);


            ComboboxItem turn = new ComboboxItem();

            turn.Text = "[Seleccione]";
            turn.Value = 0;
            item.Index = 0;

            cmb_turnID.Items.Add(turn);

            turn.Text = "Mañana";
            turn.Value = 1;
            item.Index = 1;

            cmb_turnID.Items.Add(turn);

            turn.Text = "Tarde";
            turn.Value = 2;
            item.Index = 2;

            cmb_turnID.Items.Add(turn);


            try
            {
                doSearch();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        //private void btn_Edit_Click(object sender, System.EventArgs e)
        //{
        //    int courseID = Convert.ToInt32(textBox1.Text.ToString());

                
        //}  

        private void btn_Edit_Click(object sender, System.EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count != 1)
            {
                DataGridViewRow row = dataGridView1.Rows[dataGridView1.SelectedCells[0].RowIndex];

                Modificacion_de_curso cursoEdit = new Modificacion_de_curso(formEmployee, Int32.Parse(row.Cells["courseID"].Value.ToString()), this);
                cursoEdit.Show();
            }
            else
            {
                MessageBox.Show("Para editar una asignatura debe tener seleccionada solo una.");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        internal void doSearch()
        {
            int courseNumber = 0, turnID = 0, morningClassRoom = 0, afternoonClassRoom = 0, dipregep = 0;
            String courseLetter = String.Empty;

            if (txt_courseLetter.Text != String.Empty)
                courseLetter = txt_courseLetter.Text;

            if (txt_courseNumber.Text != String.Empty)
            {
                if (!Int32.TryParse(txt_courseNumber.Text, out courseNumber))
                {
                    MessageBox.Show("El número de curso debe ser un valor numérico.");
                    return;
                }
            }

            if (!Int32.TryParse(cmb_turnID.SelectedIndex.ToString(), out turnID))
            {
                MessageBox.Show("Seleccione el turno.");
                return;
            }

            if (txt_morningClassRoom.Text != String.Empty)
            {
                if (!Int32.TryParse(txt_morningClassRoom.Text, out morningClassRoom))
                {
                    MessageBox.Show("El aula de la mañana debe ser un valor numérico.");
                    return;
                }
            }

            if (txt_afternoonClassRoom.Text != String.Empty)
            {
                if (!Int32.TryParse(txt_afternoonClassRoom.Text, out courseNumber))
                {
                    MessageBox.Show("El aula de la tarde debe ser un valor numérico.");
                    return;
                }
            }

            if (!Int32.TryParse(cmb_dipregep.SelectedIndex.ToString(), out dipregep))
            {
                MessageBox.Show("Seleccione el dipregep.");
                return;
            }

            var spParams = new DynamicParameters();
            spParams.Add("@courseNumber", courseNumber, dbType: DbType.Int32, direction: ParameterDirection.Input);
            spParams.Add("@courseLetter", courseLetter, dbType: DbType.String, direction: ParameterDirection.Input, size: 2);
            spParams.Add("@turnID", turnID, dbType: DbType.Int32, direction: ParameterDirection.Input);
            spParams.Add("@morningClassRoom", morningClassRoom, dbType: DbType.Int32, direction: ParameterDirection.Input);
            spParams.Add("@afternoonClassRoom", afternoonClassRoom, dbType: DbType.Int32, direction: ParameterDirection.Input);
            spParams.Add("@dipregep", dipregep, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (var sqlConnection = new SqlConnection(Conection.connectionString))
            {
                sqlConnection.Open();
                IDataReader rd;
                DataTable dt = new DataTable();

                rd = sqlConnection.ExecuteReader("SP_Courses_Search", spParams, commandType: CommandType.StoredProcedure);

                dt.Load(rd);

                dataGridView1.AutoGenerateColumns = true;
                dataGridView1.DataSource = dt;
                dataGridView1.Refresh();
                dataGridView1.ReadOnly = true;
                dataGridView1.AllowUserToAddRows = false;
                dataGridView1.AllowUserToDeleteRows = false;

            }
        
        }

        private void btn_Principal_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            doSearch();
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            AltaCurso frm = new AltaCurso(formEmployee);
            frm.Show();
            this.Hide();
        }

        private void cmb_dipregep_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
