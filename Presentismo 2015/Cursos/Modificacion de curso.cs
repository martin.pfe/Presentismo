﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Dapper;

namespace Presentismo_2015
{
    public partial class Modificacion_de_curso : myCustomForm
    {
        Employee formEmployee;
        int courseID = 0;
        defaultCurso parentForm;
        public Modificacion_de_curso(Employee logedEmployee, int _courseID, defaultCurso _parentForm)
        {
            InitializeComponent();
            formEmployee = logedEmployee;
            courseID = _courseID;
            parentForm = _parentForm;
        }

        private void Modificacion_de_curso_Load(object sender, EventArgs e)
        {
            ComboBoxDipregep item = new ComboBoxDipregep();

            item.Text = "[Seleccione]";
            item.Value = 0;
            item.Index = 0;

            cmb_dipregep.Items.Add(item);

            item.Text = "Secundaria Basica";
            item.Value = 1;
            item.Index = 1;

            cmb_dipregep.Items.Add(item);

            item.Text = "Secundaria Tecnica";
            item.Value = 2;
            item.Index = 2;


            cmb_dipregep.Items.Add(item);

            cmb_dipregep.SelectedText = "[Selecccione]";
            cmb_dipregep.SelectedIndex = 0;

            ComboBoxDipregep turn = new ComboBoxDipregep();

            turn.Text = "[Seleccione]";
            turn.Value = 0;
            turn.Index = 0;

            cmb_turnID.Items.Add(turn);

            turn.Text = "Mañana";
            turn.Value = 1;
            turn.Index = 1;

            cmb_turnID.Items.Add(turn);

            turn.Text = "Tarde";
            turn.Value = 2;
            turn.Index = 2;

            cmb_turnID.Items.Add(turn);

            cmb_turnID.SelectedText = "[Selecccione]";
            cmb_turnID.SelectedIndex = 0;

            try
            {
                string conexion = Conection.connectionString;
                //string connectionString = "Server=tcp:oer8py276v.database.windows.net,1433;Database=Presentismo;User ID=proyecto2015@oer8py276v;Password=Colegiojuan23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                string select = "SELECT courseID, courseNumber, courseLetter, turnID, morningClassRoom, afternoonClassRoom, studentsQTY, dipregep FROM TB_Courses WITH(NOLOCK) WHERE courseID = " + courseID ;
                //Connection c = new Connection();
                var cnn = new SqlConnection(conexion);
                cnn.Open();
                SqlCommand command = new SqlCommand(select, cnn);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();

                txt_courseNumber.Text = reader[1].ToString();
                txt_courseLetter.Text = reader[2].ToString();
                cmb_turnID.SelectedIndex = Convert.ToInt32(reader[3]);
                txt_studentsQty.Text = reader[6].ToString();
                txt_morningClassroom.Text = reader[4].ToString();
                txt_afternoonClassroom.Text = reader[5].ToString();
                cmb_dipregep.SelectedIndex = Convert.ToInt32(reader[7]);
                reader.Close();
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void AgregarBT_Click(object sender, EventArgs e)
        {
            try
            {
                
                int courseNumber = int.Parse(txt_courseNumber.Text);
                String courseLetter = txt_courseLetter.Text;
                int studentsQty = int.Parse(txt_studentsQty.Text);
                int morningClassroom = int.Parse(txt_morningClassroom.Text);
                int afternoonClassroom = int.Parse(txt_afternoonClassroom.Text);
                int dipregep = int.Parse(cmb_dipregep.SelectedIndex.ToString());
                int turnID = int.Parse(cmb_turnID.SelectedIndex.ToString());


                var spParams = new DynamicParameters();
                spParams.Add("@courseID", courseID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@courseNumber", courseNumber, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@courseLetter", courseLetter, dbType: DbType.String, direction: ParameterDirection.Input, size: 2);
                spParams.Add("@studentsQty", studentsQty, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@morningClassroom", morningClassroom, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@afternoonClassroom", afternoonClassroom, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@dipregep", dipregep, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@turnID", turnID, dbType: DbType.Int32, direction: ParameterDirection.Input);

                spParams.Add("@errorDesc", dbType: DbType.String, direction: ParameterDirection.Output, size: 500);

                spParams.Add("@RETURN_VALUE", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                using (var sqlConnection = new SqlConnection(Conection.connectionString))
                {

                    sqlConnection.Open();

                    sqlConnection.Execute("SP_Courses_Edit", spParams, commandType: CommandType.StoredProcedure);
                    int RC = spParams.Get<int>("@RETURN_VALUE");
                    string errorDesc = spParams.Get<string>("@errorDesc");
                    sqlConnection.Close();

                    if (RC == 1)
                    {
                        MessageBox.Show("El curso ha sido editado correctamente");
                        this.Hide();

                        parentForm.doSearch();
                    }
                    else
                        MessageBox.Show(String.Format("Error: {0} - Error N° :{1}", errorDesc, RC.ToString()));

                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error conectadose a la base de datos.");
            }
        }

        private void CerrarBT_Click(object sender, EventArgs e)
        {
            //defaultCurso curso = new defaultCurso(formEmployee);
            //curso.Show();
            this.Hide();
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void cmb_turnID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
