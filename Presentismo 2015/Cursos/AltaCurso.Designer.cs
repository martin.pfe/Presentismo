﻿namespace Presentismo_2015
{
    partial class AltaCurso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows myCustomForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cmb_turnID = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_afternoonClassroom = new System.Windows.Forms.TextBox();
            this.txt_morningClassroom = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CerrarBT = new System.Windows.Forms.Button();
            this.txt_studentsQty = new System.Windows.Forms.TextBox();
            this.AgregarBT = new System.Windows.Forms.Button();
            this.Label29 = new System.Windows.Forms.Label();
            this.Label28 = new System.Windows.Forms.Label();
            this.cmb_dipregep = new System.Windows.Forms.ComboBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.DipreLBL = new System.Windows.Forms.Label();
            this.txt_courseLetter = new System.Windows.Forms.TextBox();
            this.txt_courseNumber = new System.Windows.Forms.TextBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBox1
            // 
            this.GroupBox1.BackColor = System.Drawing.Color.Honeydew;
            this.GroupBox1.Controls.Add(this.label11);
            this.GroupBox1.Controls.Add(this.label10);
            this.GroupBox1.Controls.Add(this.cmb_turnID);
            this.GroupBox1.Controls.Add(this.label9);
            this.GroupBox1.Controls.Add(this.label7);
            this.GroupBox1.Controls.Add(this.label8);
            this.GroupBox1.Controls.Add(this.txt_afternoonClassroom);
            this.GroupBox1.Controls.Add(this.txt_morningClassroom);
            this.GroupBox1.Controls.Add(this.label6);
            this.GroupBox1.Controls.Add(this.label2);
            this.GroupBox1.Controls.Add(this.CerrarBT);
            this.GroupBox1.Controls.Add(this.txt_studentsQty);
            this.GroupBox1.Controls.Add(this.AgregarBT);
            this.GroupBox1.Controls.Add(this.Label29);
            this.GroupBox1.Controls.Add(this.Label28);
            this.GroupBox1.Controls.Add(this.cmb_dipregep);
            this.GroupBox1.Controls.Add(this.Label4);
            this.GroupBox1.Controls.Add(this.DipreLBL);
            this.GroupBox1.Controls.Add(this.txt_courseLetter);
            this.GroupBox1.Controls.Add(this.txt_courseNumber);
            this.GroupBox1.Controls.Add(this.Label5);
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Location = new System.Drawing.Point(25, 23);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(351, 309);
            this.GroupBox1.TabIndex = 89;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Ingrese los datos del curso";
            this.GroupBox1.Enter += new System.EventHandler(this.GroupBox1_Enter);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(298, 200);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 18);
            this.label11.TabIndex = 106;
            this.label11.Text = "*";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(292, 85);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(16, 18);
            this.label10.TabIndex = 105;
            this.label10.Text = "*";
            // 
            // cmb_turnID
            // 
            this.cmb_turnID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_turnID.FormattingEnabled = true;
            this.cmb_turnID.Location = new System.Drawing.Point(172, 81);
            this.cmb_turnID.Name = "cmb_turnID";
            this.cmb_turnID.Size = new System.Drawing.Size(120, 21);
            this.cmb_turnID.TabIndex = 104;
            this.cmb_turnID.SelectedIndexChanged += new System.EventHandler(this.cmb_turnID_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label9.Location = new System.Drawing.Point(20, 89);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 17);
            this.label9.TabIndex = 103;
            this.label9.Text = "Turno";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(223, 135);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 18);
            this.label7.TabIndex = 101;
            this.label7.Text = "*";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(223, 162);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 18);
            this.label8.TabIndex = 102;
            this.label8.Text = "*";
            // 
            // txt_afternoonClassroom
            // 
            this.txt_afternoonClassroom.Location = new System.Drawing.Point(172, 162);
            this.txt_afternoonClassroom.MaxLength = 3;
            this.txt_afternoonClassroom.Name = "txt_afternoonClassroom";
            this.txt_afternoonClassroom.Size = new System.Drawing.Size(28, 20);
            this.txt_afternoonClassroom.TabIndex = 100;
            this.txt_afternoonClassroom.Tag = "";
            // 
            // txt_morningClassroom
            // 
            this.txt_morningClassroom.Location = new System.Drawing.Point(172, 136);
            this.txt_morningClassroom.MaxLength = 3;
            this.txt_morningClassroom.Name = "txt_morningClassroom";
            this.txt_morningClassroom.Size = new System.Drawing.Size(28, 20);
            this.txt_morningClassroom.TabIndex = 99;
            this.txt_morningClassroom.Tag = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label6.Location = new System.Drawing.Point(20, 168);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 17);
            this.label6.TabIndex = 98;
            this.label6.Text = "Aula a la tarde";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label2.Location = new System.Drawing.Point(20, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 17);
            this.label2.TabIndex = 97;
            this.label2.Text = "Aula a la mañana";
            // 
            // CerrarBT
            // 
            this.CerrarBT.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.CerrarBT.BackColor = System.Drawing.Color.White;
            this.CerrarBT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.CerrarBT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.CerrarBT.Image = global::Presentismo_2015.Properties.Resources.door_out1;
            this.CerrarBT.Location = new System.Drawing.Point(24, 240);
            this.CerrarBT.Name = "CerrarBT";
            this.CerrarBT.Size = new System.Drawing.Size(55, 54);
            this.CerrarBT.TabIndex = 26;
            this.CerrarBT.Tag = "";
            this.CerrarBT.Text = "Cerrar";
            this.CerrarBT.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.CerrarBT.UseVisualStyleBackColor = false;
            this.CerrarBT.Click += new System.EventHandler(this.CerrarBT_Click);
            // 
            // txt_studentsQty
            // 
            this.txt_studentsQty.Location = new System.Drawing.Point(172, 110);
            this.txt_studentsQty.MaxLength = 2;
            this.txt_studentsQty.Name = "txt_studentsQty";
            this.txt_studentsQty.Size = new System.Drawing.Size(28, 20);
            this.txt_studentsQty.TabIndex = 96;
            this.txt_studentsQty.Tag = "";
            // 
            // AgregarBT
            // 
            this.AgregarBT.BackColor = System.Drawing.Color.White;
            this.AgregarBT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AgregarBT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.AgregarBT.Image = global::Presentismo_2015.Properties.Resources.add1;
            this.AgregarBT.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.AgregarBT.Location = new System.Drawing.Point(277, 240);
            this.AgregarBT.Name = "AgregarBT";
            this.AgregarBT.Size = new System.Drawing.Size(56, 54);
            this.AgregarBT.TabIndex = 25;
            this.AgregarBT.Text = "Agregar";
            this.AgregarBT.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AgregarBT.UseVisualStyleBackColor = false;
            this.AgregarBT.Click += new System.EventHandler(this.AgregarBT_Click);
            // 
            // Label29
            // 
            this.Label29.AutoSize = true;
            this.Label29.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label29.ForeColor = System.Drawing.Color.Red;
            this.Label29.Location = new System.Drawing.Point(223, 55);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(16, 18);
            this.Label29.TabIndex = 0;
            this.Label29.Text = "*";
            // 
            // Label28
            // 
            this.Label28.AutoSize = true;
            this.Label28.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label28.ForeColor = System.Drawing.Color.Red;
            this.Label28.Location = new System.Drawing.Point(223, 109);
            this.Label28.Name = "Label28";
            this.Label28.Size = new System.Drawing.Size(16, 18);
            this.Label28.TabIndex = 0;
            this.Label28.Text = "*";
            // 
            // cmb_dipregep
            // 
            this.cmb_dipregep.BackColor = System.Drawing.Color.White;
            this.cmb_dipregep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_dipregep.FormattingEnabled = true;
            this.cmb_dipregep.Location = new System.Drawing.Point(172, 197);
            this.cmb_dipregep.Name = "cmb_dipregep";
            this.cmb_dipregep.Size = new System.Drawing.Size(120, 21);
            this.cmb_dipregep.TabIndex = 94;
            this.cmb_dipregep.SelectedIndexChanged += new System.EventHandler(this.cmb_dipregep_SelectedIndexChanged);
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.ForeColor = System.Drawing.Color.Red;
            this.Label4.Location = new System.Drawing.Point(223, 30);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(16, 18);
            this.Label4.TabIndex = 0;
            this.Label4.Text = "*";
            // 
            // DipreLBL
            // 
            this.DipreLBL.AutoSize = true;
            this.DipreLBL.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.DipreLBL.Location = new System.Drawing.Point(20, 202);
            this.DipreLBL.Name = "DipreLBL";
            this.DipreLBL.Size = new System.Drawing.Size(80, 16);
            this.DipreLBL.TabIndex = 93;
            this.DipreLBL.Text = "DIPREGEP ";
            // 
            // txt_courseLetter
            // 
            this.txt_courseLetter.BackColor = System.Drawing.Color.White;
            this.txt_courseLetter.Location = new System.Drawing.Point(172, 56);
            this.txt_courseLetter.MaxLength = 2;
            this.txt_courseLetter.Name = "txt_courseLetter";
            this.txt_courseLetter.Size = new System.Drawing.Size(28, 20);
            this.txt_courseLetter.TabIndex = 2;
            this.txt_courseLetter.Tag = "";
            this.txt_courseLetter.TextChanged += new System.EventHandler(this.txt_courseLetter_TextChanged);
            // 
            // txt_courseNumber
            // 
            this.txt_courseNumber.CausesValidation = false;
            this.txt_courseNumber.Location = new System.Drawing.Point(172, 28);
            this.txt_courseNumber.MaxLength = 2;
            this.txt_courseNumber.Name = "txt_courseNumber";
            this.txt_courseNumber.Size = new System.Drawing.Size(28, 20);
            this.txt_courseNumber.TabIndex = 1;
            this.txt_courseNumber.Tag = "";
            this.txt_courseNumber.TextChanged += new System.EventHandler(this.txt_courseNumber_TextChanged);
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.Location = new System.Drawing.Point(20, 32);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(43, 16);
            this.Label5.TabIndex = 0;
            this.Label5.Text = "Curso";
            this.Label5.Click += new System.EventHandler(this.Label5_Click);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(20, 114);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(135, 16);
            this.Label3.TabIndex = 0;
            this.Label3.Text = "Cantidad de alumnos";
            this.Label3.Click += new System.EventHandler(this.Label3_Click);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(20, 60);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(53, 16);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Divisón";
            // 
            // AltaCurso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(402, 359);
            this.Controls.Add(this.GroupBox1);
            this.Name = "AltaCurso";
            this.Text = "Alta de curso";
            this.Load += new System.EventHandler(this.AltaCurso_Load);
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Label Label29;
        internal System.Windows.Forms.ComboBox cmb_dipregep;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label DipreLBL;
        internal System.Windows.Forms.TextBox txt_courseLetter;
        internal System.Windows.Forms.TextBox txt_courseNumber;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label Label28;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox txt_studentsQty;
        internal System.Windows.Forms.Button CerrarBT;
        internal System.Windows.Forms.Button AgregarBT;
        internal System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmb_turnID;
        private System.Windows.Forms.Label label9;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.TextBox txt_afternoonClassroom;
        internal System.Windows.Forms.TextBox txt_morningClassroom;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label label11;
    }
}