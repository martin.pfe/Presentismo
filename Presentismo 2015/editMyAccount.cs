﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Presentismo_2015
{
    public partial class editMyAccount : myCustomForm
    {
        Employee formEmployee;
        public editMyAccount(Employee logedEmployee)
        {
            InitializeComponent();
            formEmployee = logedEmployee;
            logedEmployee = null;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            txt_newPassword.Enabled = chk_UpdatePassword.Checked;
            txt_newPassword.Text = "";
            txt_NewPasswordRepeat.Enabled = chk_UpdatePassword.Checked;
            txt_NewPasswordRepeat.Text = "";
        }

        private void chkUpdateMail_CheckedChanged(object sender, EventArgs e)
        {
            txt_NewMail.Enabled = chkUpdateMail.Checked;
            txt_NewMail.Text = "";
            txt_NewMailRepeat.Enabled = chkUpdateMail.Checked;
            txt_NewMailRepeat.Text = "";
        }

        private void btn_Edit_Click(object sender, EventArgs e)
        {
            if (!chk_UpdatePassword.Checked && !chkUpdateMail.Checked)
            {
                MessageBox.Show("Debe seleccionar que datos desea actualizar.");
                return;
            }

            if (txt_oldPassword.Text == String.Empty)
            {
                MessageBox.Show("No escribió la contraseña actual, este dato es requerido para actualizar sus datos.");
                return;
            }

            if (chk_UpdatePassword.Checked)
            {
                if (txt_newPassword.Text == txt_oldPassword.Text) 
                {
                    MessageBox.Show("La contraseña nueva no puede ser igual a la anterior.");
                    return;
                }
                
                if (txt_newPassword.Text != txt_NewPasswordRepeat.Text)
                {
                    MessageBox.Show("Las nuevas contraseñas ingresadas no coinciden.");
                    return;
                }
            }

            if (chkUpdateMail.Checked)
            {
                if (txt_NewMail.Text != txt_NewMailRepeat.Text)
                {
                    MessageBox.Show("Los nuevos mails no coinciden");
                    return;
                }
            }

            
            if (txt_NewMail.Text.Length > 0)
            {

                if (!txt_NewMail.Text.ToLower().Contains('@') || !txt_NewMail.Text.ToLower().Contains('.'))
                {
                    MessageBox.Show("Por favor ingrese un mail valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txt_NewMail.SelectAll();
                    return;
                }
            }


            if (txt_NewMailRepeat.Text.Length > 0)
            {
                if (!txt_NewMail.Text.ToLower().Contains('@') || !txt_NewMail.Text.ToLower().Contains('.'))
                {
                    MessageBox.Show("Por favor ingrese un mail valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txt_NewMailRepeat.SelectAll();
                    return;
                }
            }


            try
            {
                var spParams = new DynamicParameters();
                spParams.Add("@employeeID", formEmployee.employeeID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@oldPassword", txt_oldPassword.Text, dbType: DbType.String, direction: ParameterDirection.Input, size: 15);
                spParams.Add("@updatePassword", chk_UpdatePassword.Checked ? 1 : 0 , dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@newPassword", txt_newPassword.Text, dbType: DbType.String, direction: ParameterDirection.Input, size:15);
                spParams.Add("@updateEmail", chkUpdateMail.Checked ? 1 : 0, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@newEmail", txt_NewMail.Text, dbType: DbType.String, direction: ParameterDirection.Input, size: 100);

                spParams.Add("@errorDesc", dbType: DbType.String, direction: ParameterDirection.Output, size: 500);

                spParams.Add("@RETURN_VALUE", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                using (var sqlConnection = new SqlConnection(Conection.connectionString))
                {

                    sqlConnection.Open();

                    sqlConnection.Execute("SP_Employee_EditSetting", spParams, commandType: CommandType.StoredProcedure);
                    string errorDesc = spParams.Get<string>("@errorDesc");
                    int RC = spParams.Get<int>("@RETURN_VALUE");

                    if (RC != 1)
                    {
                        MessageBox.Show(String.Format("Error: {0} - Error N° :{1}", errorDesc, RC.ToString()));
                    }
                    else
                    {
                        MessageBox.Show("Sus datos han sido actualizados correctamente.");
                        this.Hide();
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un error conectandose a la base de datos.");
            }
        }

        private void CerrarBT_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
