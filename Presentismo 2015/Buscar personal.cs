﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Dapper;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Presentismo_2015
{
    public partial class BuscarPersonal : myCustomForm
    {
        Employee logedEmployee;
        public BuscarPersonal(Employee actual)
        {
            logedEmployee = actual;
            InitializeComponent();

            //IPV 24-04-2016: Comento esto, es innecesario mostrar todos los usuarios.
            //try
            //{
            //    string select = "SELECT A.employeeID AS ID, A.DNI, A.name AS Nombre, A.lastName AS Apelido, A.birthDate AS [Fecha de nacimiento], A.birthCountry AS Nacionalidad, ";
            //    select = select + "A.homePhone AS Teléfono, A.cellPhone AS Celular, A.personalMail AS [Mail personal], A.schoolMail AS [Mail institucional], ";
            //    select = select + "A.employeeAddress AS Dirección, A.employeeLocality AS Localidad, A.employeeZipCode AS [Código Postal], A.employeeState AS Provincia, ";
            //    select = select + "CASE WHEN A.modifiedOn IS NULL THEN '' ELSE CONCAT(RTRIM(B.lastName), ', ', RTRIM(B.name), ' (',CONVERT(VARCHAR,A.modifiedOn,103), ' ',CONVERT(VARCHAR,A.modifiedOn,108),')') END AS [Última modificación]";

            //    select = select + "FROM TB_Employees A WITH(NOLOCK)";
            //    select = select + "LEFT JOIN TB_Employees B WITH(NOLOCK) ON A.modifiedBy = B.employeeID";

            //    //Connection c = new Connection();
            //    var cnn = new SqlConnection(Conection.connectionString);
            //    SqlDataAdapter dataAdapter = new SqlDataAdapter(select, Conection.connectionString); 

            //    SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
            //    DataSet ds = new DataSet();
            //    dataAdapter.Fill(ds);
            //    DataGridView1.ReadOnly = true;
            //    DataGridView1.DataSource = ds.Tables[0];
            //    cnn.Close();
                
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Ocurrió un error conectandose a la base de datos");
            //}
        }

        private void copyAlltoClipboard()
        {
            DataGridView1.SelectAll();
            DataGridView1.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            
            DataObject dataObj = DataGridView1.GetClipboardContent();
            if (dataObj != null)
                Clipboard.SetDataObject(dataObj);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                string dniStr       = dniTXT.Text;
                string legajoStr    = legajoTXT.Text;
                string nameStr      = nameTXT.Text;
                string lastNameStr  = lastNameTXT.Text;
                int bShowDeletedEmployees = 0;

                if (bShowDeletedEmployee.Checked)
                    bShowDeletedEmployees = 1;

                //string select = "SELECT A.employeeID AS ID, A.DNI, A.name AS Nombre, A.lastName AS Apelido, A.birthDate AS [Fecha de nacimiento], A.birthCountry AS Nacionalidad, ";
                //select = select + "A.homePhone AS Teléfono, A.cellPhone AS Celular, A.personalMail AS [Mail personal], A.schoolMail AS [Mail institucional], ";
                //select = select + "A.employeeAddress AS Dirección, A.employeeLocality AS Localidad, A.employeeZipCode AS [Código Postal], A.employeeState AS Provincia, ";
                //select = select + "CASE WHEN A.modifiedOn IS NULL THEN '' ELSE CONCAT(RTRIM(B.lastName), ', ', RTRIM(B.name), ' (',CONVERT(VARCHAR,A.modifiedOn,103), ' ',CONVERT(VARCHAR,A.modifiedOn,108),')') END AS [Última modificación]";

                //select = select + "FROM TB_Employees A WITH(NOLOCK)";
                //select = select + "LEFT JOIN TB_Employees B WITH(NOLOCK) ON A.modifiedBy = B.employeeID WHERE A.isEnabled = 1";

                //if (dniStr != "")
                //{
                //    select = select + " AND A.DNI LIKE '%" + dniStr + "%'";
                //}

                //if (legajoStr != "")
                //{
                //    select = select + " AND A.legajo LIKE '%" + legajoStr + "%'";
                //}

                //if (nameStr != "")
                //{
                //    select = select + " AND A.name LIKE '%" + nameStr + "%'";
                //}

                //if (lastNameStr != "")
                //{
                //    select = select + " AND A.lastName LIKE '%" + lastNameStr + "%'";
                //}

                //var cnn = new SqlConnection(Conection.connectionString);
                //SqlDataAdapter dataAdapter = new SqlDataAdapter(select, Conection.connectionString); 

                //SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
                //DataSet ds = new DataSet();
                //dataAdapter.Fill(ds);
                //DataGridView1.ReadOnly = true;
                //DataGridView1.DataSource = ds.Tables[0];
                //cnn.Close();

                var spParams = new DynamicParameters();
                spParams.Add("@_DNIstr", dniStr, dbType: DbType.String, direction: ParameterDirection.Input, size: 20);
                spParams.Add("@_legajoStr", legajoStr, dbType: DbType.String, direction: ParameterDirection.Input, size: 20);
                spParams.Add("@_nameStr", nameStr, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                spParams.Add("@_lastNameStr", lastNameStr, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                spParams.Add("@_bShowDeletedEmployees", bShowDeletedEmployees, dbType: DbType.Int32, direction: ParameterDirection.Input);


                using (var sqlConnection = new SqlConnection(Conection.connectionString))
                {
                    sqlConnection.Open();
                    IDataReader rd;
                    DataTable dt = new DataTable();

                    rd = sqlConnection.ExecuteReader("SP_Employee_Search", spParams, commandType: CommandType.StoredProcedure);

                    dt.Load(rd);

                    DataGridView1.DataSource = dt;
                    DataGridView1.AutoGenerateColumns = true;
                    DataGridView1.Refresh();
                    DataGridView1.ReadOnly = true;
                    DataGridView1.AllowUserToAddRows = false;
                    DataGridView1.AllowUserToDeleteRows = false;
                    DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string DNI;

            string name;

            DNI = DataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();

            name = DataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();

            name = name + ", " + DataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();

            string deletedBy = DataGridView1.Rows[e.RowIndex].Cells[15].Value.ToString();

            if (deletedBy == "")
            {
                int employeeID = Int32.Parse(DataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
                //InterfazDeEdicion form = new InterfazDeEdicion(logedEmployee, DNI, name, employeeID);
                //this.Close();
                //form.Show();
            }
            else
            {
                MessageBox.Show("El empleado que intenta modificar ha sido dado de baja.");
            }
        }
        private void BuscarPersonal_Load(object sender, EventArgs e)
        {

        }
        private void label1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(logedEmployee.DNI.ToString());

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void BuscarPersonal_Load_1(object sender, EventArgs e)
        {

            
        }

        private void bShowDeletedEmployee_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            copyAlltoClipboard();
            Microsoft.Office.Interop.Excel.Application xlexcel;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlexcel = new Excel.Application();
            xlexcel.Visible = true;
            xlWorkBook = xlexcel.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            xlWorkSheet.Range["A:Z"].EntireColumn.AutoFit();
            Excel.Range CR = (Excel.Range)xlWorkSheet.Cells[1, 1];

            CR.Range["A1:Z1"].EntireColumn.AutoFit();

            

            CR.Select();

            Excel.Range xlEntireColumn = null;
            xlEntireColumn = CR.EntireColumn;
            xlEntireColumn.AutoFit();

            xlWorkSheet.PasteSpecial(CR, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, true);
        }

    }
}
