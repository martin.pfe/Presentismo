﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Dapper;
using System.Data.Common;
using System.Text.RegularExpressions;

namespace Presentismo_2015
{
    public partial class Login : myCustomForm
    {
        private int employeeID { get; set; }
        public Login()
        {
            InitializeComponent();

        }

        private void UsuarioTXT_TextChanged(object sender, EventArgs e)
        {

        }

        private void OK_Click(object sender, EventArgs e)
        {
            int userDni = 0;
            //if (!Int32.TryParse(txt_user.Text, out userDni) || txt_user.Text.Length != 8 )
            if (!Int32.TryParse(txt_user.Text, out userDni))
            {
                MessageBox.Show("El DNI ingresado es incorrecto. Recuerde incluir solo los números.");
                return;
            }
                //String userDni = txt_user.Text.ToString();
            String userPassword = txt_Password.Text.ToString();

          
           // MessageBox.Show("Dni = " + userDni.ToString() + " Test1" + userPassword);

            try
            {

              //  MessageBox.Show("Connection Open ! ");
                var spParams = new DynamicParameters();
                spParams.Add("@userDNI", userDni, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@userPassword", userPassword, dbType: DbType.String, direction: ParameterDirection.Input, size:20);

                spParams.Add("@employeeID", dbType: DbType.Int32, direction: ParameterDirection.Output);
                spParams.Add("@errorDesc", dbType: DbType.String, direction: ParameterDirection.Output, size: 500);

                spParams.Add("@RETURN_VALUE", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                using (var sqlConnection = new SqlConnection(Conection.connectionString))
                {

                    sqlConnection.Open();

                    sqlConnection.Execute("SP_UserLogin", spParams, commandType: CommandType.StoredProcedure);
                    int RC = spParams.Get<int>("@RETURN_VALUE");

                    int employeeID  = spParams.Get<int>("@employeeID");
                    string errorDesc= spParams.Get<string>("@errorDesc");


                    if (RC == 1)
                    {
                        MessageBox.Show("Bienvenido");
                        Employee logedEmployee = new Employee(userDni, userPassword);
                        //logedEmployee.DNI = userDni;
                        // var dog = sqlConnection.Query<Employee>("select Age = @Age, Id = @Id", new { Age = (int?)null, Id = guid });
                       // using (DbConnection conn = new SqlConnection(Conection.connectionString))
                        using (DbCommand cmd = sqlConnection.CreateCommand())
                        {
                            cmd.CommandText = String.Format("SELECT name, lastName, birthDateChr, birthCountry, homePhone, cellPhone, personalMail, schoolMail, ISNULL(accessType,2) AS accessType FROM dbo.VW_Employees WITH(NOLOCK) WHERE employeeID = {0}", employeeID);

                            using (DbDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    logedEmployee.name          = reader.GetString(0);
                                    logedEmployee.lastName      = reader.GetString(1);
                                    logedEmployee.birthDate     = reader.GetString(2);
                                    logedEmployee.birthPlace    = reader.GetString(3);
                                    logedEmployee.homePhone     = reader.GetString(4);
                                    logedEmployee.cellPhone     = reader.GetString(5);
                                    logedEmployee.personalMail  = reader.GetString(6);
                                    logedEmployee.schoolMail    = reader.GetString(7);
                                    logedEmployee.accessType    = reader.GetInt32(8);
                                }
                            }
                        }

                        if (logedEmployee.accessType != 1)
                        {
                            using (DbCommand cmd = sqlConnection.CreateCommand())
                            {
                                cmd.CommandText = String.Format("SELECT permissionID, ISNULL(isEnabled,0) isEnabled  FROM VW_Employee_Permission WHERE employeeID = {0}", employeeID);

                                using (DbDataReader reader = cmd.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        Console.WriteLine(reader.GetInt32(0));
                                        switch (Convert.ToInt32(reader.GetInt32(0)))
                                        {
                                            /*/
                                            IPV 2016-04-07: Sacamos esta funcionalidad ya que lo vamos a hacer por tipo de usuario 
                                            case 1:
                                                editPermission = Convert.ToBoolean(reader.GetString(1));
                                                break;
                                       
                                            /*/
                                            case 2:
                                                logedEmployee.viewCourses = Convert.ToBoolean(reader.GetInt32(1));
                                                break;
                                            case 3:
                                                logedEmployee.addEmployee = Convert.ToBoolean(reader.GetInt32(1));
                                                break;
                                            case 4:
                                                logedEmployee.editEmployee = Convert.ToBoolean(reader.GetInt32(1));
                                                break;
                                            case 5:
                                                logedEmployee.editCharges = Convert.ToBoolean(reader.GetInt32(1));
                                                break;

                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            logedEmployee.viewCourses = true;
                            logedEmployee.addEmployee = true;
                            logedEmployee.editEmployee = true;
                            logedEmployee.editCharges = true;
                        }
                        logedEmployee.employeeID = employeeID;

                        Principal form1 = new Principal(logedEmployee);
                        form1.Show();
                        this.Hide();
                    }
                    else
                        MessageBox.Show(String.Format("Error: {0} - Error N° :{1}", errorDesc, RC.ToString()));

                    sqlConnection.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo establecer la conección con la base de datos.");
            }
        }

        
    // txt_user.KeyPress += new KeyPressEventHandler(txt_user_KeyPress);
        private void txt_user_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            // Check for a naughty character in the KeyDown event.
            if (System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), @"[^0-9^\b]"))
            {
                // Stop the character from being entered into the control since it is illegal.
                e.Handled = true;
            }
        }

        private void txt_user_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txt_Password_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Check for a naughty character in the KeyDown event.
            if (System.Text.RegularExpressions.Regex.IsMatch(e.KeyChar.ToString(), @"[^0-9^\b^QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm]"))
            {
                // Stop the character from being entered into the control since it is illegal.
                e.Handled = true;
            }
        }

        private void LogoPictureBox_Click(object sender, EventArgs e)
        {

        }

        private void Cancel_Click(object sender, EventArgs e)
        {

        }

        private void CerrarBT_Click(object sender, EventArgs e)
        {
            IngresoFirma.ingresoFirma form = new IngresoFirma.ingresoFirma();
            this.Hide();
            form.Show();
        }

        private void txt_Password_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txt_Password_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                OK_Click(sender, e);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Configuration config = new Configuration();
            config.Show();
        }

        //private void txt_user_KeyDown(object sender, KeyEventArgs e)
        //{
        //    Regex regex = new Regex(@"[0-9+\-\/\*\(\)]");
        //    MatchCollection matches = regex.Matches(txt_user.Text);
        //}
    }
}
