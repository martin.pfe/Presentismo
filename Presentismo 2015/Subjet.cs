﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Presentismo_2015
{
    public class Subjet
    {
        internal int ID { get; set; }
        internal String Name { get; set; }
        internal int dipregepID { get; set; }

        internal String dipregepDesc { get; set; }
    }
}
