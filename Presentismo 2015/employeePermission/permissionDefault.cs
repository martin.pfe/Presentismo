﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using Dapper;
using System.Data.SqlClient;
using System.Data.Common;


namespace Presentismo_2015.employeePermission
{
    public partial class permissionDefault : myCustomForm
    {
        Employee logedEmployee;
        string name;
        int employeeIDToFilter;

        public permissionDefault(Employee actual, string nameStr, int employeeID)
        {
            InitializeComponent();
            logedEmployee       = actual;
            name                = nameStr;
            employeeIDToFilter  = employeeID;
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        public void button2_Click(object sender, EventArgs e)
        {   

        }

        private void permissionDefault_Load(object sender, EventArgs e)
        {
            bool isCursosEnabled         = false;
            bool isEmployeeAddEnabled    = false;
            bool isEmployeeEditEnabled   = false;
            bool isChargesEditEnabled   = false;

            using (DbConnection conn = new SqlConnection(Conection.connectionString))
            using (DbCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = String.Format("SELECT * FROM VW_Employee_Permission WHERE employeeID = {0} ORDER BY permissionID", employeeIDToFilter);

                conn.Open();

                using (DbDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (reader.GetInt32(1) == 1) { 
                            switch (reader.GetInt32(0))
                            {
                                case 2:
                                    isCursosEnabled = true;
                                    break;
                                case 3:
                                    isEmployeeAddEnabled = true;
                                    break;
                                case 4:
                                    isEmployeeEditEnabled = true;
                                    break;
                                case 5:
                                    isChargesEditEnabled = true;
                                    break;
                            }
                        }
                    }
                }
                conn.Close();
            }

            if (isCursosEnabled)
                checkBox2.Checked = true;

            if (isEmployeeAddEnabled)
                checkBox3.Checked = true;

            if (isEmployeeEditEnabled)
                checkBox4.Checked = true;

            if (isChargesEditEnabled)
                checkBox5.Checked = true;
        }

        public void button4_Click(object sender, EventArgs e)
        {
            bool viewCourses        = false; 
            bool addEmployee        = false; 
            bool editEmployee       = false; 
            bool editCharges        = false; 

            if (checkBox2.Checked)
                viewCourses = true;
            if (checkBox3.Checked)
                addEmployee = true;
            if (checkBox4.Checked)
                editEmployee = true;
            if (checkBox4.Checked)
                editCharges = true;

            //IPV 2016-03-23: Levantamos el employeeID del usuario logueado en el sistema
            int userID      = logedEmployee.employeeID;
            try
            {
                var spParams = new DynamicParameters();
                spParams.Add("@employeeID", userID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@employeeIDToFilter", employeeIDToFilter, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@viewCourses", viewCourses, dbType: DbType.Boolean, direction: ParameterDirection.Input);
                spParams.Add("@addEmployee", addEmployee, dbType: DbType.Boolean, direction: ParameterDirection.Input);
                spParams.Add("@editEmployee", editEmployee, dbType: DbType.Boolean, direction: ParameterDirection.Input);
                spParams.Add("@editCharges", editCharges, dbType: DbType.Boolean, direction: ParameterDirection.Input);

                spParams.Add("@errorDesc", dbType: DbType.String, direction: ParameterDirection.Output, size: 30);
                spParams.Add("@RETURN_VALUE", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                using (var sqlConnection = new SqlConnection(Conection.connectionString))
                {

                    sqlConnection.Open();

                    sqlConnection.Execute("SP_Employee_Permission", spParams, commandType: CommandType.StoredProcedure);
                    int RC = spParams.Get<int>("@RETURN_VALUE");
                    string errorDesc = spParams.Get<string>("@errorDesc");

                    sqlConnection.Close();
                    if (RC != 1)
                    {
                        MessageBox.Show(String.Format("Error: {0} - Error N° :{1}", errorDesc, RC.ToString()));
                    }
                    else
                    {
                        MessageBox.Show("Los permisos se han guardado satisfactoriamente! A continuación será direccionado al listado de usuarios.");
                        BuscarPersonal buscarPersonal = new BuscarPersonal(logedEmployee);
                        buscarPersonal.Show();
                        this.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                MessageBox.Show("Ha ocurrido un error intentando guardar los permisos. Por favor contactese con un administrador.");
            }
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            BuscarPersonal buscarPersonal = new BuscarPersonal(logedEmployee);
            buscarPersonal.Show();
            this.Close();
        }
    }
}
