﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Dapper;

namespace Presentismo_2015
{
    public partial class editCargos : myCustomForm
    {
        Employee logedEmployee;
        int employeeIDToFIlter;
        public editCargos(int employeeID, Employee actual, string employeeName)
        {
            InitializeComponent();
            label2.Text = "Cargos de " + employeeName;
            string connectionString = Conection.connectionString;
            logedEmployee = actual;
            employeeIDTXT.Text = employeeID.ToString();
            employeeIDToFIlter = employeeID;

            try
            {
                DataTable tablaCat = new DataTable();
                //IPV: 2015/04/26: Cargo combo de categorías
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    string Sql = "SELECT categoryDesc, categoryID  FROM TB_Employee_Categories WITH(NOLOCK)";

                    SqlCommand cmd = new SqlCommand(Sql, conn);

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(tablaCat);
                }

                cmbCategory.DisplayMember = "categoryDesc";
                cmbCategory.ValueMember = "categoryID";
                cmbCategory.DataSource = tablaCat;
            }
            catch
            {
                MessageBox.Show("Ocurrió un error al intentar cargar la interfaz.");
            }

            try
            {
                //string connectionString = "Server=tcp:oer8py276v.database.windows.net,1433;Database=Presentismo;User ID=proyecto2015@oer8py276v;Password=Colegiojuan23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                string select = "SELECT R.categoryID AS ID, C.categoryDesc AS Categoría , ";
                select = select + "CONCAT(RTRIM(E.lastName), ', ', RTRIM(E.name), ' (',CONVERT(VARCHAR,R.createdOn,103), ' ',CONVERT(VARCHAR,R.createdOn,108),')') AS [Creado por]";
                select = select + " FROM TB_Employee_Categories_Relation R WITH(NOLOCK)";
                select = select + " INNER JOIN dbo.TB_Employee_Categories C WITH(NOLOCK) ON C.categoryID = R.categoryID";
                select = select + " INNER JOIN dbo.TB_Employees E WITH(NOLOCK) ON E.employeeID = R.createdBy";
                select = select + " WHERE ISNULL(R.isDeleted,0) = 0 AND R.employeeID = " + employeeID;
                //Connection c = new Connection();
                var cnn = new SqlConnection(connectionString);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(select, Conection.connectionString); //c.con is the connection string

                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
                DataSet ds = new DataSet();
                dataAdapter.Fill(ds);
                DataGridView1.ReadOnly = true;
                DataGridView1.DataSource = ds.Tables[0];
                cnn.Close();
                DataGridView1.Columns["ID"].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void cmbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void AgregarBT_Click(object sender, EventArgs e)
        {
            int employeeID = Int32.Parse(employeeIDTXT.Text);
            
            // string cmbCategory = cmbCategory.Text;
            
            int categoryID = Convert.ToInt32(cmbCategory.SelectedValue);
            string connectionString = Conection.connectionString;

            try
            {
                int userID = logedEmployee.employeeID;
                var spParams = new DynamicParameters();
                spParams.Add("@employeeID", employeeID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@categoryID", categoryID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@userID", userID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@errorDesc", dbType: DbType.String, direction: ParameterDirection.Output, size: 30);


                spParams.Add("@RETURN_VALUE", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                using (var sqlConnection = new SqlConnection(Conection.connectionString))
                {

                    sqlConnection.Open();

                    sqlConnection.Execute("SP_Charge_Add", spParams, commandType: CommandType.StoredProcedure);
                    string errorDesc = spParams.Get<string>("@errorDesc");
                    int RC = spParams.Get<int>("@RETURN_VALUE");

                    sqlConnection.Close();
                    if (RC != 1)
                    {
                        MessageBox.Show(String.Format("Error: {0} - Error N° :{1}", errorDesc, RC.ToString()));
                    }
                    else
                    {
                        MessageBox.Show("El cargo ha sido dado de alta satisfactoriamente!");
                    }

                    try
                    {
                        //string connectionString = "Server=tcp:oer8py276v.database.windows.net,1433;Database=Presentismo;User ID=proyecto2015@oer8py276v;Password=Colegiojuan23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                        string select = "SELECT R.categoryID AS ID, C.categoryDesc AS Categoría , ";
                        select = select + "CONCAT(RTRIM(E.lastName), ', ', RTRIM(E.name), ' (',CONVERT(VARCHAR,R.createdOn,103), ' ',CONVERT(VARCHAR,R.createdOn,108),')') AS [Creado por]";
                        select = select + " FROM TB_Employee_Categories_Relation R WITH(NOLOCK)";
                        select = select + " INNER JOIN dbo.TB_Employee_Categories C WITH(NOLOCK) ON C.categoryID = R.categoryID";
                        select = select + " INNER JOIN dbo.TB_Employees E WITH(NOLOCK) ON E.employeeID = R.createdBy";
                        select = select + " WHERE ISNULL(R.isDeleted,0) = 0 AND R.employeeID = " + employeeID;
                        //Connection c = new Connection();
                        var cnn = new SqlConnection(connectionString);
                        SqlDataAdapter dataAdapter = new SqlDataAdapter(select, Conection.connectionString); //c.con is the connection string

                        SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
                        DataSet ds = new DataSet();
                        dataAdapter.Fill(ds);
                        DataGridView1.ReadOnly = true;
                        DataGridView1.DataSource = ds.Tables[0];
                        cnn.Close();

                        DataGridView1.Columns["ID"].Visible = false;


                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }


        private void AgregarBT_Click2(object sender, EventArgs e)
        {

        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string connectionString = Conection.connectionString;
            if (MessageBox.Show("¿Seguro que desea eliminar el cargo?", "Eliminar cargo",
            MessageBoxButtons.YesNo, MessageBoxIcon.Question,
            MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
            {
                int categoryID = Int32.Parse(DataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());

                int employeeID = logedEmployee.employeeID;
                try
                {
                    int userID = logedEmployee.employeeID;
                    var spParams = new DynamicParameters();
                    spParams.Add("@employeeID", employeeID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    spParams.Add("@employeeIDToFIlter", employeeIDToFIlter, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    spParams.Add("@categoryID", categoryID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    spParams.Add("@errorDesc", dbType: DbType.String, direction: ParameterDirection.Output, size: 30);


                    spParams.Add("@RETURN_VALUE", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                    using (var sqlConnection = new SqlConnection(Conection.connectionString))
                    {

                        sqlConnection.Open();

                        sqlConnection.Execute("SP_Charge_Delete", spParams, commandType: CommandType.StoredProcedure);
                        string errorDesc = spParams.Get<string>("@errorDesc");
                        int RC = spParams.Get<int>("@RETURN_VALUE");

                        sqlConnection.Close();

                        if (RC != 1)
                        {
                            MessageBox.Show(String.Format("Error: {0} - Error N° :{1}", errorDesc, RC.ToString()));
                        }
                        else
                        {
                            MessageBox.Show("El cargo ha sido eliminado satisfactoriamente!");
                        }

                        try
                        {
                            //string connectionString = "Server=tcp:oer8py276v.database.windows.net,1433;Database=Presentismo;User ID=proyecto2015@oer8py276v;Password=Colegiojuan23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                            string select = "SELECT R.categoryID AS ID, C.categoryDesc AS Categoría , ";
                            select = select + "CONCAT(RTRIM(E.lastName), ', ', RTRIM(E.name), ' (',CONVERT(VARCHAR,R.createdOn,103), ' ',CONVERT(VARCHAR,R.createdOn,108),')') AS [Creado por]";
                            select = select + " FROM TB_Employee_Categories_Relation R WITH(NOLOCK)";
                            select = select + " INNER JOIN dbo.TB_Employee_Categories C WITH(NOLOCK) ON C.categoryID = R.categoryID";
                            select = select + " INNER JOIN dbo.TB_Employees E WITH(NOLOCK) ON E.employeeID = R.createdBy";
                            select = select + " WHERE ISNULL(R.isDeleted,0) = 0 AND R.employeeID = " + employeeIDToFIlter;
                            //Connection c = new Connection();
                            var cnn = new SqlConnection(connectionString);
                            SqlDataAdapter dataAdapter = new SqlDataAdapter(select, Conection.connectionString); //c.con is the connection string

                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
                            DataSet ds = new DataSet();
                            dataAdapter.Fill(ds);
                            DataGridView1.ReadOnly = true;
                            DataGridView1.DataSource = ds.Tables[0];
                            cnn.Close();

                            DataGridView1.Columns["ID"].Visible = false;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }



            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Cargos_Load(object sender, EventArgs e)
        {

        }

        private void CerrarBT_Click(object sender, EventArgs e)
        {
            this.Hide();
            BuscarPersonal BuscarPersonal = new BuscarPersonal(logedEmployee);
            BuscarPersonal.Show();
        }
    }
}
