﻿namespace Presentismo_2015
{
    partial class editMyAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows myCustomForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_oldPassword = new System.Windows.Forms.TextBox();
            this.txt_newPassword = new System.Windows.Forms.TextBox();
            this.txt_NewPasswordRepeat = new System.Windows.Forms.TextBox();
            this.txt_NewMail = new System.Windows.Forms.TextBox();
            this.txt_NewMailRepeat = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chk_UpdatePassword = new System.Windows.Forms.CheckBox();
            this.chkUpdateMail = new System.Windows.Forms.CheckBox();
            this.CerrarBT = new System.Windows.Forms.Button();
            this.btn_Edit = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nueva contraseña:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Repita nueva contraseña:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 190);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Nuevo email";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 217);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Repita nuevo email";
            // 
            // txt_oldPassword
            // 
            this.txt_oldPassword.Location = new System.Drawing.Point(237, 20);
            this.txt_oldPassword.Name = "txt_oldPassword";
            this.txt_oldPassword.PasswordChar = '*';
            this.txt_oldPassword.Size = new System.Drawing.Size(100, 20);
            this.txt_oldPassword.TabIndex = 1;
            // 
            // txt_newPassword
            // 
            this.txt_newPassword.Enabled = false;
            this.txt_newPassword.Location = new System.Drawing.Point(239, 84);
            this.txt_newPassword.MaxLength = 15;
            this.txt_newPassword.Name = "txt_newPassword";
            this.txt_newPassword.PasswordChar = '*';
            this.txt_newPassword.Size = new System.Drawing.Size(100, 20);
            this.txt_newPassword.TabIndex = 3;
            // 
            // txt_NewPasswordRepeat
            // 
            this.txt_NewPasswordRepeat.Enabled = false;
            this.txt_NewPasswordRepeat.Location = new System.Drawing.Point(239, 110);
            this.txt_NewPasswordRepeat.MaxLength = 15;
            this.txt_NewPasswordRepeat.Name = "txt_NewPasswordRepeat";
            this.txt_NewPasswordRepeat.PasswordChar = '*';
            this.txt_NewPasswordRepeat.Size = new System.Drawing.Size(100, 20);
            this.txt_NewPasswordRepeat.TabIndex = 4;
            // 
            // txt_NewMail
            // 
            this.txt_NewMail.Enabled = false;
            this.txt_NewMail.Location = new System.Drawing.Point(237, 183);
            this.txt_NewMail.MaxLength = 100;
            this.txt_NewMail.Name = "txt_NewMail";
            this.txt_NewMail.Size = new System.Drawing.Size(100, 20);
            this.txt_NewMail.TabIndex = 6;
            // 
            // txt_NewMailRepeat
            // 
            this.txt_NewMailRepeat.Enabled = false;
            this.txt_NewMailRepeat.Location = new System.Drawing.Point(237, 210);
            this.txt_NewMailRepeat.MaxLength = 100;
            this.txt_NewMailRepeat.Name = "txt_NewMailRepeat";
            this.txt_NewMailRepeat.Size = new System.Drawing.Size(100, 20);
            this.txt_NewMailRepeat.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Contraseña actual:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Honeydew;
            this.groupBox1.Controls.Add(this.chk_UpdatePassword);
            this.groupBox1.Controls.Add(this.chkUpdateMail);
            this.groupBox1.Controls.Add(this.txt_NewMailRepeat);
            this.groupBox1.Controls.Add(this.txt_NewMail);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txt_NewPasswordRepeat);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txt_newPassword);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txt_oldPassword);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(25, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(360, 255);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Seleccione los datos que quiere actualizar";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // chk_UpdatePassword
            // 
            this.chk_UpdatePassword.AutoSize = true;
            this.chk_UpdatePassword.Location = new System.Drawing.Point(17, 55);
            this.chk_UpdatePassword.Name = "chk_UpdatePassword";
            this.chk_UpdatePassword.Size = new System.Drawing.Size(128, 17);
            this.chk_UpdatePassword.TabIndex = 2;
            this.chk_UpdatePassword.Text = "Actualizar contraseña";
            this.chk_UpdatePassword.UseVisualStyleBackColor = true;
            this.chk_UpdatePassword.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // chkUpdateMail
            // 
            this.chkUpdateMail.AutoSize = true;
            this.chkUpdateMail.Location = new System.Drawing.Point(19, 153);
            this.chkUpdateMail.Name = "chkUpdateMail";
            this.chkUpdateMail.Size = new System.Drawing.Size(99, 17);
            this.chkUpdateMail.TabIndex = 5;
            this.chkUpdateMail.Text = "Actualizar email";
            this.chkUpdateMail.UseVisualStyleBackColor = true;
            this.chkUpdateMail.CheckedChanged += new System.EventHandler(this.chkUpdateMail_CheckedChanged);
            // 
            // CerrarBT
            // 
            this.CerrarBT.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.CerrarBT.BackColor = System.Drawing.Color.White;
            this.CerrarBT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.CerrarBT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.CerrarBT.Image = global::Presentismo_2015.Properties.Resources.door_out1;
            this.CerrarBT.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.CerrarBT.Location = new System.Drawing.Point(25, 317);
            this.CerrarBT.Name = "CerrarBT";
            this.CerrarBT.Size = new System.Drawing.Size(64, 60);
            this.CerrarBT.TabIndex = 9;
            this.CerrarBT.Tag = "";
            this.CerrarBT.Text = "Cerrar";
            this.CerrarBT.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.CerrarBT.UseVisualStyleBackColor = false;
            this.CerrarBT.Click += new System.EventHandler(this.CerrarBT_Click);
            // 
            // btn_Edit
            // 
            this.btn_Edit.BackColor = System.Drawing.Color.White;
            this.btn_Edit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Edit.Image = global::Presentismo_2015.Properties.Resources.pencil;
            this.btn_Edit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_Edit.Location = new System.Drawing.Point(321, 317);
            this.btn_Edit.Name = "btn_Edit";
            this.btn_Edit.Size = new System.Drawing.Size(64, 60);
            this.btn_Edit.TabIndex = 8;
            this.btn_Edit.Text = "Editar";
            this.btn_Edit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btn_Edit.UseVisualStyleBackColor = false;
            this.btn_Edit.Click += new System.EventHandler(this.btn_Edit_Click);
            // 
            // editMyAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(406, 397);
            this.Controls.Add(this.btn_Edit);
            this.Controls.Add(this.CerrarBT);
            this.Controls.Add(this.groupBox1);
            this.Name = "editMyAccount";
            this.Text = "Editar mi cuenta";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_oldPassword;
        private System.Windows.Forms.TextBox txt_newPassword;
        private System.Windows.Forms.TextBox txt_NewPasswordRepeat;
        private System.Windows.Forms.TextBox txt_NewMail;
        private System.Windows.Forms.TextBox txt_NewMailRepeat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chk_UpdatePassword;
        private System.Windows.Forms.CheckBox chkUpdateMail;
        internal System.Windows.Forms.Button CerrarBT;
        private System.Windows.Forms.Button btn_Edit;
    }
}