﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Presentismo_2015
{
    public class Course
    {
        public String courseStr { get; set; }
        public String turnID { get; set; }
        public int morningClassroom { get; set; }
        public int afternoonClassRoom { get; set; }
        public int studentsQty { get; set; }
        public int dipregep { get; set; }
        
    }
}
