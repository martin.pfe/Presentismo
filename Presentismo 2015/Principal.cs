﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Presentismo_2015
{
    public partial class Principal : myCustomForm
    {
        Employee logedEmployee;
        public Principal(Employee actual)
        {
            InitializeComponent();
            logedEmployee = actual;

            label1.Text = String.Concat(logedEmployee.lastName, ", ", logedEmployee.name);

            //ProfesorToolStripMenuItem.Enabled = logedEmployee.addEmployee;
             if(!logedEmployee.viewCourses)
            {
                cursosToolStripMenuItem.Enabled = false;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
        
        }
        //MP:: Nunca escondemos la pantalla principal
        private void ProfesorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AltaPersonal altaPersonal = new AltaPersonal(logedEmployee);
            altaPersonal.Show();
        }

        private void ProfesorToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            BuscarPersonal buscarPersonal = new BuscarPersonal(logedEmployee);
            buscarPersonal.Show();
        }

        private void cursosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            defaultCurso defaultCurso = new defaultCurso(logedEmployee);
            defaultCurso.Show();
        }

        private void cargosToolStripMenuItem_Click(object sender, EventArgs e)
        {
         
        }

        private void asignaturasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Asignatura.defaultAsignatura form = new Asignatura.defaultAsignatura(logedEmployee);
            form.Show();
        }

        private void horarioDeClaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Relation.relationDefault relation = new Relation.relationDefault(logedEmployee);
            relation.Show();
        }

        private void permisosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //employeePermission.permissionDefault permission = new employeePermission.permissionDefault(logedEmployee);
            //employeePermission.Show();
        }

        private void ToolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            editMyAccount editAcc = new editMyAccount(logedEmployee);
            editAcc.Show();
        }

        private void Principal_Load(object sender, EventArgs e)
        {

        }

        private void ToolStripButton1_Click(object sender, EventArgs e)
        {

        }

        private void endSession_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Login login = new Login();
            logedEmployee = null;
            login.Show();
            this.Hide();
        }
    }
}
