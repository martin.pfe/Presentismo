﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Presentismo_2015
{
    public partial class Configuration : myCustomForm
    {
        private String configurationPassword = ConfigurationManager.AppSettings["configurationPassword"];
        private String tryConnectionString = String.Empty;
        public Configuration()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_lastPassword.Text.Trim() != configurationPassword.Trim())
                {
                    MessageBox.Show("La contraseña actual es incorrecta.");
                    return;
                }

                if (txt_newPassword.Text.Trim() != string.Empty)
                {
                    if (txt_newPassword.Text.Length < 8)
                    {
                        MessageBox.Show("Las contraseña ingresada es poco segura.");
                        return;
                    }

                    if (txt_newPassword.Text.Trim() != txt_newPasswordRepeat.Text.Trim())
                    {
                        MessageBox.Show("Las contraseñas ingresadas no coinciden.");
                        return;
                    }

                    if (txt_newPassword.Text.Trim() == txt_lastPassword.Text.Trim())
                    {
                        MessageBox.Show("La nueva contraseña no puede ser igual a la anterior.");
                        return;
                    }
                }
                else
                {
                    if (txt_lastPassword.Text.Trim() == "PresentismoJuan23")
                    {
                        MessageBox.Show("Debe cambiar la contraseña por defecto.");
                        return;
                    } 
                }

                if (txt_signaturesPath.Text.Trim() == "")
                {
                    MessageBox.Show("Debe ingresar la carpeta donde se guardan las firmas.");
                    return;
                }

                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                config.AppSettings.Settings["configurationPassword"].Value = txt_newPassword.Text.Trim();
                config.AppSettings.Settings["signaturesPath"].Value = txt_signaturesPath.Text.Trim();
                config.AppSettings.Settings["actualCString"].Value = tryConnectionString;
                config.Save(ConfigurationSaveMode.Modified);

                MessageBox.Show("Los cambios han sido guardados correctamente.");
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error editando la configuración");
            }
        }
        private  void UpdateSetting(string key, string value)
        {
            //Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            //configuration.AppSettings.Settings[key].Value = value;
            //configuration.Save();

            //ConfigurationManager.RefreshSection("appSettings");
        }

        private void btn_testConnection_Click(object sender, EventArgs e)
        {
            if (txt_dbName.Text.Trim() == "" || txt_dbPassword.Text.Trim() == "" || txt_dbServer.Text.Trim() == "")
            {
                MessageBox.Show("Complete todos los campos.");
                return;
            }

            tryConnectionString = String.Concat("id=", txt_dbServer.Text.Trim(), ";user id=",txt_dbName.Text.Trim(),";pwd=",txt_dbPassword.Text.Trim() ,";data source=", txt_dbServer.Text.Trim(),";persist security info=False;initial catalog=Presentismo");

            try
            {
                using (var sqlConnection = new SqlConnection(Conection.connectionString))
                {
                    MessageBox.Show("La conexión con la base de datos fue establecida correctamente.");
                    txt_dbName.Enabled = false;
                    txt_dbPassword.Enabled = false;
                    txt_dbServer.Enabled = false;
                    btn_testConnection.Enabled = false;

                    confirmBtn.Enabled = true;
                }
            }
            catch (Exception ex)
            { 
                MessageBox.Show("No se pudo establecer conexión con la base de datos.");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
