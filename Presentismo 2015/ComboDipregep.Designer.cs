﻿namespace Presentismo_2015
{
    partial class ComboDipregep
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dipregepCombo = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dipregep";
            // 
            // dipregepCombo
            // 
            this.dipregepCombo.FormattingEnabled = true;
            this.dipregepCombo.Location = new System.Drawing.Point(139, 15);
            this.dipregepCombo.Name = "dipregepCombo";
            this.dipregepCombo.Size = new System.Drawing.Size(121, 21);
            this.dipregepCombo.TabIndex = 1;
            this.dipregepCombo.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // ComboDipregep
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.Controls.Add(this.dipregepCombo);
            this.Controls.Add(this.label1);
            this.Name = "ComboDipregep";
            this.Size = new System.Drawing.Size(276, 51);
            this.Load += new System.EventHandler(this.ComboDipregep_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox dipregepCombo;
    }
}
