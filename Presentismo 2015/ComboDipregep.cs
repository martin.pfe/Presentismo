﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Presentismo_2015
{
    public partial class ComboDipregep : UserControl
    {
        public ComboDipregep()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ComboDipregep_Load(object sender, EventArgs e)
        {
            DipregepComboItems Item = new DipregepComboItems();
            Item.Text = "Secundaria basica";
            Item.Value = 0;

            dipregepCombo.Items.Add(Item);

            Item.Text = "Secundaria técnica";
            Item.Value = 1;

            dipregepCombo.Items.Add(Item);

            dipregepCombo.SelectedValue = 0;
            dipregepCombo.SelectedText = "Secundaria basica";

           // MessageBox.Show((comboBox1.SelectedItem as ComboboxItem).Value.ToString());


        }
    }
}
