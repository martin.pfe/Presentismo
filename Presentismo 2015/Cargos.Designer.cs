﻿namespace Presentismo_2015
{
    partial class Cargos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows myCustomForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CerrarBT = new System.Windows.Forms.Button();
            this.AgregarBT = new System.Windows.Forms.Button();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.Label41 = new System.Windows.Forms.Label();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.employeeIDTXT = new System.Windows.Forms.Label();
            this.DataGridView1 = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // CerrarBT
            // 
            this.CerrarBT.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.CerrarBT.BackColor = System.Drawing.Color.White;
            this.CerrarBT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.CerrarBT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.CerrarBT.Image = global::Presentismo_2015.Properties.Resources.door_out1;
            this.CerrarBT.Location = new System.Drawing.Point(50, 398);
            this.CerrarBT.Name = "CerrarBT";
            this.CerrarBT.Size = new System.Drawing.Size(55, 54);
            this.CerrarBT.TabIndex = 26;
            this.CerrarBT.Tag = "";
            this.CerrarBT.Text = "Cerrar";
            this.CerrarBT.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.CerrarBT.UseVisualStyleBackColor = false;
            this.CerrarBT.Click += new System.EventHandler(this.CerrarBT_Click);
            // 
            // AgregarBT
            // 
            this.AgregarBT.BackColor = System.Drawing.Color.White;
            this.AgregarBT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AgregarBT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.AgregarBT.Image = global::Presentismo_2015.Properties.Resources.add1;
            this.AgregarBT.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.AgregarBT.Location = new System.Drawing.Point(760, 398);
            this.AgregarBT.Name = "AgregarBT";
            this.AgregarBT.Size = new System.Drawing.Size(56, 54);
            this.AgregarBT.TabIndex = 25;
            this.AgregarBT.Text = "Agregar";
            this.AgregarBT.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AgregarBT.UseVisualStyleBackColor = false;
            this.AgregarBT.Click += new System.EventHandler(this.AgregarBT_Click);
            // 
            // GroupBox1
            // 
            this.GroupBox1.BackColor = System.Drawing.Color.Honeydew;
            this.GroupBox1.Controls.Add(this.Label41);
            this.GroupBox1.Controls.Add(this.cmbCategory);
            this.GroupBox1.Location = new System.Drawing.Point(281, 97);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(300, 76);
            this.GroupBox1.TabIndex = 101;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Datos del cargo";
            // 
            // Label41
            // 
            this.Label41.AutoSize = true;
            this.Label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Label41.Location = new System.Drawing.Point(6, 25);
            this.Label41.Name = "Label41";
            this.Label41.Size = new System.Drawing.Size(67, 16);
            this.Label41.TabIndex = 92;
            this.Label41.Text = "Categoría";
            // 
            // cmbCategory
            // 
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(71, 24);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(223, 21);
            this.cmbCategory.TabIndex = 95;
            this.cmbCategory.SelectedIndexChanged += new System.EventHandler(this.cmbCategory_SelectedIndexChanged);
            // 
            // employeeIDTXT
            // 
            this.employeeIDTXT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.employeeIDTXT.Location = new System.Drawing.Point(95, 414);
            this.employeeIDTXT.Name = "employeeIDTXT";
            this.employeeIDTXT.Size = new System.Drawing.Size(10, 16);
            this.employeeIDTXT.TabIndex = 105;
            this.employeeIDTXT.Visible = false;
            // 
            // DataGridView1
            // 
            this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView1.Location = new System.Drawing.Point(200, 192);
            this.DataGridView1.Name = "DataGridView1";
            this.DataGridView1.Size = new System.Drawing.Size(472, 201);
            this.DataGridView1.TabIndex = 106;
            this.DataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(196, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(217, 24);
            this.label2.TabIndex = 108;
            this.label2.Text = "Cargos de Ignacio Ponte";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // Cargos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(842, 464);
            this.Controls.Add(this.AgregarBT);
            this.Controls.Add(this.CerrarBT);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DataGridView1);
            this.Controls.Add(this.employeeIDTXT);
            this.Controls.Add(this.GroupBox1);
            this.Name = "Cargos";
            this.Text = "Cargos";
            this.Load += new System.EventHandler(this.Cargos_Load);
            this.Click += new System.EventHandler(this.AgregarBT_Click);
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button CerrarBT;
        internal System.Windows.Forms.Button AgregarBT;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Label Label41;
        internal System.Windows.Forms.ComboBox cmbCategory;
        internal System.Windows.Forms.Label employeeIDTXT;
        internal System.Windows.Forms.DataGridView DataGridView1;
        private System.Windows.Forms.Label label2;
    }
}