﻿namespace Presentismo_2015.Relation
{
    partial class relationDefault
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows myCustomForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.courseCombo = new System.Windows.Forms.ComboBox();
            this.hourCombo = new System.Windows.Forms.ComboBox();
            this.dayCombo = new System.Windows.Forms.ComboBox();
            this.subjetCombo = new System.Windows.Forms.ComboBox();
            this.employeeCombo = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.deleteRelation = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(27, 73);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(776, 384);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Image = global::Presentismo_2015.Properties.Resources.add;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.Location = new System.Drawing.Point(739, 463);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(64, 60);
            this.button1.TabIndex = 1;
            this.button1.Text = "Agregar";
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(731, 35);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(72, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Buscar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Curso";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(147, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Asignatura";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(274, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Profesor";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(486, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Día";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(571, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Horario";
            // 
            // courseCombo
            // 
            this.courseCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.courseCombo.FormattingEnabled = true;
            this.courseCombo.Location = new System.Drawing.Point(27, 37);
            this.courseCombo.Name = "courseCombo";
            this.courseCombo.Size = new System.Drawing.Size(117, 21);
            this.courseCombo.TabIndex = 11;
            // 
            // hourCombo
            // 
            this.hourCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.hourCombo.FormattingEnabled = true;
            this.hourCombo.Location = new System.Drawing.Point(574, 37);
            this.hourCombo.Name = "hourCombo";
            this.hourCombo.Size = new System.Drawing.Size(95, 21);
            this.hourCombo.TabIndex = 12;
            // 
            // dayCombo
            // 
            this.dayCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dayCombo.FormattingEnabled = true;
            this.dayCombo.Location = new System.Drawing.Point(481, 37);
            this.dayCombo.Name = "dayCombo";
            this.dayCombo.Size = new System.Drawing.Size(86, 21);
            this.dayCombo.TabIndex = 13;
            // 
            // subjetCombo
            // 
            this.subjetCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.subjetCombo.FormattingEnabled = true;
            this.subjetCombo.Location = new System.Drawing.Point(150, 37);
            this.subjetCombo.Name = "subjetCombo";
            this.subjetCombo.Size = new System.Drawing.Size(121, 21);
            this.subjetCombo.TabIndex = 14;
            // 
            // employeeCombo
            // 
            this.employeeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.employeeCombo.FormattingEnabled = true;
            this.employeeCombo.Location = new System.Drawing.Point(277, 37);
            this.employeeCombo.Name = "employeeCombo";
            this.employeeCombo.Size = new System.Drawing.Size(198, 21);
            this.employeeCombo.TabIndex = 15;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.BackgroundImage = global::Presentismo_2015.Properties.Resources.door_out;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button3.Location = new System.Drawing.Point(27, 463);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(56, 65);
            this.button3.TabIndex = 313;
            this.button3.Text = "Cerrar";
            this.button3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // deleteRelation
            // 
            this.deleteRelation.BackColor = System.Drawing.Color.White;
            this.deleteRelation.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.deleteRelation.Image = global::Presentismo_2015.Properties.Resources.delete;
            this.deleteRelation.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.deleteRelation.Location = new System.Drawing.Point(669, 463);
            this.deleteRelation.Name = "deleteRelation";
            this.deleteRelation.Size = new System.Drawing.Size(64, 60);
            this.deleteRelation.TabIndex = 314;
            this.deleteRelation.Text = "Eliminar";
            this.deleteRelation.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.deleteRelation.UseVisualStyleBackColor = false;
            this.deleteRelation.Click += new System.EventHandler(this.button4_Click);
            // 
            // relationDefault
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(832, 535);
            this.Controls.Add(this.deleteRelation);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.employeeCombo);
            this.Controls.Add(this.subjetCombo);
            this.Controls.Add(this.dayCombo);
            this.Controls.Add(this.hourCombo);
            this.Controls.Add(this.courseCombo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "relationDefault";
            this.Text = "Relaciones";
            this.Load += new System.EventHandler(this.relationDefault_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox courseCombo;
        private System.Windows.Forms.ComboBox hourCombo;
        private System.Windows.Forms.ComboBox dayCombo;
        private System.Windows.Forms.ComboBox subjetCombo;
        private System.Windows.Forms.ComboBox employeeCombo;
        internal System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button deleteRelation;
    }
}