﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Dapper;

namespace Presentismo_2015.Relation
{
    public partial class relationAdd : myCustomForm
    {
        Employee formEmployee;
        public relationAdd(Employee logedEmployee)
        {
            InitializeComponent();
            formEmployee = logedEmployee;
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void relationAdd_Load(object sender, EventArgs e)
        {
            try
            {
                // string connectionString = "Server=tcp:oer8py276v.database.windows.net,1433;Database=Presentismo;User ID=proyecto2015@oer8py276v;Password=Colegiojuan23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                string select = "SELECT courseStr, courseID FROM dbo.TB_Courses WITH(NOLOCK)";
                //Connection c = new Connection();
                var cnn = new SqlConnection(Conection.connectionString);
                cnn.Open();
                SqlDataAdapter dataAdapter = new SqlDataAdapter(select, Conection.connectionString); //c.con is the connection string
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
                DataSet ds = new DataSet();
                dataAdapter.Fill(ds, "Course");

                courseCombo.DisplayMember = "courseStr";
                courseCombo.ValueMember = "courseID";
                courseCombo.DataSource = ds.Tables["Course"];
                ////////////////////////////////////////////////////////

                select = "SELECT subjetID, name FROM dbo.TB_Subjet WITH(NOLOCK)";
                dataAdapter = new SqlDataAdapter(select, Conection.connectionString);

                commandBuilder = new SqlCommandBuilder(dataAdapter);
                ds = new DataSet();
                dataAdapter.Fill(ds, "subjet");

                subjetCombo.DisplayMember = "name";
                subjetCombo.ValueMember = "subjetID";
                subjetCombo.DataSource = ds.Tables["subjet"];
                ///////////////////////////////////////////////////////////////

                select = "SELECT (RTRIM(name) + ', ' + RTRIM(lastname)) employeeName, employeeID FROM TB_Employees WITH(NOLOCK)";
                dataAdapter = new SqlDataAdapter(select, Conection.connectionString);

                commandBuilder = new SqlCommandBuilder(dataAdapter);
                ds = new DataSet();
                dataAdapter.Fill(ds, "Employee");

                employeeCombo.ValueMember = "employeeName";
                employeeCombo.ValueMember = "employeeID";
                employeeCombo.DataSource = ds.Tables["Employee"];
                ///////////////////////////////////////////////////////////////

                select = "SELECT hourID, shortDescc FROM dbo.TB_Hours WITH(NOLOCK) ";
                dataAdapter = new SqlDataAdapter(select, Conection.connectionString);

                commandBuilder = new SqlCommandBuilder(dataAdapter);
                ds = new DataSet();
                dataAdapter.Fill(ds, "Modules");

                hourCombo.DisplayMember = "shortDescc";
                hourCombo.ValueMember = "hourID";
                hourCombo.DataSource = ds.Tables["Modules"];
                ///////////////////////////////////////////////////////////////
                select = "SELECT dayID, dayDesc FROM dbo.TB_Day WITH(NOLOCK)";
                dataAdapter = new SqlDataAdapter(select, Conection.connectionString);

                commandBuilder = new SqlCommandBuilder(dataAdapter);
                ds = new DataSet();
                dataAdapter.Fill(ds, "Day");

                dayCombo.DisplayMember = "dayDesc";
                dayCombo.ValueMember = "dayID";
                dayCombo.DataSource = ds.Tables["Day"];
                ///////////////////////////////////////////////////////////////
                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void courseID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void AgregarBT_Click(object sender, EventArgs e)
        {
            int courseID = 0, subjetID = 0, employeeID = 0, dayID = 0, hourID = 0;

            if (!Int32.TryParse(courseCombo.SelectedValue.ToString(), out courseID))
            {
                MessageBox.Show("Seleccione el curso.");
                return;
            }
            if (!Int32.TryParse(subjetCombo.SelectedValue.ToString(), out subjetID))
            {
                MessageBox.Show("Seleccione la asignatura.");
                return;
            }
            if (!Int32.TryParse(employeeCombo.SelectedValue.ToString(), out employeeID))
            {
                MessageBox.Show("Seleccione el profesor.");
                return;
            }
            if (!Int32.TryParse(dayCombo.SelectedValue.ToString(), out dayID))
            {
                MessageBox.Show("Seleccione el día.");
                return;
            }
            if (!Int32.TryParse(hourCombo.SelectedValue.ToString(), out hourID))
            {
                MessageBox.Show("Seleccione la hora.");
                return;
            }

            try
            {
                var spParams = new DynamicParameters();
                spParams.Add("@courseID", courseID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@subjetID", subjetID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@employeeID", employeeID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@dayOfWeek", dayID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@classHourID", hourID, dbType: DbType.Int32, direction: ParameterDirection.Input);

                spParams.Add("@errorDesc", dbType: DbType.String, direction: ParameterDirection.Output, size: 30);

                spParams.Add("@RETURN_VALUE", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                using (var sqlConnection = new SqlConnection(Conection.connectionString))
                {

                    sqlConnection.Open();

                    sqlConnection.Execute("SP_Course_Subjet_Relation_Add", spParams, commandType: CommandType.StoredProcedure);
                    string errorDesc = spParams.Get<string>("@errorDesc");
                    int RC = spParams.Get<int>("@RETURN_VALUE");

                    if (RC != 1)
                    {
                        MessageBox.Show(String.Format("Error: {0} - Error N° :{1}", errorDesc, RC.ToString()));
                    }
                    else
                    {
                        MessageBox.Show("Relación dada de alta correctamente.");
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error conectandose a la base de datos.");
            }
        }

        private void CerrarBT_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void hourCombo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
