﻿namespace Presentismo_2015.Relation
{
    partial class relationAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows myCustomForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.courseCombo = new System.Windows.Forms.ComboBox();
            this.employeeCombo = new System.Windows.Forms.ComboBox();
            this.subjetCombo = new System.Windows.Forms.ComboBox();
            this.dayCombo = new System.Windows.Forms.ComboBox();
            this.hourCombo = new System.Windows.Forms.ComboBox();
            this.CerrarBT = new System.Windows.Forms.Button();
            this.AgregarBT = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Curso";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Asignatura";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Profesor";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Dia";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 175);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Horario";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // courseCombo
            // 
            this.courseCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.courseCombo.FormattingEnabled = true;
            this.courseCombo.Location = new System.Drawing.Point(228, 42);
            this.courseCombo.Name = "courseCombo";
            this.courseCombo.Size = new System.Drawing.Size(121, 21);
            this.courseCombo.TabIndex = 30;
            this.courseCombo.SelectedIndexChanged += new System.EventHandler(this.courseID_SelectedIndexChanged);
            // 
            // employeeCombo
            // 
            this.employeeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.employeeCombo.FormattingEnabled = true;
            this.employeeCombo.Location = new System.Drawing.Point(228, 107);
            this.employeeCombo.Name = "employeeCombo";
            this.employeeCombo.Size = new System.Drawing.Size(121, 21);
            this.employeeCombo.TabIndex = 31;
            this.employeeCombo.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // subjetCombo
            // 
            this.subjetCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.subjetCombo.FormattingEnabled = true;
            this.subjetCombo.Location = new System.Drawing.Point(228, 75);
            this.subjetCombo.Name = "subjetCombo";
            this.subjetCombo.Size = new System.Drawing.Size(121, 21);
            this.subjetCombo.TabIndex = 32;
            // 
            // dayCombo
            // 
            this.dayCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dayCombo.FormattingEnabled = true;
            this.dayCombo.Location = new System.Drawing.Point(228, 137);
            this.dayCombo.Name = "dayCombo";
            this.dayCombo.Size = new System.Drawing.Size(121, 21);
            this.dayCombo.TabIndex = 33;
            // 
            // hourCombo
            // 
            this.hourCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.hourCombo.FormattingEnabled = true;
            this.hourCombo.Location = new System.Drawing.Point(228, 169);
            this.hourCombo.Name = "hourCombo";
            this.hourCombo.Size = new System.Drawing.Size(121, 21);
            this.hourCombo.TabIndex = 34;
            this.hourCombo.SelectedIndexChanged += new System.EventHandler(this.hourCombo_SelectedIndexChanged);
            // 
            // CerrarBT
            // 
            this.CerrarBT.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.CerrarBT.BackColor = System.Drawing.Color.White;
            this.CerrarBT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.CerrarBT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.CerrarBT.Image = global::Presentismo_2015.Properties.Resources.door_out1;
            this.CerrarBT.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.CerrarBT.Location = new System.Drawing.Point(15, 243);
            this.CerrarBT.Name = "CerrarBT";
            this.CerrarBT.Size = new System.Drawing.Size(55, 69);
            this.CerrarBT.TabIndex = 29;
            this.CerrarBT.Tag = "";
            this.CerrarBT.Text = "Cerrar";
            this.CerrarBT.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.CerrarBT.UseVisualStyleBackColor = false;
            this.CerrarBT.Click += new System.EventHandler(this.CerrarBT_Click);
            // 
            // AgregarBT
            // 
            this.AgregarBT.BackColor = System.Drawing.Color.White;
            this.AgregarBT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AgregarBT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.AgregarBT.Image = global::Presentismo_2015.Properties.Resources.add1;
            this.AgregarBT.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.AgregarBT.Location = new System.Drawing.Point(293, 243);
            this.AgregarBT.Name = "AgregarBT";
            this.AgregarBT.Size = new System.Drawing.Size(56, 69);
            this.AgregarBT.TabIndex = 28;
            this.AgregarBT.Text = "Agregar";
            this.AgregarBT.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AgregarBT.UseVisualStyleBackColor = false;
            this.AgregarBT.Click += new System.EventHandler(this.AgregarBT_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.CerrarBT);
            this.groupBox1.Controls.Add(this.hourCombo);
            this.groupBox1.Controls.Add(this.AgregarBT);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dayCombo);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.subjetCombo);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.employeeCombo);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.courseCombo);
            this.groupBox1.Location = new System.Drawing.Point(22, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(378, 355);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ingrese los datos de la relación";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(355, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(16, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "*";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(355, 139);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(16, 18);
            this.label9.TabIndex = 38;
            this.label9.Text = "*";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(355, 170);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 18);
            this.label8.TabIndex = 37;
            this.label8.Text = "*";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(355, 110);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 18);
            this.label7.TabIndex = 36;
            this.label7.Text = "*";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(355, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 18);
            this.label6.TabIndex = 35;
            this.label6.Text = "*";
            // 
            // relationAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(414, 391);
            this.Controls.Add(this.groupBox1);
            this.Name = "relationAdd";
            this.Text = "Agregar relación";
            this.Load += new System.EventHandler(this.relationAdd_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        internal System.Windows.Forms.Button CerrarBT;
        internal System.Windows.Forms.Button AgregarBT;
        private System.Windows.Forms.ComboBox courseCombo;
        private System.Windows.Forms.ComboBox employeeCombo;
        private System.Windows.Forms.ComboBox subjetCombo;
        private System.Windows.Forms.ComboBox dayCombo;
        private System.Windows.Forms.ComboBox hourCombo;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label label7;
    }
}