﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Dapper;

namespace Presentismo_2015.Relation
{
    public partial class relationDefault : myCustomForm
    {

        Employee formEmployee;
        public relationDefault(Employee logedEmployee)
        {
            InitializeComponent();
            formEmployee = logedEmployee;

        }

        private void relationDefault_Load(object sender, EventArgs e)
        {
            try
            {
                // string connectionString = "Server=tcp:oer8py276v.database.windows.net,1433;Database=Presentismo;User ID=proyecto2015@oer8py276v;Password=Colegiojuan23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                string select = "SELECT courseID, courseStr FROM dbo.TB_Courses WITH(NOLOCK)";
                //Connection c = new Connection();
                var cnn = new SqlConnection(Conection.connectionString);
                cnn.Open();
                SqlDataAdapter dataAdapter = new SqlDataAdapter(select, Conection.connectionString); //c.con is the connection string
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
                DataSet ds = new DataSet();

                dataAdapter.Fill(ds, "Course");

                DataRow defValue = ds.Tables[0].NewRow();
                defValue[0] = 0;
                defValue[1] = "[Seleccione]";

                ds.Tables[0].DefaultView.Sort = "courseStr";

                ds.Tables[0].Rows.Add(defValue);

                courseCombo.DisplayMember = "courseStr";
                courseCombo.ValueMember = "courseID";
                courseCombo.DataSource = ds.Tables["Course"];
     

                ////////////////////////////////////////////////////////

                select = "SELECT subjetID, name FROM dbo.TB_Subjet WITH(NOLOCK)";
                dataAdapter = new SqlDataAdapter(select, Conection.connectionString);

                commandBuilder = new SqlCommandBuilder(dataAdapter);
                ds = new DataSet();
                dataAdapter.Fill(ds, "subjet");

                defValue = ds.Tables[0].NewRow();
                defValue[0] = 0;
                defValue[1] = "[Seleccione]";
                ds.Tables[0].DefaultView.Sort = "name";
                ds.Tables[0].Rows.Add(defValue);

                subjetCombo.DisplayMember = "name";
                subjetCombo.ValueMember = "subjetID";
                subjetCombo.DataSource = ds.Tables["subjet"];


                ///////////////////////////////////////////////////////////////

                select = "SELECT employeeID, employeeName  FROM VW_Employees WITH(NOLOCK)";
                dataAdapter = new SqlDataAdapter(select, Conection.connectionString);

                commandBuilder = new SqlCommandBuilder(dataAdapter);
                ds = new DataSet();
                dataAdapter.Fill(ds, "Employee");

                defValue = ds.Tables[0].NewRow();
                defValue[0] = 0;
                defValue[1] = "[Seleccione]";
                ds.Tables[0].DefaultView.Sort = "employeeName";

                ds.Tables[0].Rows.Add(defValue);

                employeeCombo.ValueMember = "employeeName";
                employeeCombo.ValueMember = "employeeID";
                employeeCombo.DataSource = ds.Tables["Employee"];

                ///////////////////////////////////////////////////////////////

                select = "SELECT hourID, shortDescc FROM dbo.TB_Hours WITH(NOLOCK) ORDER BY hourID";
                dataAdapter = new SqlDataAdapter(select, Conection.connectionString);

                commandBuilder = new SqlCommandBuilder(dataAdapter);
                ds = new DataSet();
                dataAdapter.Fill(ds, "Modules");

                defValue = ds.Tables[0].NewRow();
                defValue[0] = 0;
                defValue[1] = "[Seleccione]";
                ds.Tables[0].DefaultView.Sort = "hourID";

                ds.Tables[0].Rows.Add(defValue);

                hourCombo.DisplayMember = "shortDescc";
                hourCombo.ValueMember = "hourID";
                hourCombo.DataSource = ds.Tables["Modules"];

                ///////////////////////////////////////////////////////////////
                select = "SELECT dayID, dayDesc FROM dbo.TB_Day WITH(NOLOCK) ORDER BY dayID";
                dataAdapter = new SqlDataAdapter(select, Conection.connectionString);

                commandBuilder = new SqlCommandBuilder(dataAdapter);
                ds = new DataSet();
                dataAdapter.Fill(ds, "Day");

                defValue = ds.Tables[0].NewRow();
                defValue[0] = 0;
                defValue[1] = "[Seleccione]";
                ds.Tables[0].DefaultView.Sort = "dayID";

                ds.Tables[0].Rows.Add(defValue);

                dayCombo.DisplayMember = "dayDesc";
                dayCombo.ValueMember = "dayID";
                dayCombo.DataSource = ds.Tables["Day"];

                ///////////////////////////////////////////////////////////////
                cnn.Close();
          
                doSearch();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un error conectandose a la base de datos");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Relation.relationAdd relationAdd = new Relation.relationAdd(formEmployee);
            relationAdd.Show();
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            doSearch();
        }

        private void doSearch()
        {
            int courseID = 0, subjetID = 0, employeeID = 0, dayID = 0, hourID = 0;

            if (!Int32.TryParse(courseCombo.SelectedValue.ToString(), out courseID))
            {
                MessageBox.Show("Seleccione el curso.");
                return;
            }
            if (!Int32.TryParse(subjetCombo.SelectedValue.ToString(), out subjetID))
            {
                MessageBox.Show("Seleccione la asignatura.");
                return;
            }
            if (!Int32.TryParse(employeeCombo.SelectedValue.ToString(), out employeeID))
            {
                MessageBox.Show("Seleccione el profesor.");
                return;
            }
            if (!Int32.TryParse(dayCombo.SelectedValue.ToString(), out dayID))
            {
                MessageBox.Show("Seleccione el día.");
                return;
            }
            if (!Int32.TryParse(hourCombo.SelectedValue.ToString(), out hourID))
            {
                MessageBox.Show("Seleccione la hora.");
                return;
            }

            var spParams = new DynamicParameters();
            spParams.Add("@courseID", courseID, dbType: DbType.Int32, direction: ParameterDirection.Input);
            spParams.Add("@employeeID", employeeID, dbType: DbType.Int32, direction: ParameterDirection.Input);
            spParams.Add("@subjetID", subjetID, dbType: DbType.Int32, direction: ParameterDirection.Input);
            spParams.Add("@dayOfWeek", dayID, dbType: DbType.Int32, direction: ParameterDirection.Input);
            spParams.Add("@hourID", hourID, dbType: DbType.Int32, direction: ParameterDirection.Input);

            using (var sqlConnection = new SqlConnection(Conection.connectionString))
            {
                sqlConnection.Open();
                IDataReader rd;
                DataTable dt = new DataTable();

                rd = sqlConnection.ExecuteReader("SP_Course_Subjet_Relation_Search", spParams, commandType: CommandType.StoredProcedure);

                dt.Load(rd);

                dataGridView1.AutoGenerateColumns = true;
                dataGridView1.DataSource = dt;
                dataGridView1.Refresh();
                dataGridView1.ReadOnly = true;
                dataGridView1.AllowUserToAddRows = false;
                dataGridView1.AllowUserToDeleteRows = false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try {

                if (dataGridView1.SelectedRows.Count != 1)
                {
                    DataGridViewRow row = dataGridView1.Rows[dataGridView1.SelectedCells[0].RowIndex];

                    int relationID = Int32.Parse(row.Cells["ID"].Value.ToString());
               
                    DialogResult dialogResult = MessageBox.Show("¿Esta seguro que desea eliminar el horario de clase? Recuerde que este proceso no puede deshacerse.", "Eliminar horario de clase", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        var spParams = new DynamicParameters();
                        spParams.Add("@userID", formEmployee.employeeID , dbType: DbType.Int32, direction: ParameterDirection.Input);
                        spParams.Add("@relationID", relationID, dbType: DbType.Int32, direction: ParameterDirection.Input);

                        spParams.Add("@errorDesc", dbType: DbType.String, direction: ParameterDirection.Output, size: 30);

                        spParams.Add("@RETURN_VALUE", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                        using (var sqlConnection = new SqlConnection(Conection.connectionString))
                        {

                            sqlConnection.Open();

                            sqlConnection.Execute("SP_Course_Subjet_Relation_Delete", spParams, commandType: CommandType.StoredProcedure);
                            string errorDesc = spParams.Get<string>("@errorDesc");
                            int RC = spParams.Get<int>("@RETURN_VALUE");

                            if (RC != 1)
                            {
                                MessageBox.Show(String.Format("Error: {0} - Error N° :{1}", errorDesc, RC.ToString()));
                            }
                            else
                            {
                                MessageBox.Show("Horario de clase eliminado correctamente.");
                                doSearch();
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Para editar un horario de clase eliminado  debe tener seleccionada solo una.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error conectandose a la base de datos.");
            }
        }
    }
}
