﻿namespace Presentismo_2015
{
    partial class Busqueda_de_personal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows myCustomForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label38 = new System.Windows.Forms.Label();
            this.Label17 = new System.Windows.Forms.Label();
            this.Label25 = new System.Windows.Forms.Label();
            this.NodisponibleLBL = new System.Windows.Forms.Label();
            this.Label31 = new System.Windows.Forms.Label();
            this.NivelAcaCB = new System.Windows.Forms.ComboBox();
            this.TituloCB = new System.Windows.Forms.ComboBox();
            this.EspecialidadCB = new System.Windows.Forms.ComboBox();
            this.Label23 = new System.Windows.Forms.Label();
            this.Label39 = new System.Windows.Forms.Label();
            this.ObraSocialCB = new System.Windows.Forms.ComboBox();
            this.EmailTXT = new System.Windows.Forms.TextBox();
            this.AlergiasCB = new System.Windows.Forms.ComboBox();
            this.Label32 = new System.Windows.Forms.Label();
            this.Label36 = new System.Windows.Forms.Label();
            this.Label34 = new System.Windows.Forms.Label();
            this.GrupoSangCB = new System.Windows.Forms.ComboBox();
            this.EnfermedCB = new System.Windows.Forms.ComboBox();
            this.Label10 = new System.Windows.Forms.Label();
            this.Label20 = new System.Windows.Forms.Label();
            this.Label33 = new System.Windows.Forms.Label();
            this.GroupBox3 = new System.Windows.Forms.GroupBox();
            this.Label24 = new System.Windows.Forms.Label();
            this.SeguroTXT = new System.Windows.Forms.TextBox();
            this.Label35 = new System.Windows.Forms.Label();
            this.sexoCB = new System.Windows.Forms.ComboBox();
            this.Label16 = new System.Windows.Forms.Label();
            this.ApellidoTXT = new System.Windows.Forms.TextBox();
            this.NombreTXT = new System.Windows.Forms.TextBox();
            this.dniTXT = new System.Windows.Forms.TextBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.Label40 = new System.Windows.Forms.Label();
            this.Label30 = new System.Windows.Forms.Label();
            this.Label29 = new System.Windows.Forms.Label();
            this.Label28 = new System.Windows.Forms.Label();
            this.Label27 = new System.Windows.Forms.Label();
            this.Label26 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.NacionCB = new System.Windows.Forms.ComboBox();
            this.CodPostCB = new System.Windows.Forms.ComboBox();
            this.PciaCB = new System.Windows.Forms.ComboBox();
            this.LocCB = new System.Windows.Forms.ComboBox();
            this.ocupTXT = new System.Windows.Forms.TextBox();
            this.Label19 = new System.Windows.Forms.Label();
            this.estcivilCB = new System.Windows.Forms.ComboBox();
            this.Label18 = new System.Windows.Forms.Label();
            this.lugtrabaTXT = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.fechaDTP = new System.Windows.Forms.DateTimePicker();
            this.CelTXT = new System.Windows.Forms.TextBox();
            this.Label15 = new System.Windows.Forms.Label();
            this.TelTXT = new System.Windows.Forms.TextBox();
            this.Label14 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.Label12 = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.DirTXT = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.ImagenIMG = new System.Windows.Forms.PictureBox();
            this.Label37 = new System.Windows.Forms.Label();
            this.group2 = new System.Windows.Forms.GroupBox();
            this.CerrarBT = new System.Windows.Forms.Button();
            this.Button2 = new System.Windows.Forms.Button();
            this.Button1 = new System.Windows.Forms.Button();
            this.AgregarBT = new System.Windows.Forms.Button();
            this.Label21 = new System.Windows.Forms.Label();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ToolStripLabel1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.ActualizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AgregarAltaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AsignaturasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DIPREGEPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DIPREGEP4491TecnicaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EditarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EliminarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.SecundariToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BasicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TecnicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AmbosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.ImprimirTSB = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.Label22 = new System.Windows.Forms.Label();
            this.GroupBox3.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImagenIMG)).BeginInit();
            this.group2.SuspendLayout();
            this.ToolStrip1.SuspendLayout();
            this.GroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label38
            // 
            this.Label38.AutoSize = true;
            this.Label38.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label38.ForeColor = System.Drawing.Color.Red;
            this.Label38.Location = new System.Drawing.Point(417, 536);
            this.Label38.Name = "Label38";
            this.Label38.Size = new System.Drawing.Size(211, 16);
            this.Label38.TabIndex = 89;
            this.Label38.Text = "el ingreso de datos es obligatorio";
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label17.Location = new System.Drawing.Point(19, 370);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(93, 16);
            this.Label17.TabIndex = 0;
            this.Label17.Text = "Seguro Social";
            // 
            // Label25
            // 
            this.Label25.AutoSize = true;
            this.Label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label25.Location = new System.Drawing.Point(26, 69);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(88, 16);
            this.Label25.TabIndex = 64;
            this.Label25.Text = "Especialidad";
            // 
            // NodisponibleLBL
            // 
            this.NodisponibleLBL.AutoSize = true;
            this.NodisponibleLBL.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NodisponibleLBL.Location = new System.Drawing.Point(246, 57);
            this.NodisponibleLBL.Name = "NodisponibleLBL";
            this.NodisponibleLBL.Size = new System.Drawing.Size(107, 16);
            this.NodisponibleLBL.TabIndex = 18;
            this.NodisponibleLBL.Text = "No Disponible";
            // 
            // Label31
            // 
            this.Label31.AutoSize = true;
            this.Label31.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label31.ForeColor = System.Drawing.Color.Red;
            this.Label31.Location = new System.Drawing.Point(236, 43);
            this.Label31.Name = "Label31";
            this.Label31.Size = new System.Drawing.Size(16, 18);
            this.Label31.TabIndex = 0;
            this.Label31.Text = "*";
            // 
            // NivelAcaCB
            // 
            this.NivelAcaCB.BackColor = System.Drawing.Color.White;
            this.NivelAcaCB.DisplayMember = "sss";
            this.NivelAcaCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.NivelAcaCB.FormattingEnabled = true;
            this.NivelAcaCB.Items.AddRange(new object[] {
            "Nivel Primario",
            "Nivel Secundario",
            "Nivel Terciario"});
            this.NivelAcaCB.Location = new System.Drawing.Point(132, 41);
            this.NivelAcaCB.Name = "NivelAcaCB";
            this.NivelAcaCB.Size = new System.Drawing.Size(102, 21);
            this.NivelAcaCB.TabIndex = 18;
            // 
            // TituloCB
            // 
            this.TituloCB.BackColor = System.Drawing.Color.White;
            this.TituloCB.FormattingEnabled = true;
            this.TituloCB.Items.AddRange(new object[] {
            "Profesor Matematica",
            "Profesor Quimica",
            "Profesor Sociales",
            "Profesor Historia",
            "Mestro de Grado"});
            this.TituloCB.Location = new System.Drawing.Point(132, 96);
            this.TituloCB.Name = "TituloCB";
            this.TituloCB.Size = new System.Drawing.Size(102, 21);
            this.TituloCB.TabIndex = 20;
            // 
            // EspecialidadCB
            // 
            this.EspecialidadCB.BackColor = System.Drawing.Color.White;
            this.EspecialidadCB.FormattingEnabled = true;
            this.EspecialidadCB.Items.AddRange(new object[] {
            "Matematica",
            "Lengua",
            "Biologia",
            "Programacion",
            "Fisica",
            "Quimica",
            "Contabilidad",
            "Economia",
            "Sociales",
            "Ed. Fisica",
            "Civica",
            "Sistemas"});
            this.EspecialidadCB.Location = new System.Drawing.Point(132, 68);
            this.EspecialidadCB.Name = "EspecialidadCB";
            this.EspecialidadCB.Size = new System.Drawing.Size(102, 21);
            this.EspecialidadCB.TabIndex = 19;
            // 
            // Label23
            // 
            this.Label23.AutoSize = true;
            this.Label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label23.Location = new System.Drawing.Point(29, 99);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(41, 16);
            this.Label23.TabIndex = 66;
            this.Label23.Text = "Título";
            // 
            // Label39
            // 
            this.Label39.AutoSize = true;
            this.Label39.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label39.ForeColor = System.Drawing.Color.Red;
            this.Label39.Location = new System.Drawing.Point(236, 69);
            this.Label39.Name = "Label39";
            this.Label39.Size = new System.Drawing.Size(16, 18);
            this.Label39.TabIndex = 81;
            this.Label39.Text = "*";
            // 
            // ObraSocialCB
            // 
            this.ObraSocialCB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ObraSocialCB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ObraSocialCB.FormattingEnabled = true;
            this.ObraSocialCB.Items.AddRange(new object[] {
            "ASE  ",
            "OSAPYEA  ",
            "APDIS  ",
            "APSOT  ",
            "OSBA  ",
            "OSEIV  ",
            "OSADEF  ",
            "OSAC  ",
            "OSA  ",
            "OSALARA  ",
            "OSAPM  ",
            "OSADRA  ",
            "OSCEP  ",
            "OSCOMM  ",
            "OSCE  ",
            "OSCHOCA ",
            "OSCAMGLYP ",
            "OSCN OBRA  ",
            "OSCCPTAC  ",
            "OSCRAIA  ",
            "CTCP  ",
            "OSCONARA  ",
            "OSDO  ",
            "WITCEL  ",
            "OSDOP  ",
            "OSEDA  ",
            "OSEMM  ",
            "OSETRA ",
            "OSETYA ",
            "OSEAM ",
            "OSFOT ",
            "OSGMGM ",
            "OSJPVYF ",
            "OSJOMN",
            "OSSEG ",
            "OSAM ",
            "OSFATUN ",
            "FEDECAMARAS ",
            "OSFGPICD ",
            "OSIPA ",
            "OSUOMRA ",
            "OSDEL ",
            "OSSIMRA ",
            "OSUCI",
            "NO TIENE",
            "NO SABE"});
            this.ObraSocialCB.Location = new System.Drawing.Point(132, 65);
            this.ObraSocialCB.Name = "ObraSocialCB";
            this.ObraSocialCB.Size = new System.Drawing.Size(103, 21);
            this.ObraSocialCB.TabIndex = 22;
            // 
            // EmailTXT
            // 
            this.EmailTXT.Location = new System.Drawing.Point(113, 343);
            this.EmailTXT.Name = "EmailTXT";
            this.EmailTXT.Size = new System.Drawing.Size(103, 20);
            this.EmailTXT.TabIndex = 13;
            this.EmailTXT.Tag = "";
            // 
            // AlergiasCB
            // 
            this.AlergiasCB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.AlergiasCB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.AlergiasCB.FormattingEnabled = true;
            this.AlergiasCB.Items.AddRange(new object[] {
            "ALIMENTOS",
            "POLEN",
            "POLVO",
            "ANIMALES",
            "HUMEDAD",
            "MEDICAMENTOS",
            "INSECTOS",
            "SOL",
            "NÍQUEL",
            "LATEX",
            "ANISAKIS (Pescado)",
            "NO TIENE",
            "NO SABE"});
            this.AlergiasCB.Location = new System.Drawing.Point(132, 38);
            this.AlergiasCB.Name = "AlergiasCB";
            this.AlergiasCB.Size = new System.Drawing.Size(103, 21);
            this.AlergiasCB.TabIndex = 21;
            // 
            // Label32
            // 
            this.Label32.AutoSize = true;
            this.Label32.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label32.ForeColor = System.Drawing.Color.Red;
            this.Label32.Location = new System.Drawing.Point(236, 71);
            this.Label32.Name = "Label32";
            this.Label32.Size = new System.Drawing.Size(16, 18);
            this.Label32.TabIndex = 0;
            this.Label32.Text = "*";
            // 
            // Label36
            // 
            this.Label36.AutoSize = true;
            this.Label36.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label36.ForeColor = System.Drawing.Color.Red;
            this.Label36.Location = new System.Drawing.Point(237, 129);
            this.Label36.Name = "Label36";
            this.Label36.Size = new System.Drawing.Size(16, 18);
            this.Label36.TabIndex = 80;
            this.Label36.Text = "*";
            // 
            // Label34
            // 
            this.Label34.AutoSize = true;
            this.Label34.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label34.ForeColor = System.Drawing.Color.Red;
            this.Label34.Location = new System.Drawing.Point(237, 42);
            this.Label34.Name = "Label34";
            this.Label34.Size = new System.Drawing.Size(16, 18);
            this.Label34.TabIndex = 78;
            this.Label34.Text = "*";
            // 
            // GrupoSangCB
            // 
            this.GrupoSangCB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.GrupoSangCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GrupoSangCB.FormattingEnabled = true;
            this.GrupoSangCB.Items.AddRange(new object[] {
            "O-",
            "O+",
            "A-",
            "A+",
            "B-",
            "B+",
            "AB-",
            "AB+",
            "No Sabe"});
            this.GrupoSangCB.Location = new System.Drawing.Point(132, 126);
            this.GrupoSangCB.Name = "GrupoSangCB";
            this.GrupoSangCB.Size = new System.Drawing.Size(103, 21);
            this.GrupoSangCB.TabIndex = 24;
            // 
            // EnfermedCB
            // 
            this.EnfermedCB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.EnfermedCB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.EnfermedCB.FormattingEnabled = true;
            this.EnfermedCB.Items.AddRange(new object[] {
            "Asma",
            "Anorexia",
            "Bulimia",
            "Obesidad",
            "Diabetes",
            "No tiene",
            "No sabe"});
            this.EnfermedCB.Location = new System.Drawing.Point(132, 97);
            this.EnfermedCB.Name = "EnfermedCB";
            this.EnfermedCB.Size = new System.Drawing.Size(103, 21);
            this.EnfermedCB.TabIndex = 23;
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label10.Location = new System.Drawing.Point(13, 126);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(113, 16);
            this.Label10.TabIndex = 66;
            this.Label10.Text = "Grupo Sanguineo";
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label20.Location = new System.Drawing.Point(29, 43);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(58, 16);
            this.Label20.TabIndex = 65;
            this.Label20.Text = "Alergias";
            // 
            // Label33
            // 
            this.Label33.AutoSize = true;
            this.Label33.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label33.ForeColor = System.Drawing.Color.Red;
            this.Label33.Location = new System.Drawing.Point(236, 97);
            this.Label33.Name = "Label33";
            this.Label33.Size = new System.Drawing.Size(16, 18);
            this.Label33.TabIndex = 0;
            this.Label33.Text = "*";
            // 
            // GroupBox3
            // 
            this.GroupBox3.BackColor = System.Drawing.Color.Honeydew;
            this.GroupBox3.Controls.Add(this.Label33);
            this.GroupBox3.Controls.Add(this.Label32);
            this.GroupBox3.Controls.Add(this.Label31);
            this.GroupBox3.Controls.Add(this.NivelAcaCB);
            this.GroupBox3.Controls.Add(this.TituloCB);
            this.GroupBox3.Controls.Add(this.EspecialidadCB);
            this.GroupBox3.Controls.Add(this.Label23);
            this.GroupBox3.Controls.Add(this.Label24);
            this.GroupBox3.Controls.Add(this.Label25);
            this.GroupBox3.Location = new System.Drawing.Point(430, 91);
            this.GroupBox3.Name = "GroupBox3";
            this.GroupBox3.Size = new System.Drawing.Size(265, 186);
            this.GroupBox3.TabIndex = 93;
            this.GroupBox3.TabStop = false;
            this.GroupBox3.Text = "Datos Academicos Profesor";
            // 
            // Label24
            // 
            this.Label24.AutoSize = true;
            this.Label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label24.Location = new System.Drawing.Point(13, 43);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(110, 16);
            this.Label24.TabIndex = 65;
            this.Label24.Text = "Nivel academico";
            // 
            // SeguroTXT
            // 
            this.SeguroTXT.BackColor = System.Drawing.Color.White;
            this.SeguroTXT.Location = new System.Drawing.Point(113, 369);
            this.SeguroTXT.Name = "SeguroTXT";
            this.SeguroTXT.Size = new System.Drawing.Size(103, 20);
            this.SeguroTXT.TabIndex = 14;
            this.SeguroTXT.Tag = "";
            // 
            // Label35
            // 
            this.Label35.AutoSize = true;
            this.Label35.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label35.ForeColor = System.Drawing.Color.Red;
            this.Label35.Location = new System.Drawing.Point(237, 97);
            this.Label35.Name = "Label35";
            this.Label35.Size = new System.Drawing.Size(16, 18);
            this.Label35.TabIndex = 79;
            this.Label35.Text = "*";
            // 
            // sexoCB
            // 
            this.sexoCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sexoCB.FormattingEnabled = true;
            this.sexoCB.Location = new System.Drawing.Point(113, 134);
            this.sexoCB.Name = "sexoCB";
            this.sexoCB.Size = new System.Drawing.Size(103, 21);
            this.sexoCB.TabIndex = 5;
            this.sexoCB.Tag = "";
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label16.Location = new System.Drawing.Point(20, 344);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(42, 16);
            this.Label16.TabIndex = 0;
            this.Label16.Text = "Email";
            // 
            // ApellidoTXT
            // 
            this.ApellidoTXT.Location = new System.Drawing.Point(113, 83);
            this.ApellidoTXT.Name = "ApellidoTXT";
            this.ApellidoTXT.Size = new System.Drawing.Size(103, 20);
            this.ApellidoTXT.TabIndex = 3;
            this.ApellidoTXT.Tag = "";
            // 
            // NombreTXT
            // 
            this.NombreTXT.BackColor = System.Drawing.Color.White;
            this.NombreTXT.Location = new System.Drawing.Point(113, 57);
            this.NombreTXT.Name = "NombreTXT";
            this.NombreTXT.Size = new System.Drawing.Size(103, 20);
            this.NombreTXT.TabIndex = 2;
            this.NombreTXT.Tag = "";
            // 
            // dniTXT
            // 
            this.dniTXT.Location = new System.Drawing.Point(113, 31);
            this.dniTXT.MaxLength = 8;
            this.dniTXT.Name = "dniTXT";
            this.dniTXT.Size = new System.Drawing.Size(103, 20);
            this.dniTXT.TabIndex = 1;
            this.dniTXT.Tag = "";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.Location = new System.Drawing.Point(20, 162);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(65, 16);
            this.Label7.TabIndex = 0;
            this.Label7.Text = "Dirección";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(20, 136);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(39, 16);
            this.Label6.TabIndex = 0;
            this.Label6.Text = "Sexo";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.Location = new System.Drawing.Point(20, 32);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(31, 16);
            this.Label5.TabIndex = 0;
            this.Label5.Text = "DNI";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(20, 84);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(58, 16);
            this.Label3.TabIndex = 0;
            this.Label3.Text = "Apellido";
            // 
            // GroupBox1
            // 
            this.GroupBox1.BackColor = System.Drawing.Color.Honeydew;
            this.GroupBox1.Controls.Add(this.NodisponibleLBL);
            this.GroupBox1.Controls.Add(this.Label40);
            this.GroupBox1.Controls.Add(this.Label30);
            this.GroupBox1.Controls.Add(this.Label29);
            this.GroupBox1.Controls.Add(this.Label28);
            this.GroupBox1.Controls.Add(this.Label27);
            this.GroupBox1.Controls.Add(this.Label26);
            this.GroupBox1.Controls.Add(this.Label4);
            this.GroupBox1.Controls.Add(this.NacionCB);
            this.GroupBox1.Controls.Add(this.CodPostCB);
            this.GroupBox1.Controls.Add(this.PciaCB);
            this.GroupBox1.Controls.Add(this.LocCB);
            this.GroupBox1.Controls.Add(this.ocupTXT);
            this.GroupBox1.Controls.Add(this.Label19);
            this.GroupBox1.Controls.Add(this.estcivilCB);
            this.GroupBox1.Controls.Add(this.Label18);
            this.GroupBox1.Controls.Add(this.lugtrabaTXT);
            this.GroupBox1.Controls.Add(this.Label2);
            this.GroupBox1.Controls.Add(this.fechaDTP);
            this.GroupBox1.Controls.Add(this.sexoCB);
            this.GroupBox1.Controls.Add(this.SeguroTXT);
            this.GroupBox1.Controls.Add(this.Label17);
            this.GroupBox1.Controls.Add(this.EmailTXT);
            this.GroupBox1.Controls.Add(this.Label16);
            this.GroupBox1.Controls.Add(this.CelTXT);
            this.GroupBox1.Controls.Add(this.Label15);
            this.GroupBox1.Controls.Add(this.TelTXT);
            this.GroupBox1.Controls.Add(this.Label14);
            this.GroupBox1.Controls.Add(this.Label13);
            this.GroupBox1.Controls.Add(this.Label12);
            this.GroupBox1.Controls.Add(this.Label11);
            this.GroupBox1.Controls.Add(this.Label8);
            this.GroupBox1.Controls.Add(this.DirTXT);
            this.GroupBox1.Controls.Add(this.ApellidoTXT);
            this.GroupBox1.Controls.Add(this.NombreTXT);
            this.GroupBox1.Controls.Add(this.dniTXT);
            this.GroupBox1.Controls.Add(this.Label7);
            this.GroupBox1.Controls.Add(this.Label6);
            this.GroupBox1.Controls.Add(this.Label9);
            this.GroupBox1.Controls.Add(this.Label5);
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Controls.Add(this.ImagenIMG);
            this.GroupBox1.Location = new System.Drawing.Point(12, 91);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(358, 533);
            this.GroupBox1.TabIndex = 90;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Datos Profesor";
            // 
            // Label40
            // 
            this.Label40.AutoSize = true;
            this.Label40.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label40.ForeColor = System.Drawing.Color.Red;
            this.Label40.Location = new System.Drawing.Point(218, 319);
            this.Label40.Name = "Label40";
            this.Label40.Size = new System.Drawing.Size(16, 18);
            this.Label40.TabIndex = 0;
            this.Label40.Text = "*";
            // 
            // Label30
            // 
            this.Label30.AutoSize = true;
            this.Label30.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label30.ForeColor = System.Drawing.Color.Red;
            this.Label30.Location = new System.Drawing.Point(218, 371);
            this.Label30.Name = "Label30";
            this.Label30.Size = new System.Drawing.Size(16, 18);
            this.Label30.TabIndex = 0;
            this.Label30.Text = "*";
            // 
            // Label29
            // 
            this.Label29.AutoSize = true;
            this.Label29.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label29.ForeColor = System.Drawing.Color.Red;
            this.Label29.Location = new System.Drawing.Point(218, 58);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(16, 18);
            this.Label29.TabIndex = 0;
            this.Label29.Text = "*";
            // 
            // Label28
            // 
            this.Label28.AutoSize = true;
            this.Label28.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label28.ForeColor = System.Drawing.Color.Red;
            this.Label28.Location = new System.Drawing.Point(218, 85);
            this.Label28.Name = "Label28";
            this.Label28.Size = new System.Drawing.Size(16, 18);
            this.Label28.TabIndex = 0;
            this.Label28.Text = "*";
            // 
            // Label27
            // 
            this.Label27.AutoSize = true;
            this.Label27.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label27.ForeColor = System.Drawing.Color.Red;
            this.Label27.Location = new System.Drawing.Point(218, 163);
            this.Label27.Name = "Label27";
            this.Label27.Size = new System.Drawing.Size(16, 18);
            this.Label27.TabIndex = 0;
            this.Label27.Text = "*";
            // 
            // Label26
            // 
            this.Label26.AutoSize = true;
            this.Label26.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label26.ForeColor = System.Drawing.Color.Red;
            this.Label26.Location = new System.Drawing.Point(218, 293);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(16, 18);
            this.Label26.TabIndex = 0;
            this.Label26.Text = "*";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.ForeColor = System.Drawing.Color.Red;
            this.Label4.Location = new System.Drawing.Point(218, 33);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(16, 18);
            this.Label4.TabIndex = 0;
            this.Label4.Text = "*";
            // 
            // NacionCB
            // 
            this.NacionCB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.NacionCB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.NacionCB.FormattingEnabled = true;
            this.NacionCB.Items.AddRange(new object[] {
            "argentino ",
            "boliviano ",
            "chileno ",
            "colombiano ",
            "costarricense ",
            "cubano someone ",
            "dominicano ",
            "ecuatoriano ",
            "española ",
            "estadounidense ",
            "hondureño ",
            "mexicano ",
            "nicaragüense ",
            "panameño ",
            "paraguayo ",
            "peruano ",
            "puertorriqueño ",
            "salvadoreño ",
            "uruguayo ",
            "venezolana ",
            "guatemalteco"});
            this.NacionCB.Location = new System.Drawing.Point(114, 266);
            this.NacionCB.Name = "NacionCB";
            this.NacionCB.Size = new System.Drawing.Size(101, 21);
            this.NacionCB.TabIndex = 10;
            // 
            // CodPostCB
            // 
            this.CodPostCB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CodPostCB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CodPostCB.FormattingEnabled = true;
            this.CodPostCB.Items.AddRange(new object[] {
            "1712",
            "1886"});
            this.CodPostCB.Location = new System.Drawing.Point(114, 239);
            this.CodPostCB.Name = "CodPostCB";
            this.CodPostCB.Size = new System.Drawing.Size(101, 21);
            this.CodPostCB.TabIndex = 9;
            // 
            // PciaCB
            // 
            this.PciaCB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.PciaCB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.PciaCB.FormattingEnabled = true;
            this.PciaCB.Items.AddRange(new object[] {
            "Buenos Aires\t",
            "Catamarca\t",
            "Chaco\t",
            "Chubut\t",
            "Córdoba\t",
            "Corrientes\t",
            "Entre Ríos\t",
            "Formosa\t",
            "Jujuy\t",
            "La Pampa\t",
            "La Rioja\t",
            "Mendoza\t",
            "Misiones\t",
            "Neuquén\t",
            "Río Negro\t",
            "Salta\t",
            "San Juan\t",
            "San Luis\t",
            "Santa Cruz\t",
            "Santa Fe\t",
            "Santiago del Estero\t",
            "Tierra del Fuego\t",
            "Tucumán\t"});
            this.PciaCB.Location = new System.Drawing.Point(114, 212);
            this.PciaCB.Name = "PciaCB";
            this.PciaCB.Size = new System.Drawing.Size(101, 21);
            this.PciaCB.TabIndex = 8;
            // 
            // LocCB
            // 
            this.LocCB.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.LocCB.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.LocCB.FormattingEnabled = true;
            this.LocCB.Items.AddRange(new object[] {
            "Ayacucho",
            "Alberti",
            "Moron",
            "Castelar",
            "Ituzaingo"});
            this.LocCB.Location = new System.Drawing.Point(114, 185);
            this.LocCB.Name = "LocCB";
            this.LocCB.Size = new System.Drawing.Size(101, 21);
            this.LocCB.TabIndex = 7;
            // 
            // ocupTXT
            // 
            this.ocupTXT.BackColor = System.Drawing.Color.White;
            this.ocupTXT.Location = new System.Drawing.Point(113, 421);
            this.ocupTXT.Name = "ocupTXT";
            this.ocupTXT.Size = new System.Drawing.Size(103, 20);
            this.ocupTXT.TabIndex = 16;
            this.ocupTXT.Tag = "";
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label19.Location = new System.Drawing.Point(19, 422);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(73, 16);
            this.Label19.TabIndex = 0;
            this.Label19.Text = "Ocupacion";
            // 
            // estcivilCB
            // 
            this.estcivilCB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.estcivilCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.estcivilCB.FormattingEnabled = true;
            this.estcivilCB.Location = new System.Drawing.Point(113, 447);
            this.estcivilCB.Name = "estcivilCB";
            this.estcivilCB.Size = new System.Drawing.Size(103, 21);
            this.estcivilCB.TabIndex = 17;
            this.estcivilCB.Tag = "";
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label18.Location = new System.Drawing.Point(20, 448);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(79, 16);
            this.Label18.TabIndex = 0;
            this.Label18.Text = "Estado Civil";
            // 
            // lugtrabaTXT
            // 
            this.lugtrabaTXT.BackColor = System.Drawing.Color.White;
            this.lugtrabaTXT.Location = new System.Drawing.Point(113, 395);
            this.lugtrabaTXT.Name = "lugtrabaTXT";
            this.lugtrabaTXT.Size = new System.Drawing.Size(103, 20);
            this.lugtrabaTXT.TabIndex = 15;
            this.lugtrabaTXT.Tag = "";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(19, 389);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(61, 32);
            this.Label2.TabIndex = 0;
            this.Label2.Text = "Lugar de\r\nTrabajo\r\n";
            // 
            // fechaDTP
            // 
            this.fechaDTP.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fechaDTP.Location = new System.Drawing.Point(113, 110);
            this.fechaDTP.MinDate = new System.DateTime(1950, 1, 1, 0, 0, 0, 0);
            this.fechaDTP.Name = "fechaDTP";
            this.fechaDTP.Size = new System.Drawing.Size(103, 20);
            this.fechaDTP.TabIndex = 4;
            this.fechaDTP.Tag = "";
            // 
            // CelTXT
            // 
            this.CelTXT.BackColor = System.Drawing.Color.White;
            this.CelTXT.Location = new System.Drawing.Point(113, 317);
            this.CelTXT.MaxLength = 10;
            this.CelTXT.Name = "CelTXT";
            this.CelTXT.Size = new System.Drawing.Size(103, 20);
            this.CelTXT.TabIndex = 12;
            this.CelTXT.Tag = "";
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label15.Location = new System.Drawing.Point(20, 318);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(28, 16);
            this.Label15.TabIndex = 0;
            this.Label15.Text = "Cel";
            // 
            // TelTXT
            // 
            this.TelTXT.Location = new System.Drawing.Point(113, 291);
            this.TelTXT.MaxLength = 12;
            this.TelTXT.Name = "TelTXT";
            this.TelTXT.Size = new System.Drawing.Size(103, 20);
            this.TelTXT.TabIndex = 11;
            this.TelTXT.Tag = "";
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label14.Location = new System.Drawing.Point(20, 292);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(28, 16);
            this.Label14.TabIndex = 0;
            this.Label14.Text = "Tel";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label13.Location = new System.Drawing.Point(20, 266);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(89, 16);
            this.Label13.TabIndex = 0;
            this.Label13.Text = "Nacionalidad";
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label12.Location = new System.Drawing.Point(20, 240);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(60, 16);
            this.Label12.TabIndex = 0;
            this.Label12.Text = "CodPost";
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label11.Location = new System.Drawing.Point(20, 214);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(35, 16);
            this.Label11.TabIndex = 0;
            this.Label11.Text = "Pcia";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.Location = new System.Drawing.Point(20, 188);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(68, 16);
            this.Label8.TabIndex = 0;
            this.Label8.Text = "Localidad";
            // 
            // DirTXT
            // 
            this.DirTXT.BackColor = System.Drawing.Color.White;
            this.DirTXT.Location = new System.Drawing.Point(113, 161);
            this.DirTXT.Name = "DirTXT";
            this.DirTXT.Size = new System.Drawing.Size(103, 20);
            this.DirTXT.TabIndex = 6;
            this.DirTXT.Tag = "";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label9.Location = new System.Drawing.Point(20, 110);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(71, 16);
            this.Label9.TabIndex = 0;
            this.Label9.Text = "FechaNac";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(20, 58);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(57, 16);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Nombre";
            // 
            // ImagenIMG
            // 
            this.ImagenIMG.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ImagenIMG.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ImagenIMG.Location = new System.Drawing.Point(249, 19);
            this.ImagenIMG.Name = "ImagenIMG";
            this.ImagenIMG.Size = new System.Drawing.Size(99, 130);
            this.ImagenIMG.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ImagenIMG.TabIndex = 12;
            this.ImagenIMG.TabStop = false;
            this.ImagenIMG.Tag = "";
            // 
            // Label37
            // 
            this.Label37.AutoSize = true;
            this.Label37.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label37.ForeColor = System.Drawing.Color.Red;
            this.Label37.Location = new System.Drawing.Point(395, 536);
            this.Label37.Name = "Label37";
            this.Label37.Size = new System.Drawing.Size(16, 18);
            this.Label37.TabIndex = 88;
            this.Label37.Text = "*";
            // 
            // group2
            // 
            this.group2.BackColor = System.Drawing.Color.Honeydew;
            this.group2.Controls.Add(this.CerrarBT);
            this.group2.Controls.Add(this.Button2);
            this.group2.Controls.Add(this.Button1);
            this.group2.Controls.Add(this.AgregarBT);
            this.group2.Location = new System.Drawing.Point(735, 91);
            this.group2.Name = "group2";
            this.group2.Size = new System.Drawing.Size(91, 530);
            this.group2.TabIndex = 92;
            this.group2.TabStop = false;
            // 
            // CerrarBT
            // 
            this.CerrarBT.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.CerrarBT.BackColor = System.Drawing.Color.White;
            this.CerrarBT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.CerrarBT.Image = global::Presentismo_2015.Properties.Resources.door_out3;
            this.CerrarBT.Location = new System.Drawing.Point(17, 407);
            this.CerrarBT.Name = "CerrarBT";
            this.CerrarBT.Size = new System.Drawing.Size(56, 74);
            this.CerrarBT.TabIndex = 26;
            this.CerrarBT.Tag = "";
            this.CerrarBT.Text = "Cerrar";
            this.CerrarBT.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.CerrarBT.UseVisualStyleBackColor = false;
            // 
            // Button2
            // 
            this.Button2.BackColor = System.Drawing.Color.White;
            this.Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button2.Image = global::Presentismo_2015.Properties.Resources.delete;
            this.Button2.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Button2.Location = new System.Drawing.Point(17, 280);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(61, 74);
            this.Button2.TabIndex = 25;
            this.Button2.Text = "Eliminar\r\n";
            this.Button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Button2.UseVisualStyleBackColor = false;
            // 
            // Button1
            // 
            this.Button1.BackColor = System.Drawing.Color.White;
            this.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button1.Image = global::Presentismo_2015.Properties.Resources.update;
            this.Button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Button1.Location = new System.Drawing.Point(17, 161);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(61, 69);
            this.Button1.TabIndex = 25;
            this.Button1.Text = "Actualizar";
            this.Button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Button1.UseVisualStyleBackColor = false;
            // 
            // AgregarBT
            // 
            this.AgregarBT.BackColor = System.Drawing.Color.White;
            this.AgregarBT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AgregarBT.Image = global::Presentismo_2015.Properties.Resources.add;
            this.AgregarBT.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.AgregarBT.Location = new System.Drawing.Point(17, 39);
            this.AgregarBT.Name = "AgregarBT";
            this.AgregarBT.Size = new System.Drawing.Size(61, 61);
            this.AgregarBT.TabIndex = 25;
            this.AgregarBT.Text = "Agregar";
            this.AgregarBT.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AgregarBT.UseVisualStyleBackColor = false;
            this.AgregarBT.Click += new System.EventHandler(this.AgregarBT_Click);
            // 
            // Label21
            // 
            this.Label21.AutoSize = true;
            this.Label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label21.Location = new System.Drawing.Point(13, 98);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(97, 16);
            this.Label21.TabIndex = 64;
            this.Label21.Text = "Enfermedades";
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 88);
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.AutoSize = false;
            this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripLabel1,
            this.ToolStripSeparator1,
            this.ToolStripDropDownButton1,
            this.ToolStripSeparator3,
            this.ImprimirTSB,
            this.ToolStripSeparator2,
            this.ToolStripButton2});
            this.ToolStrip1.Location = new System.Drawing.Point(0, 0);
            this.ToolStrip1.Name = "ToolStrip1";
            this.ToolStrip1.Size = new System.Drawing.Size(954, 88);
            this.ToolStrip1.TabIndex = 94;
            this.ToolStrip1.Text = "ToolStrip1";
            // 
            // ToolStripLabel1
            // 
            this.ToolStripLabel1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ActualizarToolStripMenuItem,
            this.AgregarAltaToolStripMenuItem,
            this.AsignaturasToolStripMenuItem,
            this.EditarToolStripMenuItem,
            this.EliminarToolStripMenuItem});
            this.ToolStripLabel1.Image = global::Presentismo_2015.Properties.Resources.document_prepare;
            this.ToolStripLabel1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolStripLabel1.Name = "ToolStripLabel1";
            this.ToolStripLabel1.Size = new System.Drawing.Size(133, 85);
            this.ToolStripLabel1.Text = "&Modificaciones";
            // 
            // ActualizarToolStripMenuItem
            // 
            this.ActualizarToolStripMenuItem.Image = global::Presentismo_2015.Properties.Resources.update;
            this.ActualizarToolStripMenuItem.Name = "ActualizarToolStripMenuItem";
            this.ActualizarToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.ActualizarToolStripMenuItem.Text = "&Actualizar";
            // 
            // AgregarAltaToolStripMenuItem
            // 
            this.AgregarAltaToolStripMenuItem.Image = global::Presentismo_2015.Properties.Resources.add;
            this.AgregarAltaToolStripMenuItem.Name = "AgregarAltaToolStripMenuItem";
            this.AgregarAltaToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.AgregarAltaToolStripMenuItem.Text = "&Agregar (Alta)";
            // 
            // AsignaturasToolStripMenuItem
            // 
            this.AsignaturasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DIPREGEPToolStripMenuItem,
            this.DIPREGEP4491TecnicaToolStripMenuItem});
            this.AsignaturasToolStripMenuItem.Image = global::Presentismo_2015.Properties.Resources.book;
            this.AsignaturasToolStripMenuItem.Name = "AsignaturasToolStripMenuItem";
            this.AsignaturasToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.AsignaturasToolStripMenuItem.Text = "&Asignaturas";
            // 
            // DIPREGEPToolStripMenuItem
            // 
            this.DIPREGEPToolStripMenuItem.Name = "DIPREGEPToolStripMenuItem";
            this.DIPREGEPToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.DIPREGEPToolStripMenuItem.Text = "DIPREGEP 4491 Tecnica";
            // 
            // DIPREGEP4491TecnicaToolStripMenuItem
            // 
            this.DIPREGEP4491TecnicaToolStripMenuItem.Name = "DIPREGEP4491TecnicaToolStripMenuItem";
            this.DIPREGEP4491TecnicaToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.DIPREGEP4491TecnicaToolStripMenuItem.Text = "DIPREGEP 7275 Básica";
            // 
            // EditarToolStripMenuItem
            // 
            this.EditarToolStripMenuItem.Image = global::Presentismo_2015.Properties.Resources.user_edit;
            this.EditarToolStripMenuItem.Name = "EditarToolStripMenuItem";
            this.EditarToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.EditarToolStripMenuItem.Text = "&Editar";
            // 
            // EliminarToolStripMenuItem
            // 
            this.EliminarToolStripMenuItem.Image = global::Presentismo_2015.Properties.Resources.delete;
            this.EliminarToolStripMenuItem.Name = "EliminarToolStripMenuItem";
            this.EliminarToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.EliminarToolStripMenuItem.Text = "&Eliminar (Baja)";
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 88);
            // 
            // ToolStripDropDownButton1
            // 
            this.ToolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SecundariToolStripMenuItem});
            this.ToolStripDropDownButton1.Image = global::Presentismo_2015.Properties.Resources.book;
            this.ToolStripDropDownButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripDropDownButton1.Name = "ToolStripDropDownButton1";
            this.ToolStripDropDownButton1.Size = new System.Drawing.Size(134, 85);
            this.ToolStripDropDownButton1.Text = "&Ver Asignaturas";
            // 
            // SecundariToolStripMenuItem
            // 
            this.SecundariToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BasicoToolStripMenuItem,
            this.TecnicoToolStripMenuItem,
            this.AmbosToolStripMenuItem});
            this.SecundariToolStripMenuItem.Name = "SecundariToolStripMenuItem";
            this.SecundariToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.SecundariToolStripMenuItem.Text = "Secundario";
            // 
            // BasicoToolStripMenuItem
            // 
            this.BasicoToolStripMenuItem.Name = "BasicoToolStripMenuItem";
            this.BasicoToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.BasicoToolStripMenuItem.Text = "Basico";
            // 
            // TecnicoToolStripMenuItem
            // 
            this.TecnicoToolStripMenuItem.Name = "TecnicoToolStripMenuItem";
            this.TecnicoToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.TecnicoToolStripMenuItem.Text = "Tecnico";
            // 
            // AmbosToolStripMenuItem
            // 
            this.AmbosToolStripMenuItem.Name = "AmbosToolStripMenuItem";
            this.AmbosToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.AmbosToolStripMenuItem.Text = "Ambos";
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 88);
            // 
            // ImprimirTSB
            // 
            this.ImprimirTSB.Image = global::Presentismo_2015.Properties.Resources.printer;
            this.ImprimirTSB.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ImprimirTSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ImprimirTSB.Name = "ImprimirTSB";
            this.ImprimirTSB.Size = new System.Drawing.Size(89, 85);
            this.ImprimirTSB.Text = "&Imprimir";
            // 
            // ToolStripButton2
            // 
            this.ToolStripButton2.Image = global::Presentismo_2015.Properties.Resources.door_out1;
            this.ToolStripButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButton2.Name = "ToolStripButton2";
            this.ToolStripButton2.Size = new System.Drawing.Size(75, 85);
            this.ToolStripButton2.Text = "Cerrar";
            // 
            // GroupBox2
            // 
            this.GroupBox2.BackColor = System.Drawing.Color.Honeydew;
            this.GroupBox2.Controls.Add(this.Label39);
            this.GroupBox2.Controls.Add(this.ObraSocialCB);
            this.GroupBox2.Controls.Add(this.AlergiasCB);
            this.GroupBox2.Controls.Add(this.Label36);
            this.GroupBox2.Controls.Add(this.Label35);
            this.GroupBox2.Controls.Add(this.Label34);
            this.GroupBox2.Controls.Add(this.GrupoSangCB);
            this.GroupBox2.Controls.Add(this.EnfermedCB);
            this.GroupBox2.Controls.Add(this.Label10);
            this.GroupBox2.Controls.Add(this.Label20);
            this.GroupBox2.Controls.Add(this.Label21);
            this.GroupBox2.Controls.Add(this.Label22);
            this.GroupBox2.Location = new System.Drawing.Point(430, 331);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(265, 211);
            this.GroupBox2.TabIndex = 91;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = "Datos Clinicos Profesor";
            // 
            // Label22
            // 
            this.Label22.AutoSize = true;
            this.Label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label22.Location = new System.Drawing.Point(29, 69);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(77, 16);
            this.Label22.TabIndex = 63;
            this.Label22.Text = "Obra social";
            // 
            // Busqueda_de_personal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(954, 655);
            this.Controls.Add(this.Label38);
            this.Controls.Add(this.GroupBox3);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.Label37);
            this.Controls.Add(this.group2);
            this.Controls.Add(this.ToolStrip1);
            this.Controls.Add(this.GroupBox2);
            this.Name = "Busqueda_de_personal";
            this.Text = "Busqueda_de_personal";
            this.GroupBox3.ResumeLayout(false);
            this.GroupBox3.PerformLayout();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImagenIMG)).EndInit();
            this.group2.ResumeLayout(false);
            this.ToolStrip1.ResumeLayout(false);
            this.ToolStrip1.PerformLayout();
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label38;
        internal System.Windows.Forms.Label Label17;
        internal System.Windows.Forms.Label Label25;
        internal System.Windows.Forms.Label NodisponibleLBL;
        internal System.Windows.Forms.Label Label31;
        internal System.Windows.Forms.ComboBox NivelAcaCB;
        internal System.Windows.Forms.ComboBox TituloCB;
        internal System.Windows.Forms.ComboBox EspecialidadCB;
        internal System.Windows.Forms.Label Label23;
        internal System.Windows.Forms.Label Label39;
        internal System.Windows.Forms.ComboBox ObraSocialCB;
        internal System.Windows.Forms.TextBox EmailTXT;
        internal System.Windows.Forms.ComboBox AlergiasCB;
        internal System.Windows.Forms.Label Label32;
        internal System.Windows.Forms.Label Label36;
        internal System.Windows.Forms.Label Label34;
        internal System.Windows.Forms.ComboBox GrupoSangCB;
        internal System.Windows.Forms.ComboBox EnfermedCB;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.Label Label33;
        internal System.Windows.Forms.GroupBox GroupBox3;
        internal System.Windows.Forms.Label Label24;
        internal System.Windows.Forms.TextBox SeguroTXT;
        internal System.Windows.Forms.Label Label35;
        internal System.Windows.Forms.ComboBox sexoCB;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.TextBox ApellidoTXT;
        internal System.Windows.Forms.TextBox NombreTXT;
        internal System.Windows.Forms.TextBox dniTXT;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.PictureBox ImagenIMG;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Label Label40;
        internal System.Windows.Forms.Label Label30;
        internal System.Windows.Forms.Label Label29;
        internal System.Windows.Forms.Label Label28;
        internal System.Windows.Forms.Label Label27;
        internal System.Windows.Forms.Label Label26;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.ComboBox NacionCB;
        internal System.Windows.Forms.ComboBox CodPostCB;
        internal System.Windows.Forms.ComboBox PciaCB;
        internal System.Windows.Forms.ComboBox LocCB;
        internal System.Windows.Forms.TextBox ocupTXT;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.ComboBox estcivilCB;
        internal System.Windows.Forms.Label Label18;
        internal System.Windows.Forms.TextBox lugtrabaTXT;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.DateTimePicker fechaDTP;
        internal System.Windows.Forms.TextBox CelTXT;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.TextBox TelTXT;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.TextBox DirTXT;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button CerrarBT;
        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.Button Button1;
        internal System.Windows.Forms.Button AgregarBT;
        internal System.Windows.Forms.Label Label37;
        internal System.Windows.Forms.GroupBox group2;
        internal System.Windows.Forms.Label Label21;
        internal System.Windows.Forms.ToolStripMenuItem ActualizarToolStripMenuItem;
        internal System.Windows.Forms.ToolStripDropDownButton ToolStripLabel1;
        internal System.Windows.Forms.ToolStripMenuItem AgregarAltaToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem AsignaturasToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem DIPREGEPToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem DIPREGEP4491TecnicaToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem EditarToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem EliminarToolStripMenuItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStrip ToolStrip1;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripDropDownButton ToolStripDropDownButton1;
        internal System.Windows.Forms.ToolStripMenuItem SecundariToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem BasicoToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem TecnicoToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem AmbosToolStripMenuItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton ImprimirTSB;
        internal System.Windows.Forms.ToolStripButton ToolStripButton2;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.Label Label22;

    }
}