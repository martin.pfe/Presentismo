﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Presentismo_2015
{
    public class Employee
    {
        public String name { get; set; }
        public String lastName { get; set; }
        public int DNI { get;   private set; }
        public String birthDate { get; set; }
        public String birthPlace { get; set; }
        public String homePhone { get; set; }
        public String cellPhone { get; set; }
        public String schoolMail { get; set; }
        public String personalMail { get; set; }
        public String password { get; private set; }
        public int employeeID { get; set; }
        public bool viewCourses { get; set; }
        public bool addEmployee { get; set; }
        public bool editEmployee { get; set; }
        public bool editCharges { get; set; }
        public int accessType { get; set; }

        public Employee(int employeeDni, String passWord)
        {
            DNI = employeeDni;
            password = passWord;
        }

    }

    public class ComboBoxDipregep
    {
        public string Text { get; set; }
        public object Value { get; set; }

        public object Index { get; set; }
        public override string ToString()
        {
            return Text;
        }
    }

    public class permissionClass
    {
        public String text { get; set; }
        public int value { get; set; }
        public bool enabled { get; set; }


        public permissionClass(string permissionDesc, int id, bool isEnabled)
        {
	        text    = permissionDesc;
	        value   = id;
	        enabled = isEnabled;
        }
    }

    
}
