﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Presentismo_2015
{
    public partial class ConectionTry : myCustomForm
    {
        public ConectionTry()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                using (var sqlConnection = new SqlConnection(Conection.connectionString))
                {
                    sqlConnection.Open();

                    MessageBox.Show("Se pudo establecer la conexión con la base de datos.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
