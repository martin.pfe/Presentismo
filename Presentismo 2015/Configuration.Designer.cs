﻿namespace Presentismo_2015
{
    partial class Configuration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows myCustomForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.confirmBtn = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.txt_lastPassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_newPassword = new System.Windows.Forms.TextBox();
            this.txt_newPasswordRepeat = new System.Windows.Forms.TextBox();
            this.txt_dbPassword = new System.Windows.Forms.TextBox();
            this.txt_dbName = new System.Windows.Forms.TextBox();
            this.txt_dbServer = new System.Windows.Forms.TextBox();
            this.btn_testConnection = new System.Windows.Forms.Button();
            this.txt_signaturesPath = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Contraseña actual";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nueva contraseña";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Repita la nueva contraseña";
            // 
            // confirmBtn
            // 
            this.confirmBtn.Enabled = false;
            this.confirmBtn.Location = new System.Drawing.Point(222, 365);
            this.confirmBtn.Name = "confirmBtn";
            this.confirmBtn.Size = new System.Drawing.Size(89, 27);
            this.confirmBtn.TabIndex = 9;
            this.confirmBtn.Text = "Confirmar";
            this.confirmBtn.UseVisualStyleBackColor = true;
            this.confirmBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 365);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(89, 27);
            this.button2.TabIndex = 4;
            this.button2.Text = "Cerrar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txt_lastPassword
            // 
            this.txt_lastPassword.Location = new System.Drawing.Point(213, 36);
            this.txt_lastPassword.Name = "txt_lastPassword";
            this.txt_lastPassword.Size = new System.Drawing.Size(98, 20);
            this.txt_lastPassword.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(139, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Usuario de la base de datos";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(34, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Contraseña";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(35, 218);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Servidor:";
            // 
            // txt_newPassword
            // 
            this.txt_newPassword.Location = new System.Drawing.Point(213, 72);
            this.txt_newPassword.Name = "txt_newPassword";
            this.txt_newPassword.Size = new System.Drawing.Size(98, 20);
            this.txt_newPassword.TabIndex = 2;
            // 
            // txt_newPasswordRepeat
            // 
            this.txt_newPasswordRepeat.Location = new System.Drawing.Point(213, 109);
            this.txt_newPasswordRepeat.Name = "txt_newPasswordRepeat";
            this.txt_newPasswordRepeat.Size = new System.Drawing.Size(98, 20);
            this.txt_newPasswordRepeat.TabIndex = 3;
            // 
            // txt_dbPassword
            // 
            this.txt_dbPassword.Location = new System.Drawing.Point(213, 183);
            this.txt_dbPassword.Name = "txt_dbPassword";
            this.txt_dbPassword.Size = new System.Drawing.Size(98, 20);
            this.txt_dbPassword.TabIndex = 5;
            // 
            // txt_dbName
            // 
            this.txt_dbName.Location = new System.Drawing.Point(213, 147);
            this.txt_dbName.Name = "txt_dbName";
            this.txt_dbName.Size = new System.Drawing.Size(98, 20);
            this.txt_dbName.TabIndex = 4;
            // 
            // txt_dbServer
            // 
            this.txt_dbServer.Location = new System.Drawing.Point(214, 215);
            this.txt_dbServer.Name = "txt_dbServer";
            this.txt_dbServer.Size = new System.Drawing.Size(98, 20);
            this.txt_dbServer.TabIndex = 6;
            // 
            // btn_testConnection
            // 
            this.btn_testConnection.Location = new System.Drawing.Point(213, 299);
            this.btn_testConnection.Name = "btn_testConnection";
            this.btn_testConnection.Size = new System.Drawing.Size(98, 28);
            this.btn_testConnection.TabIndex = 8;
            this.btn_testConnection.Text = "Probar conexión";
            this.btn_testConnection.UseVisualStyleBackColor = true;
            this.btn_testConnection.Click += new System.EventHandler(this.btn_testConnection_Click);
            // 
            // txt_signaturesPath
            // 
            this.txt_signaturesPath.Location = new System.Drawing.Point(213, 249);
            this.txt_signaturesPath.Name = "txt_signaturesPath";
            this.txt_signaturesPath.Size = new System.Drawing.Size(98, 20);
            this.txt_signaturesPath.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 256);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Carpeta de firmas:";
            // 
            // Configuration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 404);
            this.Controls.Add(this.txt_signaturesPath);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btn_testConnection);
            this.Controls.Add(this.txt_dbServer);
            this.Controls.Add(this.txt_dbName);
            this.Controls.Add(this.txt_dbPassword);
            this.Controls.Add(this.txt_newPasswordRepeat);
            this.Controls.Add(this.txt_newPassword);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_lastPassword);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.confirmBtn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Configuration";
            this.Text = "Configuration";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button confirmBtn;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txt_lastPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_newPassword;
        private System.Windows.Forms.TextBox txt_newPasswordRepeat;
        private System.Windows.Forms.TextBox txt_dbPassword;
        private System.Windows.Forms.TextBox txt_dbName;
        private System.Windows.Forms.TextBox txt_dbServer;
        private System.Windows.Forms.Button btn_testConnection;
        private System.Windows.Forms.TextBox txt_signaturesPath;
        private System.Windows.Forms.Label label7;
    }
}