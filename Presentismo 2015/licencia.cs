﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Dapper;

namespace Presentismo_2015
{
    public partial class licencia : myCustomForm
    {
        public licencia()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int employeeID  = 1;
                string motivo   = motivoTxt.Text;
                int licenseType = 1;
                string dateFrom = dateTimePicker1.Text;
                string dateTo   = dateTimePicker1.Text;


                var spParams = new DynamicParameters();
                spParams.Add("@employeeID", employeeID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@motivo", motivo, dbType: DbType.String, direction: ParameterDirection.Input, size: 200);
                spParams.Add("@licenseType", licenseType, dbType: DbType.Int32, direction: ParameterDirection.Input);
                spParams.Add("@dateFrom", dateFrom, dbType: DbType.DateTime, direction: ParameterDirection.Input);
                spParams.Add("@dateTo", dateTo, dbType: DbType.Int32, direction: ParameterDirection.Input);

                spParams.Add("@errorDesc", dbType: DbType.String, direction: ParameterDirection.Output, size: 500);

                spParams.Add("@RETURN_VALUE", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
                using (var sqlConnection = new SqlConnection(Conection.connectionString))
                {

                    sqlConnection.Open();

                    sqlConnection.Execute("SP_User_License_Add", spParams, commandType: CommandType.StoredProcedure);
                    int RC = spParams.Get<int>("@RETURN_VALUE");
                    string errorDesc = spParams.Get<string>("@errorDesc");

                    if (RC == 1)
                    {
                        MessageBox.Show("La licencia ha sido cargada satisfactoriamente!");
                    }
                    else
                        MessageBox.Show(String.Format("Error: {0} - Error N° :{1}", errorDesc, RC.ToString()));

                    sqlConnection.Close();
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
