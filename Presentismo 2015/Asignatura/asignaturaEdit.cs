﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Dapper;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.Common;

namespace Presentismo_2015.Asignatura
{
    public partial class asignaturaEdit : Form
    {
        Subjet editSubjet = new Subjet();
        defaultAsignatura _parentForm;
        Employee formEmployee;
        public asignaturaEdit(int subjetID, defaultAsignatura parentForm, Employee logedEmployee)
        {
            _parentForm = parentForm;
            InitializeComponent();

            formEmployee = logedEmployee;
            //ComboboxItem Item = new ComboboxItem();
            //Item.Text = "Secundario básico";
            //Item.Value = 1;

            //dipregepCombo.Items.Add(Item);

           //ComboboxItem Item2 = new ComboboxItem();

            //Item2.Text = "Secundario técnico";
            //Item2.Value = 2;

            //dipregepCombo.Items.Add(Item2);

            using (SqlConnection conn = new SqlConnection(Conection.connectionString))
            {
                conn.Open();
                using (DbCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = String.Format("SELECT name, dipregepID, dipregepDesc FROM dbo.VW_Subjet WITH(NOLOCK) WHERE subjetID = {0}", subjetID);

                    using (DbDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            editSubjet.ID = subjetID;
                            editSubjet.Name = reader.GetString(0);
                            editSubjet.dipregepID = reader.GetInt32(1);
                            editSubjet.dipregepDesc = reader.GetString(2);
                        }
                    }
                }
            }

            txt_subjetName.Text = editSubjet.Name;
            dipregepCombo.SelectedIndex = dipregepCombo.FindString((editSubjet.dipregepDesc).Trim());

            this.Text = String.Format("Edición de asignatura: {0}", editSubjet.Name);

        }

        private void asignaturaEdit_Load(object sender, EventArgs e)
        {

        }

        private void dipregepCombo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void AgregarBT_Click(object sender, EventArgs e)
        {

            try
            {
                String subjetName = txt_subjetName.Text.ToString();
                int dipregepID = Convert.ToInt32(((DipregepComboItems)dipregepCombo.SelectedItem).Value);

                var spParams = new DynamicParameters();
                spParams.Add("@subjetID", editSubjet.ID, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                spParams.Add("@subjetName", subjetName, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                spParams.Add("@dipregepID", dipregepID, dbType: DbType.Int32, direction: ParameterDirection.Input);

                spParams.Add("@errorDesc", dbType: DbType.String, direction: ParameterDirection.Output, size: 30);

                spParams.Add("@RETURN_VALUE", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                using (var sqlConnection = new SqlConnection(Conection.connectionString))
                {
                    sqlConnection.Open();

                    sqlConnection.Execute("SP_Subjet_Edit", spParams, commandType: CommandType.StoredProcedure);
                    string errorDesc = spParams.Get<string>("@errorDesc");
                    int RC = spParams.Get<int>("@RETURN_VALUE");

                    sqlConnection.Close();
                    if (RC != 1)
                    {
                        MessageBox.Show(String.Format("Error: {0} - Error N° :{1}", errorDesc, RC.ToString()));
                    }
                    else
                    {
                        MessageBox.Show("La materia ha sido editada correctamente.");
                        closeWindow();
                    }
                }

            }
            catch (Exception ex)
            { }
        }


        private void CerrarBT_Click(object sender, EventArgs e)
        {
            closeWindow();
        }

        private void closeWindow()
        {
            _parentForm.getSubjetGrid();
            editSubjet = null;
            this.Hide();
        }
    }
}
