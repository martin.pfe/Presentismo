﻿namespace Presentismo_2015.Asignatura
{
    partial class asignaturaEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.dipregepCombo = new System.Windows.Forms.ComboBox();
            this.txt_subjetName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CerrarBT = new System.Windows.Forms.Button();
            this.AgregarBT = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(17, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 24);
            this.label2.TabIndex = 37;
            this.label2.Text = "Dipregep";
            // 
            // dipregepCombo
            // 
            this.dipregepCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dipregepCombo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dipregepCombo.FormattingEnabled = true;
            this.dipregepCombo.Location = new System.Drawing.Point(185, 85);
            this.dipregepCombo.Name = "dipregepCombo";
            this.dipregepCombo.Size = new System.Drawing.Size(150, 24);
            this.dipregepCombo.TabIndex = 36;
            this.dipregepCombo.SelectedIndexChanged += new System.EventHandler(this.dipregepCombo_SelectedIndexChanged);
            // 
            // txt_subjetName
            // 
            this.txt_subjetName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_subjetName.Location = new System.Drawing.Point(185, 39);
            this.txt_subjetName.Name = "txt_subjetName";
            this.txt_subjetName.Size = new System.Drawing.Size(150, 23);
            this.txt_subjetName.TabIndex = 35;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 24);
            this.label1.TabIndex = 34;
            this.label1.Text = "Nombre";
            // 
            // CerrarBT
            // 
            this.CerrarBT.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.CerrarBT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.CerrarBT.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CerrarBT.Image = global::Presentismo_2015.Properties.Resources.door_out1;
            this.CerrarBT.Location = new System.Drawing.Point(21, 226);
            this.CerrarBT.Name = "CerrarBT";
            this.CerrarBT.Size = new System.Drawing.Size(90, 70);
            this.CerrarBT.TabIndex = 33;
            this.CerrarBT.Tag = "";
            this.CerrarBT.Text = "Cerrar";
            this.CerrarBT.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.CerrarBT.UseVisualStyleBackColor = true;
            this.CerrarBT.Click += new System.EventHandler(this.CerrarBT_Click);
            // 
            // AgregarBT
            // 
            this.AgregarBT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AgregarBT.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AgregarBT.Image = global::Presentismo_2015.Properties.Resources.pencil;
            this.AgregarBT.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.AgregarBT.Location = new System.Drawing.Point(248, 226);
            this.AgregarBT.Name = "AgregarBT";
            this.AgregarBT.Size = new System.Drawing.Size(90, 70);
            this.AgregarBT.TabIndex = 32;
            this.AgregarBT.Text = "Editar";
            this.AgregarBT.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AgregarBT.UseVisualStyleBackColor = true;
            this.AgregarBT.Click += new System.EventHandler(this.AgregarBT_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txt_subjetName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dipregepCombo);
            this.groupBox1.Controls.Add(this.CerrarBT);
            this.groupBox1.Controls.Add(this.AgregarBT);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(360, 337);
            this.groupBox1.TabIndex = 38;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Edite los datos de la asignatura";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // asignaturaEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 361);
            this.Controls.Add(this.groupBox1);
            this.Name = "asignaturaEdit";
            this.Text = "Edición de asignatura";
            this.Load += new System.EventHandler(this.asignaturaEdit_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox dipregepCombo;
        private System.Windows.Forms.TextBox txt_subjetName;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Button CerrarBT;
        internal System.Windows.Forms.Button AgregarBT;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}