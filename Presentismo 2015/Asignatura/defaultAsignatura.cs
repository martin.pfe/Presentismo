﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Dapper;

namespace Presentismo_2015.Asignatura
{

    public partial class defaultAsignatura : Form
    {
        Employee formEmployee;
        public defaultAsignatura(Employee logedEmployee)
        {
            InitializeComponent();
            formEmployee = logedEmployee;


            ComboBoxDipregep item = new ComboBoxDipregep();

            item.Text = "[Seleccione]";
            item.Value = 0;
            item.Index = 0;

            cmb_dipregep.Items.Add(item);

            item = new ComboBoxDipregep();
            item.Text = "Secundaria Basica";
            item.Value = 1;
            item.Index = 1;

            cmb_dipregep.Items.Add(item);

            item = new ComboBoxDipregep();
            item.Text = "Secundaria Tecnica";
            item.Value = 2;
            item.Index = 2;

            cmb_dipregep.Items.Add(item);

            cmb_dipregep.SelectedText = "[Selecccione]";
            cmb_dipregep.SelectedIndex = 0;

            doSearch();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AsignaturaAdd newasig = new AsignaturaAdd(formEmployee);
            newasig.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void defaultAsignatura_Load(object sender, EventArgs e)
        {
            getSubjetGrid();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            AsignaturaAdd asignaturaAdd = new AsignaturaAdd(formEmployee);
            asignaturaAdd.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        internal void getSubjetGrid()
        {
            try
            {
                dataGridView1.ReadOnly = false;
                dataGridView1.DataSource = null;
                dataGridView1.Rows.Clear();
                dataGridView1.Refresh();
                // string connectionString = "Server=tcp:oer8py276v.database.windows.net,1433;Database=Presentismo;User ID=proyecto2015@oer8py276v;Password=Colegiojuan23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                string select = "SELECT subjetID, name AS Asignatura , dipregepDesc AS Dipregep FROM dbo.VW_Subjet WITH(NOLOCK)";
                //Connection c = new Connection();
                using (SqlConnection cnn = new SqlConnection(Conection.connectionString))
                {
                    SqlDataAdapter dataAdapter = new SqlDataAdapter(select, Conection.connectionString); //c.con is the connection string

                    SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
                    DataSet ds = new DataSet();
                    dataAdapter.Fill(ds);
                    dataGridView1.ReadOnly = true;
                    dataGridView1.DataSource = ds.Tables[0];
                    dataGridView1.Columns[0].Visible = false;
                    dataGridView1.AllowUserToAddRows = false;
                    dataGridView1.AllowUserToDeleteRows = false;
                    cnn.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            getSubjetGrid();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            AsignaturaAdd asignaturaAdd = new AsignaturaAdd(formEmployee);
            asignaturaAdd.Show();
        }

        private void btn_Edit_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count != 1)
            {
                DataGridViewRow row = dataGridView1.Rows[dataGridView1.SelectedCells[0].RowIndex];

                asignaturaEdit asignaturaEdit = new asignaturaEdit(Int32.Parse(row.Cells["subjetID"].Value.ToString()), this, formEmployee);
                asignaturaEdit.Show();
            }
            else
            {
                MessageBox.Show("Para editar una asignatura debe tener seleccionada solo una.");
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button1_Click_3(object sender, EventArgs e)
        {
            doSearch();
        }

     
        private void doSearch()
        {
            int dipregepID = 0;
            String subjetName = String.Empty;

            if (txt_subjetName.Text != String.Empty)
            {
                subjetName = txt_subjetName.Text.Trim();
            }

            if (!Int32.TryParse(cmb_dipregep.SelectedIndex.ToString(), out dipregepID))
            {
                MessageBox.Show("Seleccione el dipregep.");
                return;
            }

            var spParams = new DynamicParameters();
            spParams.Add("@dipregepID", dipregepID, dbType: DbType.Int32, direction: ParameterDirection.Input);
            spParams.Add("@subjetName", subjetName, dbType: DbType.String, direction: ParameterDirection.Input);

            using (var sqlConnection = new SqlConnection(Conection.connectionString))
            {
                sqlConnection.Open();
                DataTable dt = new DataTable();

                dt.Load(sqlConnection.ExecuteReader("SP_Subjet_Search", spParams, commandType: CommandType.StoredProcedure));

                dataGridView1.AutoGenerateColumns = true;
                dataGridView1.DataSource = dt;
                dataGridView1.Refresh();
                dataGridView1.ReadOnly = true;
                dataGridView1.AllowUserToAddRows = false;
                dataGridView1.AllowUserToDeleteRows = false;
            }
        
        }
    }
}
