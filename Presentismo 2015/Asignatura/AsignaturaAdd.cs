﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Dapper;


namespace Presentismo_2015.Asignatura
{
    public partial class AsignaturaAdd : myCustomForm
    {
        Employee formEmployee;
        public AsignaturaAdd(Employee logedEmployee)
        {
            InitializeComponent();

            formEmployee = logedEmployee;

            DipregepComboItems Item = new DipregepComboItems();
            Item.Text = "Secundario básico";
            Item.Value = 1;

            dipregepCombo.Items.Add(Item);

            DipregepComboItems Item2 = new DipregepComboItems();

            Item2.Text = "Secundario técnico";
            Item2.Value = 2;

            dipregepCombo.Items.Add(Item2);
        }

        private void CerrarBT_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Hide();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void AgregarBT_Click(object sender, EventArgs e)
        {
            String subjetName = txt_subjetName.Text.ToString();
            int dipregepID = Convert.ToInt32(((DipregepComboItems)dipregepCombo.SelectedItem).Value);

            try
            {
                var spParams = new DynamicParameters();
                spParams.Add("@subjetName", subjetName, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                spParams.Add("@dipregepID", dipregepID, dbType: DbType.Int32, direction: ParameterDirection.Input);

                spParams.Add("@errorDesc", dbType: DbType.String, direction: ParameterDirection.Output, size: 30);

                spParams.Add("@RETURN_VALUE", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                using (var sqlConnection = new SqlConnection(Conection.connectionString))
                {
                    sqlConnection.Open();

                    sqlConnection.Execute("SP_Subjet_Add", spParams, commandType: CommandType.StoredProcedure);
                    string errorDesc = spParams.Get<string>("@errorDesc");
                    int RC = spParams.Get<int>("@RETURN_VALUE");

                    sqlConnection.Close();
                    if (RC != 1)
                    {
                        MessageBox.Show(String.Format("Error: {0} - Error N° :{1}", errorDesc, RC.ToString()));
                    }
                    else
                    {
                        MessageBox.Show("La materia ha sido dada de alta correctamente.");
                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void comboDipregep1_Load(object sender, EventArgs e)
        {
        
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dipregepCombo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
