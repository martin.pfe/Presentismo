﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Presentismo_2015
{
    public class DipregepComboItems
    {

        public string Text { get; set; }
        public object Value { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}
