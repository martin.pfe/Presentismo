﻿namespace Presentismo_2015
{
    partial class BuscarPersonal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows myCustomForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.bShowDeletedEmployee = new System.Windows.Forms.CheckBox();
            this.legajoTXT = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lastNameTXT = new System.Windows.Forms.TextBox();
            this.Button2 = new System.Windows.Forms.Button();
            this.nameTXT = new System.Windows.Forms.TextBox();
            this.Button1 = new System.Windows.Forms.Button();
            this.dniTXT = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.DataGridView1 = new System.Windows.Forms.DataGridView();
            this.Label38 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.bShowDeletedEmployee);
            this.GroupBox1.Controls.Add(this.legajoTXT);
            this.GroupBox1.Controls.Add(this.label4);
            this.GroupBox1.Controls.Add(this.lastNameTXT);
            this.GroupBox1.Controls.Add(this.Button2);
            this.GroupBox1.Controls.Add(this.nameTXT);
            this.GroupBox1.Controls.Add(this.Button1);
            this.GroupBox1.Controls.Add(this.dniTXT);
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Controls.Add(this.Label2);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox1.Location = new System.Drawing.Point(24, 46);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(749, 199);
            this.GroupBox1.TabIndex = 3;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Búsqueda de Personal";
            // 
            // bShowDeletedEmployee
            // 
            this.bShowDeletedEmployee.AutoSize = true;
            this.bShowDeletedEmployee.Location = new System.Drawing.Point(317, 42);
            this.bShowDeletedEmployee.Name = "bShowDeletedEmployee";
            this.bShowDeletedEmployee.Size = new System.Drawing.Size(216, 20);
            this.bShowDeletedEmployee.TabIndex = 6;
            this.bShowDeletedEmployee.Text = "Mostrar personal eliminado";
            this.bShowDeletedEmployee.UseVisualStyleBackColor = true;
            this.bShowDeletedEmployee.CheckedChanged += new System.EventHandler(this.bShowDeletedEmployee_CheckedChanged);
            // 
            // legajoTXT
            // 
            this.legajoTXT.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.legajoTXT.Location = new System.Drawing.Point(143, 72);
            this.legajoTXT.Name = "legajoTXT";
            this.legajoTXT.Size = new System.Drawing.Size(100, 29);
            this.legajoTXT.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label4.Location = new System.Drawing.Point(56, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Legajo";
            // 
            // lastNameTXT
            // 
            this.lastNameTXT.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lastNameTXT.Location = new System.Drawing.Point(143, 145);
            this.lastNameTXT.Name = "lastNameTXT";
            this.lastNameTXT.Size = new System.Drawing.Size(100, 29);
            this.lastNameTXT.TabIndex = 3;
            // 
            // Button2
            // 
            this.Button2.BackColor = System.Drawing.Color.White;
            this.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button2.Image = global::Presentismo_2015.Properties.Resources.door_out2;
            this.Button2.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Button2.Location = new System.Drawing.Point(676, 111);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(56, 64);
            this.Button2.TabIndex = 1;
            this.Button2.Text = "Cerrar";
            this.Button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Button2.UseVisualStyleBackColor = false;
            this.Button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // nameTXT
            // 
            this.nameTXT.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameTXT.Location = new System.Drawing.Point(143, 107);
            this.nameTXT.Name = "nameTXT";
            this.nameTXT.Size = new System.Drawing.Size(100, 29);
            this.nameTXT.TabIndex = 3;
            // 
            // Button1
            // 
            this.Button1.BackColor = System.Drawing.Color.White;
            this.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button1.Image = global::Presentismo_2015.Properties.Resources.magnifier;
            this.Button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Button1.Location = new System.Drawing.Point(676, 21);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(56, 66);
            this.Button1.TabIndex = 1;
            this.Button1.Text = "Buscar";
            this.Button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Button1.UseVisualStyleBackColor = false;
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // dniTXT
            // 
            this.dniTXT.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dniTXT.Location = new System.Drawing.Point(143, 37);
            this.dniTXT.Name = "dniTXT";
            this.dniTXT.Size = new System.Drawing.Size(100, 29);
            this.dniTXT.TabIndex = 3;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Label3.Location = new System.Drawing.Point(56, 46);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(31, 16);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "DNI";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Label2.Location = new System.Drawing.Point(56, 154);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(58, 16);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "Apellido";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.Label1.Location = new System.Drawing.Point(56, 116);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(57, 16);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Nombre";
            // 
            // DataGridView1
            // 
            this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView1.Location = new System.Drawing.Point(24, 273);
            this.DataGridView1.Name = "DataGridView1";
            this.DataGridView1.Size = new System.Drawing.Size(749, 196);
            this.DataGridView1.TabIndex = 4;
            this.DataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // Label38
            // 
            this.Label38.AutoSize = true;
            this.Label38.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label38.ForeColor = System.Drawing.Color.RoyalBlue;
            this.Label38.Location = new System.Drawing.Point(21, 491);
            this.Label38.Name = "Label38";
            this.Label38.Size = new System.Drawing.Size(457, 16);
            this.Label38.TabIndex = 103;
            this.Label38.Text = "Para ver las opciones disponibles haga doble clic en el usuario que desee ";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = global::Presentismo_2015.Properties.Resources.file_extension_xls;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button3.Location = new System.Drawing.Point(779, 6);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(55, 55);
            this.button3.TabIndex = 7;
            this.button3.Text = "Exportar";
            this.button3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // BuscarPersonal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(846, 526);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.Label38);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.DataGridView1);
            this.Name = "BuscarPersonal";
            this.Text = "Personal";
            this.Load += new System.EventHandler(this.BuscarPersonal_Load_1);
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.TextBox lastNameTXT;
        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.TextBox nameTXT;
        internal System.Windows.Forms.Button Button1;
        internal System.Windows.Forms.TextBox dniTXT;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.DataGridView DataGridView1;
        internal System.Windows.Forms.TextBox legajoTXT;
        internal System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox bShowDeletedEmployee;
        internal System.Windows.Forms.Label Label38;
        internal System.Windows.Forms.Button button3;
    }
}