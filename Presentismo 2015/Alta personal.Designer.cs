﻿namespace Presentismo_2015
{
    partial class AltaPersonal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows myCustomForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label29 = new System.Windows.Forms.Label();
            this.Label28 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.dateDTP = new System.Windows.Forms.DateTimePicker();
            this.Label38 = new System.Windows.Forms.Label();
            this.group2 = new System.Windows.Forms.GroupBox();
            this.CerrarBT = new System.Windows.Forms.Button();
            this.AgregarBT = new System.Windows.Forms.Button();
            this.lastNameTxt = new System.Windows.Forms.TextBox();
            this.nameTxt = new System.Windows.Forms.TextBox();
            this.DNITxt = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label37 = new System.Windows.Forms.Label();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.redLabelAccessType = new System.Windows.Forms.Label();
            this.labelAccessType = new System.Windows.Forms.Label();
            this.employeeTypeCb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ingressDate = new System.Windows.Forms.DateTimePicker();
            this.label17 = new System.Windows.Forms.Label();
            this.legajoTxt = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.countryTxt = new System.Windows.Forms.TextBox();
            this.zipCodeTxt = new System.Windows.Forms.TextBox();
            this.stateTxt = new System.Windows.Forms.TextBox();
            this.cityTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.personalMailTxt = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Label40 = new System.Windows.Forms.Label();
            this.Label27 = new System.Windows.Forms.Label();
            this.Label26 = new System.Windows.Forms.Label();
            this.schoolMailTxt = new System.Windows.Forms.TextBox();
            this.Label16 = new System.Windows.Forms.Label();
            this.cellPhoneTxt = new System.Windows.Forms.TextBox();
            this.Label15 = new System.Windows.Forms.Label();
            this.phoneTxt = new System.Windows.Forms.TextBox();
            this.Label14 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.Label12 = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.addressTxt = new System.Windows.Forms.TextBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.group2.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label29
            // 
            this.Label29.AutoSize = true;
            this.Label29.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label29.ForeColor = System.Drawing.Color.Red;
            this.Label29.Location = new System.Drawing.Point(269, 56);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(16, 18);
            this.Label29.TabIndex = 0;
            this.Label29.Text = "*";
            // 
            // Label28
            // 
            this.Label28.AutoSize = true;
            this.Label28.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label28.ForeColor = System.Drawing.Color.Red;
            this.Label28.Location = new System.Drawing.Point(269, 82);
            this.Label28.Name = "Label28";
            this.Label28.Size = new System.Drawing.Size(16, 18);
            this.Label28.TabIndex = 0;
            this.Label28.Text = "*";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.ForeColor = System.Drawing.Color.Red;
            this.Label4.Location = new System.Drawing.Point(269, 33);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(16, 18);
            this.Label4.TabIndex = 0;
            this.Label4.Text = "*";
            // 
            // dateDTP
            // 
            this.dateDTP.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateDTP.Location = new System.Drawing.Point(124, 110);
            this.dateDTP.MinDate = new System.DateTime(1950, 1, 1, 0, 0, 0, 0);
            this.dateDTP.Name = "dateDTP";
            this.dateDTP.Size = new System.Drawing.Size(138, 20);
            this.dateDTP.TabIndex = 4;
            this.dateDTP.Tag = "";
            // 
            // Label38
            // 
            this.Label38.AutoSize = true;
            this.Label38.Font = new System.Drawing.Font("Georgia", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label38.ForeColor = System.Drawing.Color.Red;
            this.Label38.Location = new System.Drawing.Point(34, 379);
            this.Label38.Name = "Label38";
            this.Label38.Size = new System.Drawing.Size(211, 16);
            this.Label38.TabIndex = 0;
            this.Label38.Text = "el ingreso de datos es obligatorio";
            // 
            // group2
            // 
            this.group2.BackColor = System.Drawing.Color.Honeydew;
            this.group2.Controls.Add(this.CerrarBT);
            this.group2.Controls.Add(this.AgregarBT);
            this.group2.Location = new System.Drawing.Point(667, 98);
            this.group2.Name = "group2";
            this.group2.Size = new System.Drawing.Size(120, 257);
            this.group2.TabIndex = 90;
            this.group2.TabStop = false;
            // 
            // CerrarBT
            // 
            this.CerrarBT.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.CerrarBT.BackColor = System.Drawing.Color.White;
            this.CerrarBT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.CerrarBT.Image = global::Presentismo_2015.Properties.Resources.door_out1;
            this.CerrarBT.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.CerrarBT.Location = new System.Drawing.Point(18, 177);
            this.CerrarBT.Name = "CerrarBT";
            this.CerrarBT.Size = new System.Drawing.Size(90, 70);
            this.CerrarBT.TabIndex = 18;
            this.CerrarBT.Tag = "";
            this.CerrarBT.Text = "Cerrar";
            this.CerrarBT.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.CerrarBT.UseVisualStyleBackColor = false;
            this.CerrarBT.Click += new System.EventHandler(this.CerrarBT_Click);
            // 
            // AgregarBT
            // 
            this.AgregarBT.BackColor = System.Drawing.Color.White;
            this.AgregarBT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AgregarBT.Image = global::Presentismo_2015.Properties.Resources.add1;
            this.AgregarBT.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.AgregarBT.Location = new System.Drawing.Point(18, 21);
            this.AgregarBT.Name = "AgregarBT";
            this.AgregarBT.Size = new System.Drawing.Size(90, 70);
            this.AgregarBT.TabIndex = 17;
            this.AgregarBT.Text = "Agregar";
            this.AgregarBT.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AgregarBT.UseVisualStyleBackColor = false;
            this.AgregarBT.Click += new System.EventHandler(this.AgregarBT_Click);
            // 
            // lastNameTxt
            // 
            this.lastNameTxt.Location = new System.Drawing.Point(124, 80);
            this.lastNameTxt.MaxLength = 50;
            this.lastNameTxt.Name = "lastNameTxt";
            this.lastNameTxt.Size = new System.Drawing.Size(138, 20);
            this.lastNameTxt.TabIndex = 3;
            this.lastNameTxt.Tag = "";
            // 
            // nameTxt
            // 
            this.nameTxt.BackColor = System.Drawing.Color.White;
            this.nameTxt.Location = new System.Drawing.Point(124, 56);
            this.nameTxt.MaxLength = 30;
            this.nameTxt.Name = "nameTxt";
            this.nameTxt.Size = new System.Drawing.Size(138, 20);
            this.nameTxt.TabIndex = 2;
            this.nameTxt.Tag = "";
            // 
            // DNITxt
            // 
            this.DNITxt.Location = new System.Drawing.Point(124, 31);
            this.DNITxt.MaxLength = 8;
            this.DNITxt.Name = "DNITxt";
            this.DNITxt.Size = new System.Drawing.Size(138, 20);
            this.DNITxt.TabIndex = 1;
            this.DNITxt.Tag = "";
            this.DNITxt.TextChanged += new System.EventHandler(this.DNITxt_TextChanged);
            this.DNITxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DNITxt_KeyPress);
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label9.Location = new System.Drawing.Point(11, 110);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(87, 16);
            this.Label9.TabIndex = 0;
            this.Label9.Text = "F. nacimiento";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.Location = new System.Drawing.Point(11, 35);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(31, 16);
            this.Label5.TabIndex = 0;
            this.Label5.Text = "DNI";
            // 
            // Label37
            // 
            this.Label37.AutoSize = true;
            this.Label37.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label37.ForeColor = System.Drawing.Color.Red;
            this.Label37.Location = new System.Drawing.Point(12, 379);
            this.Label37.Name = "Label37";
            this.Label37.Size = new System.Drawing.Size(16, 18);
            this.Label37.TabIndex = 96;
            this.Label37.Text = "*";
            // 
            // GroupBox1
            // 
            this.GroupBox1.BackColor = System.Drawing.Color.Honeydew;
            this.GroupBox1.Controls.Add(this.label20);
            this.GroupBox1.Controls.Add(this.label19);
            this.GroupBox1.Controls.Add(this.redLabelAccessType);
            this.GroupBox1.Controls.Add(this.labelAccessType);
            this.GroupBox1.Controls.Add(this.employeeTypeCb);
            this.GroupBox1.Controls.Add(this.label6);
            this.GroupBox1.Controls.Add(this.ingressDate);
            this.GroupBox1.Controls.Add(this.label17);
            this.GroupBox1.Controls.Add(this.legajoTxt);
            this.GroupBox1.Controls.Add(this.label18);
            this.GroupBox1.Controls.Add(this.Label29);
            this.GroupBox1.Controls.Add(this.Label28);
            this.GroupBox1.Controls.Add(this.Label4);
            this.GroupBox1.Controls.Add(this.dateDTP);
            this.GroupBox1.Controls.Add(this.lastNameTxt);
            this.GroupBox1.Controls.Add(this.nameTxt);
            this.GroupBox1.Controls.Add(this.DNITxt);
            this.GroupBox1.Controls.Add(this.Label9);
            this.GroupBox1.Controls.Add(this.Label5);
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Location = new System.Drawing.Point(12, 98);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(291, 257);
            this.GroupBox1.TabIndex = 1;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Datos Profesor";
            this.GroupBox1.Enter += new System.EventHandler(this.GroupBox1_Enter);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(268, 164);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(16, 18);
            this.label20.TabIndex = 0;
            this.label20.Text = "*";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(268, 108);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(16, 18);
            this.label19.TabIndex = 0;
            this.label19.Text = "*";
            // 
            // redLabelAccessType
            // 
            this.redLabelAccessType.AutoSize = true;
            this.redLabelAccessType.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.redLabelAccessType.ForeColor = System.Drawing.Color.Red;
            this.redLabelAccessType.Location = new System.Drawing.Point(268, 198);
            this.redLabelAccessType.Name = "redLabelAccessType";
            this.redLabelAccessType.Size = new System.Drawing.Size(16, 18);
            this.redLabelAccessType.TabIndex = 0;
            this.redLabelAccessType.Text = "*";
            // 
            // labelAccessType
            // 
            this.labelAccessType.AutoSize = true;
            this.labelAccessType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAccessType.Location = new System.Drawing.Point(6, 199);
            this.labelAccessType.Name = "labelAccessType";
            this.labelAccessType.Size = new System.Drawing.Size(120, 16);
            this.labelAccessType.TabIndex = 103;
            this.labelAccessType.Text = "Tipo de empleado";
            // 
            // employeeTypeCb
            // 
            this.employeeTypeCb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.employeeTypeCb.FormattingEnabled = true;
            this.employeeTypeCb.Items.AddRange(new object[] {
            "Administrador",
            "Administrativo",
            "Sin acceso al sistema"});
            this.employeeTypeCb.Location = new System.Drawing.Point(126, 198);
            this.employeeTypeCb.Name = "employeeTypeCb";
            this.employeeTypeCb.Size = new System.Drawing.Size(136, 21);
            this.employeeTypeCb.TabIndex = 7;
            this.employeeTypeCb.SelectedIndexChanged += new System.EventHandler(this.employeeTypeCb_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 166);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 16);
            this.label6.TabIndex = 100;
            this.label6.Text = "F. ingreso";
            // 
            // ingressDate
            // 
            this.ingressDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ingressDate.Location = new System.Drawing.Point(126, 166);
            this.ingressDate.MinDate = new System.DateTime(1950, 1, 1, 0, 0, 0, 0);
            this.ingressDate.Name = "ingressDate";
            this.ingressDate.Size = new System.Drawing.Size(136, 20);
            this.ingressDate.TabIndex = 6;
            this.ingressDate.Tag = "";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(269, 138);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(16, 18);
            this.label17.TabIndex = 0;
            this.label17.Text = "*";
            // 
            // legajoTxt
            // 
            this.legajoTxt.Location = new System.Drawing.Point(126, 136);
            this.legajoTxt.MaxLength = 8;
            this.legajoTxt.Name = "legajoTxt";
            this.legajoTxt.Size = new System.Drawing.Size(136, 20);
            this.legajoTxt.TabIndex = 5;
            this.legajoTxt.Tag = "";
            this.legajoTxt.TextChanged += new System.EventHandler(this.legajoTxt_TextChanged);
            this.legajoTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.legajoTxt_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(11, 137);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(50, 16);
            this.label18.TabIndex = 97;
            this.label18.Text = "Legajo";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(11, 84);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(58, 16);
            this.Label3.TabIndex = 0;
            this.Label3.Text = "Apellido";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(12, 58);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(57, 16);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Nombre";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Honeydew;
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.countryTxt);
            this.groupBox2.Controls.Add(this.zipCodeTxt);
            this.groupBox2.Controls.Add(this.stateTxt);
            this.groupBox2.Controls.Add(this.cityTxt);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.personalMailTxt);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.Label40);
            this.groupBox2.Controls.Add(this.Label27);
            this.groupBox2.Controls.Add(this.Label26);
            this.groupBox2.Controls.Add(this.schoolMailTxt);
            this.groupBox2.Controls.Add(this.Label16);
            this.groupBox2.Controls.Add(this.cellPhoneTxt);
            this.groupBox2.Controls.Add(this.Label15);
            this.groupBox2.Controls.Add(this.phoneTxt);
            this.groupBox2.Controls.Add(this.Label14);
            this.groupBox2.Controls.Add(this.Label13);
            this.groupBox2.Controls.Add(this.Label12);
            this.groupBox2.Controls.Add(this.Label11);
            this.groupBox2.Controls.Add(this.Label8);
            this.groupBox2.Controls.Add(this.addressTxt);
            this.groupBox2.Controls.Add(this.Label7);
            this.groupBox2.Location = new System.Drawing.Point(358, 98);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(285, 257);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Contacto";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(245, 177);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(16, 18);
            this.label25.TabIndex = 0;
            this.label25.Text = "*";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(245, 124);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(16, 18);
            this.label24.TabIndex = 0;
            this.label24.Text = "*";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(245, 98);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(16, 18);
            this.label23.TabIndex = 0;
            this.label23.Text = "*";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(245, 72);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(16, 18);
            this.label22.TabIndex = 0;
            this.label22.Text = "*";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(245, 45);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(16, 18);
            this.label21.TabIndex = 0;
            this.label21.Text = "*";
            // 
            // countryTxt
            // 
            this.countryTxt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.countryTxt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.FileSystem;
            this.countryTxt.BackColor = System.Drawing.Color.White;
            this.countryTxt.Location = new System.Drawing.Point(139, 125);
            this.countryTxt.MaxLength = 100;
            this.countryTxt.Name = "countryTxt";
            this.countryTxt.Size = new System.Drawing.Size(103, 20);
            this.countryTxt.TabIndex = 12;
            this.countryTxt.Tag = "";
            // 
            // zipCodeTxt
            // 
            this.zipCodeTxt.BackColor = System.Drawing.Color.White;
            this.zipCodeTxt.Location = new System.Drawing.Point(139, 100);
            this.zipCodeTxt.MaxLength = 4;
            this.zipCodeTxt.Name = "zipCodeTxt";
            this.zipCodeTxt.Size = new System.Drawing.Size(103, 20);
            this.zipCodeTxt.TabIndex = 11;
            this.zipCodeTxt.Tag = "";
            this.zipCodeTxt.TextChanged += new System.EventHandler(this.zipCodeTxt_TextChanged);
            this.zipCodeTxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.zipCodeTxt_KeyPress);
            // 
            // stateTxt
            // 
            this.stateTxt.BackColor = System.Drawing.Color.White;
            this.stateTxt.Location = new System.Drawing.Point(139, 72);
            this.stateTxt.MaxLength = 100;
            this.stateTxt.Name = "stateTxt";
            this.stateTxt.Size = new System.Drawing.Size(103, 20);
            this.stateTxt.TabIndex = 10;
            this.stateTxt.Tag = "";
            this.stateTxt.TextChanged += new System.EventHandler(this.stateTxt_TextChanged);
            // 
            // cityTxt
            // 
            this.cityTxt.BackColor = System.Drawing.Color.White;
            this.cityTxt.Location = new System.Drawing.Point(140, 46);
            this.cityTxt.MaxLength = 100;
            this.cityTxt.Name = "cityTxt";
            this.cityTxt.Size = new System.Drawing.Size(103, 20);
            this.cityTxt.TabIndex = 9;
            this.cityTxt.Tag = "";
            this.cityTxt.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(245, 230);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "*";
            // 
            // personalMailTxt
            // 
            this.personalMailTxt.Location = new System.Drawing.Point(140, 228);
            this.personalMailTxt.Name = "personalMailTxt";
            this.personalMailTxt.Size = new System.Drawing.Size(103, 20);
            this.personalMailTxt.TabIndex = 16;
            this.personalMailTxt.Tag = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(35, 228);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(89, 16);
            this.label10.TabIndex = 37;
            this.label10.Text = "Mail personal";
            // 
            // Label40
            // 
            this.Label40.AutoSize = true;
            this.Label40.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label40.ForeColor = System.Drawing.Color.Red;
            this.Label40.Location = new System.Drawing.Point(245, 205);
            this.Label40.Name = "Label40";
            this.Label40.Size = new System.Drawing.Size(16, 18);
            this.Label40.TabIndex = 0;
            this.Label40.Text = "*";
            // 
            // Label27
            // 
            this.Label27.AutoSize = true;
            this.Label27.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label27.ForeColor = System.Drawing.Color.Red;
            this.Label27.Location = new System.Drawing.Point(245, 23);
            this.Label27.Name = "Label27";
            this.Label27.Size = new System.Drawing.Size(16, 18);
            this.Label27.TabIndex = 0;
            this.Label27.Text = "*";
            // 
            // Label26
            // 
            this.Label26.AutoSize = true;
            this.Label26.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label26.ForeColor = System.Drawing.Color.Red;
            this.Label26.Location = new System.Drawing.Point(245, 153);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(16, 18);
            this.Label26.TabIndex = 0;
            this.Label26.Text = "*";
            // 
            // schoolMailTxt
            // 
            this.schoolMailTxt.Location = new System.Drawing.Point(140, 203);
            this.schoolMailTxt.Name = "schoolMailTxt";
            this.schoolMailTxt.Size = new System.Drawing.Size(103, 20);
            this.schoolMailTxt.TabIndex = 15;
            this.schoolMailTxt.Tag = "";
            this.schoolMailTxt.TextChanged += new System.EventHandler(this.schoolMailTxt_TextChanged);
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label16.Location = new System.Drawing.Point(35, 203);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(105, 16);
            this.Label16.TabIndex = 21;
            this.Label16.Text = "Mail institucional";
            // 
            // cellPhoneTxt
            // 
            this.cellPhoneTxt.BackColor = System.Drawing.Color.White;
            this.cellPhoneTxt.Location = new System.Drawing.Point(140, 177);
            this.cellPhoneTxt.MaxLength = 12;
            this.cellPhoneTxt.Name = "cellPhoneTxt";
            this.cellPhoneTxt.Size = new System.Drawing.Size(103, 20);
            this.cellPhoneTxt.TabIndex = 14;
            this.cellPhoneTxt.Tag = "";
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label15.Location = new System.Drawing.Point(35, 177);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(50, 16);
            this.Label15.TabIndex = 20;
            this.Label15.Text = "Celular";
            // 
            // phoneTxt
            // 
            this.phoneTxt.Location = new System.Drawing.Point(140, 151);
            this.phoneTxt.MaxLength = 12;
            this.phoneTxt.Name = "phoneTxt";
            this.phoneTxt.Size = new System.Drawing.Size(103, 20);
            this.phoneTxt.TabIndex = 13;
            this.phoneTxt.Tag = "";
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label14.Location = new System.Drawing.Point(35, 151);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(62, 16);
            this.Label14.TabIndex = 19;
            this.Label14.Text = "Teléfono";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label13.Location = new System.Drawing.Point(35, 125);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(89, 16);
            this.Label13.TabIndex = 18;
            this.Label13.Text = "Nacionalidad";
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label12.Location = new System.Drawing.Point(35, 99);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(29, 16);
            this.Label12.TabIndex = 17;
            this.Label12.Text = "C.P";
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label11.Location = new System.Drawing.Point(35, 73);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(64, 16);
            this.Label11.TabIndex = 16;
            this.Label11.Text = "Provincia";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.Location = new System.Drawing.Point(35, 47);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(68, 16);
            this.Label8.TabIndex = 15;
            this.Label8.Text = "Localidad";
            // 
            // addressTxt
            // 
            this.addressTxt.BackColor = System.Drawing.Color.White;
            this.addressTxt.Location = new System.Drawing.Point(140, 21);
            this.addressTxt.MaxLength = 100;
            this.addressTxt.Name = "addressTxt";
            this.addressTxt.Size = new System.Drawing.Size(103, 20);
            this.addressTxt.TabIndex = 8;
            this.addressTxt.Tag = "";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.Location = new System.Drawing.Point(35, 21);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(65, 16);
            this.Label7.TabIndex = 22;
            this.Label7.Text = "Dirección";
            // 
            // AltaPersonal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(814, 564);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.Label38);
            this.Controls.Add(this.group2);
            this.Controls.Add(this.Label37);
            this.Controls.Add(this.GroupBox1);
            this.Name = "Alta de personal";
            this.Text = "Alta de personal";
            this.Load += new System.EventHandler(this.AltaPersonal_Load);
            this.group2.ResumeLayout(false);
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label29;
        internal System.Windows.Forms.Label Label28;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.DateTimePicker dateDTP;
        internal System.Windows.Forms.Label Label38;
        internal System.Windows.Forms.Button CerrarBT;
        internal System.Windows.Forms.Button AgregarBT;
        internal System.Windows.Forms.GroupBox group2;
        internal System.Windows.Forms.TextBox lastNameTxt;
        internal System.Windows.Forms.TextBox nameTxt;
        internal System.Windows.Forms.TextBox DNITxt;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label37;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.GroupBox groupBox2;
        internal System.Windows.Forms.Label Label40;
        internal System.Windows.Forms.Label Label27;
        internal System.Windows.Forms.Label Label26;
        internal System.Windows.Forms.TextBox schoolMailTxt;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.TextBox cellPhoneTxt;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.TextBox phoneTxt;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.TextBox addressTxt;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.TextBox personalMailTxt;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label label17;
        internal System.Windows.Forms.TextBox legajoTxt;
        internal System.Windows.Forms.Label label18;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.DateTimePicker ingressDate;
        internal System.Windows.Forms.TextBox cityTxt;
        internal System.Windows.Forms.TextBox stateTxt;
        internal System.Windows.Forms.TextBox zipCodeTxt;
        internal System.Windows.Forms.TextBox countryTxt;
        internal System.Windows.Forms.Label labelAccessType;
        private System.Windows.Forms.ComboBox employeeTypeCb;
        internal System.Windows.Forms.Label redLabelAccessType;
        internal System.Windows.Forms.Label label20;
        internal System.Windows.Forms.Label label19;
        internal System.Windows.Forms.Label label25;
        internal System.Windows.Forms.Label label24;
        internal System.Windows.Forms.Label label23;
        internal System.Windows.Forms.Label label22;
        internal System.Windows.Forms.Label label21;
    }
}