﻿namespace Presentismo_2015
{
    partial class Secundaria_tecnica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows myCustomForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button2 = new System.Windows.Forms.Button();
            this.Button1 = new System.Windows.Forms.Button();
            this.ComboBox21 = new System.Windows.Forms.ComboBox();
            this.ComboBox22 = new System.Windows.Forms.ComboBox();
            this.ComboBox23 = new System.Windows.Forms.ComboBox();
            this.ComboBox24 = new System.Windows.Forms.ComboBox();
            this.ComboBox25 = new System.Windows.Forms.ComboBox();
            this.ComboBox26 = new System.Windows.Forms.ComboBox();
            this.ComboBox27 = new System.Windows.Forms.ComboBox();
            this.ComboBox28 = new System.Windows.Forms.ComboBox();
            this.ComboBox29 = new System.Windows.Forms.ComboBox();
            this.ComboBox30 = new System.Windows.Forms.ComboBox();
            this.ComboBox31 = new System.Windows.Forms.ComboBox();
            this.ComboBox32 = new System.Windows.Forms.ComboBox();
            this.ComboBox33 = new System.Windows.Forms.ComboBox();
            this.ComboBox34 = new System.Windows.Forms.ComboBox();
            this.ComboBox35 = new System.Windows.Forms.ComboBox();
            this.ComboBox36 = new System.Windows.Forms.ComboBox();
            this.ComboBox37 = new System.Windows.Forms.ComboBox();
            this.ComboBox38 = new System.Windows.Forms.ComboBox();
            this.ComboBox39 = new System.Windows.Forms.ComboBox();
            this.ComboBox40 = new System.Windows.Forms.ComboBox();
            this.ComboBox41 = new System.Windows.Forms.ComboBox();
            this.ComboBox42 = new System.Windows.Forms.ComboBox();
            this.ComboBox43 = new System.Windows.Forms.ComboBox();
            this.ComboBox44 = new System.Windows.Forms.ComboBox();
            this.ComboBox45 = new System.Windows.Forms.ComboBox();
            this.ComboBox46 = new System.Windows.Forms.ComboBox();
            this.ComboBox47 = new System.Windows.Forms.ComboBox();
            this.ComboBox48 = new System.Windows.Forms.ComboBox();
            this.ComboBox49 = new System.Windows.Forms.ComboBox();
            this.ComboBox50 = new System.Windows.Forms.ComboBox();
            this.ComboBox51 = new System.Windows.Forms.ComboBox();
            this.ComboBox52 = new System.Windows.Forms.ComboBox();
            this.ComboBox53 = new System.Windows.Forms.ComboBox();
            this.ComboBox54 = new System.Windows.Forms.ComboBox();
            this.ComboBox55 = new System.Windows.Forms.ComboBox();
            this.ComboBox56 = new System.Windows.Forms.ComboBox();
            this.ComboBox17 = new System.Windows.Forms.ComboBox();
            this.ComboBox18 = new System.Windows.Forms.ComboBox();
            this.ComboBox19 = new System.Windows.Forms.ComboBox();
            this.ComboBox20 = new System.Windows.Forms.ComboBox();
            this.Label17 = new System.Windows.Forms.Label();
            this.ComboBox13 = new System.Windows.Forms.ComboBox();
            this.ComboBox14 = new System.Windows.Forms.ComboBox();
            this.ComboBox15 = new System.Windows.Forms.ComboBox();
            this.ComboBox16 = new System.Windows.Forms.ComboBox();
            this.Label16 = new System.Windows.Forms.Label();
            this.Label15 = new System.Windows.Forms.Label();
            this.Label14 = new System.Windows.Forms.Label();
            this.TextBox2 = new System.Windows.Forms.TextBox();
            this.TextBox1 = new System.Windows.Forms.TextBox();
            this.ComboBox5 = new System.Windows.Forms.ComboBox();
            this.ComboBox6 = new System.Windows.Forms.ComboBox();
            this.ComboBox7 = new System.Windows.Forms.ComboBox();
            this.ComboBox8 = new System.Windows.Forms.ComboBox();
            this.ComboBox1 = new System.Windows.Forms.ComboBox();
            this.ComboBox2 = new System.Windows.Forms.ComboBox();
            this.ComboBox3 = new System.Windows.Forms.ComboBox();
            this.ComboBox4 = new System.Windows.Forms.ComboBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.ComboBox12 = new System.Windows.Forms.ComboBox();
            this.ComboBox11 = new System.Windows.Forms.ComboBox();
            this.ComboBox10 = new System.Windows.Forms.ComboBox();
            this.ComboBox9 = new System.Windows.Forms.ComboBox();
            this.HrsCB4 = new System.Windows.Forms.ComboBox();
            this.HrsCB3 = new System.Windows.Forms.ComboBox();
            this.HrsCB2 = new System.Windows.Forms.ComboBox();
            this.HrsCB1 = new System.Windows.Forms.ComboBox();
            this.DivisionCB4 = new System.Windows.Forms.ComboBox();
            this.DivisionCB3 = new System.Windows.Forms.ComboBox();
            this.DivisionCB2 = new System.Windows.Forms.ComboBox();
            this.DivisionCB1 = new System.Windows.Forms.ComboBox();
            this.Curso4CB = new System.Windows.Forms.ComboBox();
            this.Curso3CB = new System.Windows.Forms.ComboBox();
            this.Curso2CB = new System.Windows.Forms.ComboBox();
            this.Curso1CB = new System.Windows.Forms.ComboBox();
            this.AsigCB4 = new System.Windows.Forms.ComboBox();
            this.AsigCB3 = new System.Windows.Forms.ComboBox();
            this.AsigCB2 = new System.Windows.Forms.ComboBox();
            this.ComboBox64 = new System.Windows.Forms.ComboBox();
            this.ComboBox63 = new System.Windows.Forms.ComboBox();
            this.ComboBox62 = new System.Windows.Forms.ComboBox();
            this.ComboBox61 = new System.Windows.Forms.ComboBox();
            this.ComboBox60 = new System.Windows.Forms.ComboBox();
            this.ComboBox59 = new System.Windows.Forms.ComboBox();
            this.ComboBox58 = new System.Windows.Forms.ComboBox();
            this.ComboBox73 = new System.Windows.Forms.ComboBox();
            this.ComboBox72 = new System.Windows.Forms.ComboBox();
            this.ComboBox71 = new System.Windows.Forms.ComboBox();
            this.ComboBox70 = new System.Windows.Forms.ComboBox();
            this.ComboBox69 = new System.Windows.Forms.ComboBox();
            this.ComboBox68 = new System.Windows.Forms.ComboBox();
            this.ComboBox67 = new System.Windows.Forms.ComboBox();
            this.ComboBox66 = new System.Windows.Forms.ComboBox();
            this.ComboBox57 = new System.Windows.Forms.ComboBox();
            this.AsigCB1 = new System.Windows.Forms.ComboBox();
            this.Label23 = new System.Windows.Forms.Label();
            this.Label10 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.totalTXT = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label24 = new System.Windows.Forms.Label();
            this.Label22 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.TextBox3 = new System.Windows.Forms.TextBox();
            this.DNIsbTXT = new System.Windows.Forms.TextBox();
            this.Label21 = new System.Windows.Forms.Label();
            this.Label20 = new System.Windows.Forms.Label();
            this.Label19 = new System.Windows.Forms.Label();
            this.Label18 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Button2
            // 
            this.Button2.BackColor = System.Drawing.Color.White;
            this.Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Button2.Image = global::Presentismo_2015.Properties.Resources.door_out;
            this.Button2.Location = new System.Drawing.Point(1006, 436);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(56, 65);
            this.Button2.TabIndex = 379;
            this.Button2.Text = "Cerrar";
            this.Button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Button2.UseVisualStyleBackColor = false;
            // 
            // Button1
            // 
            this.Button1.BackColor = System.Drawing.Color.White;
            this.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button1.Image = global::Presentismo_2015.Properties.Resources.add;
            this.Button1.Location = new System.Drawing.Point(356, 434);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(56, 67);
            this.Button1.TabIndex = 378;
            this.Button1.Text = "Agregar";
            this.Button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Button1.UseVisualStyleBackColor = false;
            // 
            // ComboBox21
            // 
            this.ComboBox21.FormattingEnabled = true;
            this.ComboBox21.Items.AddRange(new object[] {
            "Con Aporte",
            "Sin Aporte"});
            this.ComboBox21.Location = new System.Drawing.Point(968, 365);
            this.ComboBox21.Name = "ComboBox21";
            this.ComboBox21.Size = new System.Drawing.Size(112, 21);
            this.ComboBox21.TabIndex = 377;
            // 
            // ComboBox22
            // 
            this.ComboBox22.FormattingEnabled = true;
            this.ComboBox22.Items.AddRange(new object[] {
            "Con Aporte",
            "Sin Aporte"});
            this.ComboBox22.Location = new System.Drawing.Point(968, 329);
            this.ComboBox22.Name = "ComboBox22";
            this.ComboBox22.Size = new System.Drawing.Size(112, 21);
            this.ComboBox22.TabIndex = 376;
            // 
            // ComboBox23
            // 
            this.ComboBox23.FormattingEnabled = true;
            this.ComboBox23.Items.AddRange(new object[] {
            "Con Aporte",
            "Sin Aporte"});
            this.ComboBox23.Location = new System.Drawing.Point(968, 293);
            this.ComboBox23.Name = "ComboBox23";
            this.ComboBox23.Size = new System.Drawing.Size(112, 21);
            this.ComboBox23.TabIndex = 375;
            // 
            // ComboBox24
            // 
            this.ComboBox24.FormattingEnabled = true;
            this.ComboBox24.Items.AddRange(new object[] {
            "Con Aporte",
            "Sin Aporte"});
            this.ComboBox24.Location = new System.Drawing.Point(968, 257);
            this.ComboBox24.Name = "ComboBox24";
            this.ComboBox24.Size = new System.Drawing.Size(112, 21);
            this.ComboBox24.TabIndex = 374;
            // 
            // ComboBox25
            // 
            this.ComboBox25.FormattingEnabled = true;
            this.ComboBox25.Location = new System.Drawing.Point(897, 365);
            this.ComboBox25.Name = "ComboBox25";
            this.ComboBox25.Size = new System.Drawing.Size(57, 21);
            this.ComboBox25.TabIndex = 373;
            // 
            // ComboBox26
            // 
            this.ComboBox26.FormattingEnabled = true;
            this.ComboBox26.Location = new System.Drawing.Point(897, 329);
            this.ComboBox26.Name = "ComboBox26";
            this.ComboBox26.Size = new System.Drawing.Size(57, 21);
            this.ComboBox26.TabIndex = 372;
            // 
            // ComboBox27
            // 
            this.ComboBox27.FormattingEnabled = true;
            this.ComboBox27.Location = new System.Drawing.Point(897, 293);
            this.ComboBox27.Name = "ComboBox27";
            this.ComboBox27.Size = new System.Drawing.Size(57, 21);
            this.ComboBox27.TabIndex = 371;
            // 
            // ComboBox28
            // 
            this.ComboBox28.FormattingEnabled = true;
            this.ComboBox28.Location = new System.Drawing.Point(897, 257);
            this.ComboBox28.Name = "ComboBox28";
            this.ComboBox28.Size = new System.Drawing.Size(57, 21);
            this.ComboBox28.TabIndex = 370;
            // 
            // ComboBox29
            // 
            this.ComboBox29.FormattingEnabled = true;
            this.ComboBox29.Location = new System.Drawing.Point(766, 365);
            this.ComboBox29.Name = "ComboBox29";
            this.ComboBox29.Size = new System.Drawing.Size(57, 21);
            this.ComboBox29.TabIndex = 369;
            // 
            // ComboBox30
            // 
            this.ComboBox30.FormattingEnabled = true;
            this.ComboBox30.Location = new System.Drawing.Point(766, 329);
            this.ComboBox30.Name = "ComboBox30";
            this.ComboBox30.Size = new System.Drawing.Size(57, 21);
            this.ComboBox30.TabIndex = 368;
            // 
            // ComboBox31
            // 
            this.ComboBox31.FormattingEnabled = true;
            this.ComboBox31.Location = new System.Drawing.Point(766, 293);
            this.ComboBox31.Name = "ComboBox31";
            this.ComboBox31.Size = new System.Drawing.Size(57, 21);
            this.ComboBox31.TabIndex = 367;
            // 
            // ComboBox32
            // 
            this.ComboBox32.FormattingEnabled = true;
            this.ComboBox32.Location = new System.Drawing.Point(766, 257);
            this.ComboBox32.Name = "ComboBox32";
            this.ComboBox32.Size = new System.Drawing.Size(57, 21);
            this.ComboBox32.TabIndex = 366;
            // 
            // ComboBox33
            // 
            this.ComboBox33.FormattingEnabled = true;
            this.ComboBox33.Location = new System.Drawing.Point(677, 365);
            this.ComboBox33.Name = "ComboBox33";
            this.ComboBox33.Size = new System.Drawing.Size(57, 21);
            this.ComboBox33.TabIndex = 365;
            // 
            // ComboBox34
            // 
            this.ComboBox34.FormattingEnabled = true;
            this.ComboBox34.Location = new System.Drawing.Point(677, 329);
            this.ComboBox34.Name = "ComboBox34";
            this.ComboBox34.Size = new System.Drawing.Size(57, 21);
            this.ComboBox34.TabIndex = 364;
            // 
            // ComboBox35
            // 
            this.ComboBox35.FormattingEnabled = true;
            this.ComboBox35.Location = new System.Drawing.Point(677, 293);
            this.ComboBox35.Name = "ComboBox35";
            this.ComboBox35.Size = new System.Drawing.Size(57, 21);
            this.ComboBox35.TabIndex = 363;
            // 
            // ComboBox36
            // 
            this.ComboBox36.FormattingEnabled = true;
            this.ComboBox36.Location = new System.Drawing.Point(677, 257);
            this.ComboBox36.Name = "ComboBox36";
            this.ComboBox36.Size = new System.Drawing.Size(57, 21);
            this.ComboBox36.TabIndex = 362;
            // 
            // ComboBox37
            // 
            this.ComboBox37.FormattingEnabled = true;
            this.ComboBox37.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes"});
            this.ComboBox37.Location = new System.Drawing.Point(589, 365);
            this.ComboBox37.Name = "ComboBox37";
            this.ComboBox37.Size = new System.Drawing.Size(73, 21);
            this.ComboBox37.TabIndex = 361;
            // 
            // ComboBox38
            // 
            this.ComboBox38.FormattingEnabled = true;
            this.ComboBox38.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes"});
            this.ComboBox38.Location = new System.Drawing.Point(589, 329);
            this.ComboBox38.Name = "ComboBox38";
            this.ComboBox38.Size = new System.Drawing.Size(73, 21);
            this.ComboBox38.TabIndex = 360;
            // 
            // ComboBox39
            // 
            this.ComboBox39.FormattingEnabled = true;
            this.ComboBox39.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes"});
            this.ComboBox39.Location = new System.Drawing.Point(589, 293);
            this.ComboBox39.Name = "ComboBox39";
            this.ComboBox39.Size = new System.Drawing.Size(73, 21);
            this.ComboBox39.TabIndex = 359;
            // 
            // ComboBox40
            // 
            this.ComboBox40.FormattingEnabled = true;
            this.ComboBox40.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes"});
            this.ComboBox40.Location = new System.Drawing.Point(589, 257);
            this.ComboBox40.Name = "ComboBox40";
            this.ComboBox40.Size = new System.Drawing.Size(73, 21);
            this.ComboBox40.TabIndex = 358;
            // 
            // ComboBox41
            // 
            this.ComboBox41.FormattingEnabled = true;
            this.ComboBox41.Location = new System.Drawing.Point(846, 368);
            this.ComboBox41.Name = "ComboBox41";
            this.ComboBox41.Size = new System.Drawing.Size(38, 21);
            this.ComboBox41.TabIndex = 357;
            this.ComboBox41.Text = "0";
            // 
            // ComboBox42
            // 
            this.ComboBox42.FormattingEnabled = true;
            this.ComboBox42.Location = new System.Drawing.Point(846, 331);
            this.ComboBox42.Name = "ComboBox42";
            this.ComboBox42.Size = new System.Drawing.Size(38, 21);
            this.ComboBox42.TabIndex = 356;
            this.ComboBox42.Text = "0";
            // 
            // ComboBox43
            // 
            this.ComboBox43.FormattingEnabled = true;
            this.ComboBox43.Location = new System.Drawing.Point(846, 294);
            this.ComboBox43.Name = "ComboBox43";
            this.ComboBox43.Size = new System.Drawing.Size(38, 21);
            this.ComboBox43.TabIndex = 355;
            this.ComboBox43.Text = "0";
            // 
            // ComboBox44
            // 
            this.ComboBox44.FormattingEnabled = true;
            this.ComboBox44.Location = new System.Drawing.Point(846, 257);
            this.ComboBox44.Name = "ComboBox44";
            this.ComboBox44.Size = new System.Drawing.Size(38, 21);
            this.ComboBox44.TabIndex = 354;
            this.ComboBox44.Text = "0";
            // 
            // ComboBox45
            // 
            this.ComboBox45.FormattingEnabled = true;
            this.ComboBox45.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.ComboBox45.Location = new System.Drawing.Point(539, 365);
            this.ComboBox45.Name = "ComboBox45";
            this.ComboBox45.Size = new System.Drawing.Size(34, 21);
            this.ComboBox45.TabIndex = 353;
            // 
            // ComboBox46
            // 
            this.ComboBox46.FormattingEnabled = true;
            this.ComboBox46.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.ComboBox46.Location = new System.Drawing.Point(539, 329);
            this.ComboBox46.Name = "ComboBox46";
            this.ComboBox46.Size = new System.Drawing.Size(34, 21);
            this.ComboBox46.TabIndex = 352;
            // 
            // ComboBox47
            // 
            this.ComboBox47.FormattingEnabled = true;
            this.ComboBox47.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.ComboBox47.Location = new System.Drawing.Point(539, 293);
            this.ComboBox47.Name = "ComboBox47";
            this.ComboBox47.Size = new System.Drawing.Size(34, 21);
            this.ComboBox47.TabIndex = 351;
            // 
            // ComboBox48
            // 
            this.ComboBox48.FormattingEnabled = true;
            this.ComboBox48.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.ComboBox48.Location = new System.Drawing.Point(539, 257);
            this.ComboBox48.Name = "ComboBox48";
            this.ComboBox48.Size = new System.Drawing.Size(34, 21);
            this.ComboBox48.TabIndex = 350;
            // 
            // ComboBox49
            // 
            this.ComboBox49.FormattingEnabled = true;
            this.ComboBox49.Items.AddRange(new object[] {
            "1°",
            "2°",
            "3°",
            "4°",
            "5°",
            "6°",
            "7°",
            "8°",
            "9°"});
            this.ComboBox49.Location = new System.Drawing.Point(486, 365);
            this.ComboBox49.Name = "ComboBox49";
            this.ComboBox49.Size = new System.Drawing.Size(33, 21);
            this.ComboBox49.TabIndex = 349;
            // 
            // ComboBox50
            // 
            this.ComboBox50.FormattingEnabled = true;
            this.ComboBox50.Items.AddRange(new object[] {
            "1°",
            "2°",
            "3°",
            "4°",
            "5°",
            "6°",
            "7°",
            "8°",
            "9°"});
            this.ComboBox50.Location = new System.Drawing.Point(486, 329);
            this.ComboBox50.Name = "ComboBox50";
            this.ComboBox50.Size = new System.Drawing.Size(33, 21);
            this.ComboBox50.TabIndex = 348;
            // 
            // ComboBox51
            // 
            this.ComboBox51.FormattingEnabled = true;
            this.ComboBox51.Items.AddRange(new object[] {
            "1°",
            "2°",
            "3°",
            "4°",
            "5°",
            "6°",
            "7°",
            "8°",
            "9°"});
            this.ComboBox51.Location = new System.Drawing.Point(486, 293);
            this.ComboBox51.Name = "ComboBox51";
            this.ComboBox51.Size = new System.Drawing.Size(33, 21);
            this.ComboBox51.TabIndex = 347;
            // 
            // ComboBox52
            // 
            this.ComboBox52.FormattingEnabled = true;
            this.ComboBox52.Items.AddRange(new object[] {
            "1°",
            "2°",
            "3°",
            "4°",
            "5°",
            "6°",
            "7°",
            "8°",
            "9°"});
            this.ComboBox52.Location = new System.Drawing.Point(486, 257);
            this.ComboBox52.Name = "ComboBox52";
            this.ComboBox52.Size = new System.Drawing.Size(33, 21);
            this.ComboBox52.TabIndex = 346;
            // 
            // ComboBox53
            // 
            this.ComboBox53.FormattingEnabled = true;
            this.ComboBox53.Location = new System.Drawing.Point(353, 365);
            this.ComboBox53.Name = "ComboBox53";
            this.ComboBox53.Size = new System.Drawing.Size(121, 21);
            this.ComboBox53.TabIndex = 345;
            // 
            // ComboBox54
            // 
            this.ComboBox54.FormattingEnabled = true;
            this.ComboBox54.Location = new System.Drawing.Point(353, 329);
            this.ComboBox54.Name = "ComboBox54";
            this.ComboBox54.Size = new System.Drawing.Size(121, 21);
            this.ComboBox54.TabIndex = 344;
            // 
            // ComboBox55
            // 
            this.ComboBox55.FormattingEnabled = true;
            this.ComboBox55.Location = new System.Drawing.Point(353, 293);
            this.ComboBox55.Name = "ComboBox55";
            this.ComboBox55.Size = new System.Drawing.Size(121, 21);
            this.ComboBox55.TabIndex = 343;
            // 
            // ComboBox56
            // 
            this.ComboBox56.FormattingEnabled = true;
            this.ComboBox56.Location = new System.Drawing.Point(353, 257);
            this.ComboBox56.Name = "ComboBox56";
            this.ComboBox56.Size = new System.Drawing.Size(121, 21);
            this.ComboBox56.TabIndex = 342;
            // 
            // ComboBox17
            // 
            this.ComboBox17.FormattingEnabled = true;
            this.ComboBox17.Items.AddRange(new object[] {
            "Con Aporte",
            "Sin Aporte"});
            this.ComboBox17.Location = new System.Drawing.Point(968, 221);
            this.ComboBox17.Name = "ComboBox17";
            this.ComboBox17.Size = new System.Drawing.Size(112, 21);
            this.ComboBox17.TabIndex = 341;
            // 
            // ComboBox18
            // 
            this.ComboBox18.FormattingEnabled = true;
            this.ComboBox18.Items.AddRange(new object[] {
            "Con Aporte",
            "Sin Aporte"});
            this.ComboBox18.Location = new System.Drawing.Point(968, 185);
            this.ComboBox18.Name = "ComboBox18";
            this.ComboBox18.Size = new System.Drawing.Size(112, 21);
            this.ComboBox18.TabIndex = 340;
            // 
            // ComboBox19
            // 
            this.ComboBox19.FormattingEnabled = true;
            this.ComboBox19.Items.AddRange(new object[] {
            "Con Aporte",
            "Sin Aporte"});
            this.ComboBox19.Location = new System.Drawing.Point(968, 149);
            this.ComboBox19.Name = "ComboBox19";
            this.ComboBox19.Size = new System.Drawing.Size(112, 21);
            this.ComboBox19.TabIndex = 339;
            // 
            // ComboBox20
            // 
            this.ComboBox20.FormattingEnabled = true;
            this.ComboBox20.Items.AddRange(new object[] {
            "Con Aporte",
            "Sin Aporte",
            "Extraprogramatica"});
            this.ComboBox20.Location = new System.Drawing.Point(968, 113);
            this.ComboBox20.Name = "ComboBox20";
            this.ComboBox20.Size = new System.Drawing.Size(112, 21);
            this.ComboBox20.TabIndex = 338;
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Location = new System.Drawing.Point(979, 76);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(43, 13);
            this.Label17.TabIndex = 337;
            this.Label17.Text = "Aportes";
            // 
            // ComboBox13
            // 
            this.ComboBox13.FormattingEnabled = true;
            this.ComboBox13.Location = new System.Drawing.Point(897, 221);
            this.ComboBox13.Name = "ComboBox13";
            this.ComboBox13.Size = new System.Drawing.Size(57, 21);
            this.ComboBox13.TabIndex = 336;
            // 
            // ComboBox14
            // 
            this.ComboBox14.FormattingEnabled = true;
            this.ComboBox14.Location = new System.Drawing.Point(897, 185);
            this.ComboBox14.Name = "ComboBox14";
            this.ComboBox14.Size = new System.Drawing.Size(57, 21);
            this.ComboBox14.TabIndex = 335;
            // 
            // ComboBox15
            // 
            this.ComboBox15.FormattingEnabled = true;
            this.ComboBox15.Location = new System.Drawing.Point(897, 149);
            this.ComboBox15.Name = "ComboBox15";
            this.ComboBox15.Size = new System.Drawing.Size(57, 21);
            this.ComboBox15.TabIndex = 334;
            // 
            // ComboBox16
            // 
            this.ComboBox16.FormattingEnabled = true;
            this.ComboBox16.Location = new System.Drawing.Point(897, 113);
            this.ComboBox16.Name = "ComboBox16";
            this.ComboBox16.Size = new System.Drawing.Size(57, 21);
            this.ComboBox16.TabIndex = 333;
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Location = new System.Drawing.Point(902, 76);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(52, 13);
            this.Label16.TabIndex = 332;
            this.Label16.Text = "Categoria";
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Location = new System.Drawing.Point(831, 35);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(44, 13);
            this.Label15.TabIndex = 331;
            this.Label15.Text = "Apellido";
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(663, 35);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(44, 13);
            this.Label14.TabIndex = 330;
            this.Label14.Text = "Nombre";
            // 
            // TextBox2
            // 
            this.TextBox2.Location = new System.Drawing.Point(883, 32);
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Size = new System.Drawing.Size(100, 20);
            this.TextBox2.TabIndex = 329;
            // 
            // TextBox1
            // 
            this.TextBox1.Location = new System.Drawing.Point(713, 32);
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Size = new System.Drawing.Size(100, 20);
            this.TextBox1.TabIndex = 328;
            // 
            // ComboBox5
            // 
            this.ComboBox5.FormattingEnabled = true;
            this.ComboBox5.Location = new System.Drawing.Point(766, 221);
            this.ComboBox5.Name = "ComboBox5";
            this.ComboBox5.Size = new System.Drawing.Size(57, 21);
            this.ComboBox5.TabIndex = 327;
            // 
            // ComboBox6
            // 
            this.ComboBox6.FormattingEnabled = true;
            this.ComboBox6.Location = new System.Drawing.Point(766, 185);
            this.ComboBox6.Name = "ComboBox6";
            this.ComboBox6.Size = new System.Drawing.Size(57, 21);
            this.ComboBox6.TabIndex = 326;
            // 
            // ComboBox7
            // 
            this.ComboBox7.FormattingEnabled = true;
            this.ComboBox7.Location = new System.Drawing.Point(766, 149);
            this.ComboBox7.Name = "ComboBox7";
            this.ComboBox7.Size = new System.Drawing.Size(57, 21);
            this.ComboBox7.TabIndex = 325;
            // 
            // ComboBox8
            // 
            this.ComboBox8.FormattingEnabled = true;
            this.ComboBox8.Location = new System.Drawing.Point(766, 113);
            this.ComboBox8.Name = "ComboBox8";
            this.ComboBox8.Size = new System.Drawing.Size(57, 21);
            this.ComboBox8.TabIndex = 324;
            // 
            // ComboBox1
            // 
            this.ComboBox1.FormattingEnabled = true;
            this.ComboBox1.Location = new System.Drawing.Point(677, 221);
            this.ComboBox1.Name = "ComboBox1";
            this.ComboBox1.Size = new System.Drawing.Size(57, 21);
            this.ComboBox1.TabIndex = 323;
            // 
            // ComboBox2
            // 
            this.ComboBox2.FormattingEnabled = true;
            this.ComboBox2.Location = new System.Drawing.Point(677, 185);
            this.ComboBox2.Name = "ComboBox2";
            this.ComboBox2.Size = new System.Drawing.Size(57, 21);
            this.ComboBox2.TabIndex = 322;
            // 
            // ComboBox3
            // 
            this.ComboBox3.FormattingEnabled = true;
            this.ComboBox3.Location = new System.Drawing.Point(677, 149);
            this.ComboBox3.Name = "ComboBox3";
            this.ComboBox3.Size = new System.Drawing.Size(57, 21);
            this.ComboBox3.TabIndex = 321;
            // 
            // ComboBox4
            // 
            this.ComboBox4.FormattingEnabled = true;
            this.ComboBox4.Location = new System.Drawing.Point(677, 113);
            this.ComboBox4.Name = "ComboBox4";
            this.ComboBox4.Size = new System.Drawing.Size(57, 21);
            this.ComboBox4.TabIndex = 320;
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(753, 76);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(79, 13);
            this.Label12.TabIndex = 317;
            this.Label12.Text = "Horario Finaliza";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(611, 76);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(23, 13);
            this.Label13.TabIndex = 318;
            this.Label13.Text = "Dia";
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(665, 76);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(69, 13);
            this.Label11.TabIndex = 319;
            this.Label11.Text = "Horario Inicio";
            // 
            // ComboBox12
            // 
            this.ComboBox12.FormattingEnabled = true;
            this.ComboBox12.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes"});
            this.ComboBox12.Location = new System.Drawing.Point(589, 221);
            this.ComboBox12.Name = "ComboBox12";
            this.ComboBox12.Size = new System.Drawing.Size(73, 21);
            this.ComboBox12.TabIndex = 316;
            // 
            // ComboBox11
            // 
            this.ComboBox11.FormattingEnabled = true;
            this.ComboBox11.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes"});
            this.ComboBox11.Location = new System.Drawing.Point(589, 185);
            this.ComboBox11.Name = "ComboBox11";
            this.ComboBox11.Size = new System.Drawing.Size(73, 21);
            this.ComboBox11.TabIndex = 315;
            // 
            // ComboBox10
            // 
            this.ComboBox10.FormattingEnabled = true;
            this.ComboBox10.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes"});
            this.ComboBox10.Location = new System.Drawing.Point(589, 149);
            this.ComboBox10.Name = "ComboBox10";
            this.ComboBox10.Size = new System.Drawing.Size(73, 21);
            this.ComboBox10.TabIndex = 314;
            // 
            // ComboBox9
            // 
            this.ComboBox9.FormattingEnabled = true;
            this.ComboBox9.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes"});
            this.ComboBox9.Location = new System.Drawing.Point(589, 113);
            this.ComboBox9.Name = "ComboBox9";
            this.ComboBox9.Size = new System.Drawing.Size(73, 21);
            this.ComboBox9.TabIndex = 313;
            // 
            // HrsCB4
            // 
            this.HrsCB4.FormattingEnabled = true;
            this.HrsCB4.Location = new System.Drawing.Point(846, 224);
            this.HrsCB4.Name = "HrsCB4";
            this.HrsCB4.Size = new System.Drawing.Size(38, 21);
            this.HrsCB4.TabIndex = 312;
            this.HrsCB4.Text = "0";
            // 
            // HrsCB3
            // 
            this.HrsCB3.FormattingEnabled = true;
            this.HrsCB3.Location = new System.Drawing.Point(846, 187);
            this.HrsCB3.Name = "HrsCB3";
            this.HrsCB3.Size = new System.Drawing.Size(38, 21);
            this.HrsCB3.TabIndex = 311;
            this.HrsCB3.Text = "0";
            // 
            // HrsCB2
            // 
            this.HrsCB2.FormattingEnabled = true;
            this.HrsCB2.Location = new System.Drawing.Point(846, 150);
            this.HrsCB2.Name = "HrsCB2";
            this.HrsCB2.Size = new System.Drawing.Size(38, 21);
            this.HrsCB2.TabIndex = 310;
            this.HrsCB2.Text = "0";
            // 
            // HrsCB1
            // 
            this.HrsCB1.FormattingEnabled = true;
            this.HrsCB1.Location = new System.Drawing.Point(846, 113);
            this.HrsCB1.Name = "HrsCB1";
            this.HrsCB1.Size = new System.Drawing.Size(38, 21);
            this.HrsCB1.TabIndex = 309;
            this.HrsCB1.Text = "0";
            // 
            // DivisionCB4
            // 
            this.DivisionCB4.FormattingEnabled = true;
            this.DivisionCB4.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.DivisionCB4.Location = new System.Drawing.Point(539, 221);
            this.DivisionCB4.Name = "DivisionCB4";
            this.DivisionCB4.Size = new System.Drawing.Size(34, 21);
            this.DivisionCB4.TabIndex = 308;
            // 
            // DivisionCB3
            // 
            this.DivisionCB3.FormattingEnabled = true;
            this.DivisionCB3.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.DivisionCB3.Location = new System.Drawing.Point(539, 185);
            this.DivisionCB3.Name = "DivisionCB3";
            this.DivisionCB3.Size = new System.Drawing.Size(34, 21);
            this.DivisionCB3.TabIndex = 307;
            // 
            // DivisionCB2
            // 
            this.DivisionCB2.FormattingEnabled = true;
            this.DivisionCB2.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.DivisionCB2.Location = new System.Drawing.Point(539, 149);
            this.DivisionCB2.Name = "DivisionCB2";
            this.DivisionCB2.Size = new System.Drawing.Size(34, 21);
            this.DivisionCB2.TabIndex = 306;
            // 
            // DivisionCB1
            // 
            this.DivisionCB1.FormattingEnabled = true;
            this.DivisionCB1.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.DivisionCB1.Location = new System.Drawing.Point(539, 113);
            this.DivisionCB1.Name = "DivisionCB1";
            this.DivisionCB1.Size = new System.Drawing.Size(34, 21);
            this.DivisionCB1.TabIndex = 305;
            // 
            // Curso4CB
            // 
            this.Curso4CB.FormattingEnabled = true;
            this.Curso4CB.Items.AddRange(new object[] {
            "1°",
            "2°",
            "3°",
            "4°",
            "5°",
            "6°",
            "7°",
            "8°",
            "9°"});
            this.Curso4CB.Location = new System.Drawing.Point(486, 221);
            this.Curso4CB.Name = "Curso4CB";
            this.Curso4CB.Size = new System.Drawing.Size(33, 21);
            this.Curso4CB.TabIndex = 304;
            // 
            // Curso3CB
            // 
            this.Curso3CB.FormattingEnabled = true;
            this.Curso3CB.Items.AddRange(new object[] {
            "1°",
            "2°",
            "3°",
            "4°",
            "5°",
            "6°",
            "7°",
            "8°",
            "9°"});
            this.Curso3CB.Location = new System.Drawing.Point(486, 185);
            this.Curso3CB.Name = "Curso3CB";
            this.Curso3CB.Size = new System.Drawing.Size(33, 21);
            this.Curso3CB.TabIndex = 303;
            // 
            // Curso2CB
            // 
            this.Curso2CB.FormattingEnabled = true;
            this.Curso2CB.Items.AddRange(new object[] {
            "1°",
            "2°",
            "3°",
            "4°",
            "5°",
            "6°",
            "7°",
            "8°",
            "9°"});
            this.Curso2CB.Location = new System.Drawing.Point(486, 149);
            this.Curso2CB.Name = "Curso2CB";
            this.Curso2CB.Size = new System.Drawing.Size(33, 21);
            this.Curso2CB.TabIndex = 302;
            // 
            // Curso1CB
            // 
            this.Curso1CB.FormattingEnabled = true;
            this.Curso1CB.Items.AddRange(new object[] {
            "1°",
            "2°",
            "3°",
            "4°",
            "5°",
            "6°",
            "7°",
            "8°",
            "9°"});
            this.Curso1CB.Location = new System.Drawing.Point(486, 113);
            this.Curso1CB.Name = "Curso1CB";
            this.Curso1CB.Size = new System.Drawing.Size(33, 21);
            this.Curso1CB.TabIndex = 301;
            // 
            // AsigCB4
            // 
            this.AsigCB4.FormattingEnabled = true;
            this.AsigCB4.Location = new System.Drawing.Point(353, 221);
            this.AsigCB4.Name = "AsigCB4";
            this.AsigCB4.Size = new System.Drawing.Size(121, 21);
            this.AsigCB4.TabIndex = 300;
            // 
            // AsigCB3
            // 
            this.AsigCB3.FormattingEnabled = true;
            this.AsigCB3.Location = new System.Drawing.Point(353, 185);
            this.AsigCB3.Name = "AsigCB3";
            this.AsigCB3.Size = new System.Drawing.Size(121, 21);
            this.AsigCB3.TabIndex = 299;
            // 
            // AsigCB2
            // 
            this.AsigCB2.FormattingEnabled = true;
            this.AsigCB2.Location = new System.Drawing.Point(353, 149);
            this.AsigCB2.Name = "AsigCB2";
            this.AsigCB2.Size = new System.Drawing.Size(121, 21);
            this.AsigCB2.TabIndex = 298;
            // 
            // ComboBox64
            // 
            this.ComboBox64.FormattingEnabled = true;
            this.ComboBox64.Items.AddRange(new object[] {
            "Titular",
            "Suplente"});
            this.ComboBox64.Location = new System.Drawing.Point(168, 364);
            this.ComboBox64.Name = "ComboBox64";
            this.ComboBox64.Size = new System.Drawing.Size(68, 21);
            this.ComboBox64.TabIndex = 286;
            // 
            // ComboBox63
            // 
            this.ComboBox63.FormattingEnabled = true;
            this.ComboBox63.Items.AddRange(new object[] {
            "Titular",
            "Suplente"});
            this.ComboBox63.Location = new System.Drawing.Point(167, 329);
            this.ComboBox63.Name = "ComboBox63";
            this.ComboBox63.Size = new System.Drawing.Size(68, 21);
            this.ComboBox63.TabIndex = 285;
            // 
            // ComboBox62
            // 
            this.ComboBox62.FormattingEnabled = true;
            this.ComboBox62.Items.AddRange(new object[] {
            "Titular",
            "Suplente"});
            this.ComboBox62.Location = new System.Drawing.Point(167, 294);
            this.ComboBox62.Name = "ComboBox62";
            this.ComboBox62.Size = new System.Drawing.Size(68, 21);
            this.ComboBox62.TabIndex = 288;
            // 
            // ComboBox61
            // 
            this.ComboBox61.FormattingEnabled = true;
            this.ComboBox61.Items.AddRange(new object[] {
            "Titular",
            "Suplente"});
            this.ComboBox61.Location = new System.Drawing.Point(167, 257);
            this.ComboBox61.Name = "ComboBox61";
            this.ComboBox61.Size = new System.Drawing.Size(68, 21);
            this.ComboBox61.TabIndex = 287;
            // 
            // ComboBox60
            // 
            this.ComboBox60.FormattingEnabled = true;
            this.ComboBox60.Items.AddRange(new object[] {
            "Titular",
            "Suplente"});
            this.ComboBox60.Location = new System.Drawing.Point(167, 221);
            this.ComboBox60.Name = "ComboBox60";
            this.ComboBox60.Size = new System.Drawing.Size(68, 21);
            this.ComboBox60.TabIndex = 282;
            // 
            // ComboBox59
            // 
            this.ComboBox59.FormattingEnabled = true;
            this.ComboBox59.Items.AddRange(new object[] {
            "Titular",
            "Suplente"});
            this.ComboBox59.Location = new System.Drawing.Point(167, 187);
            this.ComboBox59.Name = "ComboBox59";
            this.ComboBox59.Size = new System.Drawing.Size(68, 21);
            this.ComboBox59.TabIndex = 281;
            // 
            // ComboBox58
            // 
            this.ComboBox58.FormattingEnabled = true;
            this.ComboBox58.Items.AddRange(new object[] {
            "Titular",
            "Suplente"});
            this.ComboBox58.Location = new System.Drawing.Point(167, 150);
            this.ComboBox58.Name = "ComboBox58";
            this.ComboBox58.Size = new System.Drawing.Size(68, 21);
            this.ComboBox58.TabIndex = 284;
            // 
            // ComboBox73
            // 
            this.ComboBox73.FormattingEnabled = true;
            this.ComboBox73.Location = new System.Drawing.Point(241, 365);
            this.ComboBox73.Name = "ComboBox73";
            this.ComboBox73.Size = new System.Drawing.Size(97, 21);
            this.ComboBox73.TabIndex = 283;
            // 
            // ComboBox72
            // 
            this.ComboBox72.FormattingEnabled = true;
            this.ComboBox72.Location = new System.Drawing.Point(241, 331);
            this.ComboBox72.Name = "ComboBox72";
            this.ComboBox72.Size = new System.Drawing.Size(97, 21);
            this.ComboBox72.TabIndex = 289;
            // 
            // ComboBox71
            // 
            this.ComboBox71.FormattingEnabled = true;
            this.ComboBox71.Location = new System.Drawing.Point(241, 293);
            this.ComboBox71.Name = "ComboBox71";
            this.ComboBox71.Size = new System.Drawing.Size(97, 21);
            this.ComboBox71.TabIndex = 295;
            // 
            // ComboBox70
            // 
            this.ComboBox70.FormattingEnabled = true;
            this.ComboBox70.Location = new System.Drawing.Point(241, 257);
            this.ComboBox70.Name = "ComboBox70";
            this.ComboBox70.Size = new System.Drawing.Size(97, 21);
            this.ComboBox70.TabIndex = 294;
            // 
            // ComboBox69
            // 
            this.ComboBox69.FormattingEnabled = true;
            this.ComboBox69.Location = new System.Drawing.Point(241, 224);
            this.ComboBox69.Name = "ComboBox69";
            this.ComboBox69.Size = new System.Drawing.Size(97, 21);
            this.ComboBox69.TabIndex = 297;
            // 
            // ComboBox68
            // 
            this.ComboBox68.FormattingEnabled = true;
            this.ComboBox68.Location = new System.Drawing.Point(241, 187);
            this.ComboBox68.Name = "ComboBox68";
            this.ComboBox68.Size = new System.Drawing.Size(97, 21);
            this.ComboBox68.TabIndex = 296;
            // 
            // ComboBox67
            // 
            this.ComboBox67.FormattingEnabled = true;
            this.ComboBox67.Location = new System.Drawing.Point(241, 150);
            this.ComboBox67.Name = "ComboBox67";
            this.ComboBox67.Size = new System.Drawing.Size(97, 21);
            this.ComboBox67.TabIndex = 291;
            // 
            // ComboBox66
            // 
            this.ComboBox66.FormattingEnabled = true;
            this.ComboBox66.Location = new System.Drawing.Point(241, 113);
            this.ComboBox66.Name = "ComboBox66";
            this.ComboBox66.Size = new System.Drawing.Size(97, 21);
            this.ComboBox66.TabIndex = 290;
            // 
            // ComboBox57
            // 
            this.ComboBox57.FormattingEnabled = true;
            this.ComboBox57.Items.AddRange(new object[] {
            "Titular",
            "Suplente"});
            this.ComboBox57.Location = new System.Drawing.Point(167, 113);
            this.ComboBox57.Name = "ComboBox57";
            this.ComboBox57.Size = new System.Drawing.Size(68, 21);
            this.ComboBox57.TabIndex = 293;
            // 
            // AsigCB1
            // 
            this.AsigCB1.FormattingEnabled = true;
            this.AsigCB1.Location = new System.Drawing.Point(353, 113);
            this.AsigCB1.Name = "AsigCB1";
            this.AsigCB1.Size = new System.Drawing.Size(121, 21);
            this.AsigCB1.TabIndex = 292;
            // 
            // Label23
            // 
            this.Label23.AutoSize = true;
            this.Label23.Location = new System.Drawing.Point(288, 35);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(62, 13);
            this.Label23.TabIndex = 279;
            this.Label23.Text = "DIPREGEP";
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(523, 35);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(26, 13);
            this.Label10.TabIndex = 280;
            this.Label10.Text = "DNI";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(689, 422);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(56, 13);
            this.Label9.TabIndex = 278;
            this.Label9.Text = "TOTALES";
            // 
            // totalTXT
            // 
            this.totalTXT.Location = new System.Drawing.Point(841, 422);
            this.totalTXT.Name = "totalTXT";
            this.totalTXT.Size = new System.Drawing.Size(56, 20);
            this.totalTXT.TabIndex = 277;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(539, 76);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(44, 13);
            this.Label8.TabIndex = 276;
            this.Label8.Text = "Division";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(483, 76);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(39, 13);
            this.Label7.TabIndex = 275;
            this.Label7.Text = "Cursos";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(838, 76);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(60, 13);
            this.Label6.TabIndex = 271;
            this.Label6.Text = "Cant Horas";
            // 
            // Label24
            // 
            this.Label24.AutoSize = true;
            this.Label24.Location = new System.Drawing.Point(249, 76);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(70, 13);
            this.Label24.TabIndex = 273;
            this.Label24.Text = "Suplente de..";
            // 
            // Label22
            // 
            this.Label22.AutoSize = true;
            this.Label22.Location = new System.Drawing.Point(175, 76);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(51, 13);
            this.Label22.TabIndex = 274;
            this.Label22.Text = "Situación";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(380, 76);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(62, 13);
            this.Label5.TabIndex = 272;
            this.Label5.Text = "Asignaturas";
            // 
            // TextBox3
            // 
            this.TextBox3.Location = new System.Drawing.Point(356, 32);
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Size = new System.Drawing.Size(42, 20);
            this.TextBox3.TabIndex = 269;
            this.TextBox3.Text = "4491";
            // 
            // DNIsbTXT
            // 
            this.DNIsbTXT.Location = new System.Drawing.Point(555, 32);
            this.DNIsbTXT.Name = "DNIsbTXT";
            this.DNIsbTXT.Size = new System.Drawing.Size(100, 20);
            this.DNIsbTXT.TabIndex = 270;
            // 
            // Label21
            // 
            this.Label21.AutoSize = true;
            this.Label21.Location = new System.Drawing.Point(148, 372);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(13, 13);
            this.Label21.TabIndex = 265;
            this.Label21.Text = "8";
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Location = new System.Drawing.Point(148, 336);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(13, 13);
            this.Label20.TabIndex = 264;
            this.Label20.Text = "7";
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Location = new System.Drawing.Point(148, 301);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(13, 13);
            this.Label19.TabIndex = 266;
            this.Label19.Text = "6";
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.Location = new System.Drawing.Point(148, 264);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(13, 13);
            this.Label18.TabIndex = 268;
            this.Label18.Text = "5";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(148, 231);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(13, 13);
            this.Label4.TabIndex = 267;
            this.Label4.Text = "4";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(148, 193);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(13, 13);
            this.Label3.TabIndex = 263;
            this.Label3.Text = "3";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(148, 157);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(13, 13);
            this.Label2.TabIndex = 262;
            this.Label2.Text = "2";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(148, 113);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(13, 13);
            this.Label1.TabIndex = 261;
            this.Label1.Text = "1";
            // 
            // Secundaria_tecnica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(1263, 535);
            this.Controls.Add(this.Button2);
            this.Controls.Add(this.Button1);
            this.Controls.Add(this.ComboBox21);
            this.Controls.Add(this.ComboBox22);
            this.Controls.Add(this.ComboBox23);
            this.Controls.Add(this.ComboBox24);
            this.Controls.Add(this.ComboBox25);
            this.Controls.Add(this.ComboBox26);
            this.Controls.Add(this.ComboBox27);
            this.Controls.Add(this.ComboBox28);
            this.Controls.Add(this.ComboBox29);
            this.Controls.Add(this.ComboBox30);
            this.Controls.Add(this.ComboBox31);
            this.Controls.Add(this.ComboBox32);
            this.Controls.Add(this.ComboBox33);
            this.Controls.Add(this.ComboBox34);
            this.Controls.Add(this.ComboBox35);
            this.Controls.Add(this.ComboBox36);
            this.Controls.Add(this.ComboBox37);
            this.Controls.Add(this.ComboBox38);
            this.Controls.Add(this.ComboBox39);
            this.Controls.Add(this.ComboBox40);
            this.Controls.Add(this.ComboBox41);
            this.Controls.Add(this.ComboBox42);
            this.Controls.Add(this.ComboBox43);
            this.Controls.Add(this.ComboBox44);
            this.Controls.Add(this.ComboBox45);
            this.Controls.Add(this.ComboBox46);
            this.Controls.Add(this.ComboBox47);
            this.Controls.Add(this.ComboBox48);
            this.Controls.Add(this.ComboBox49);
            this.Controls.Add(this.ComboBox50);
            this.Controls.Add(this.ComboBox51);
            this.Controls.Add(this.ComboBox52);
            this.Controls.Add(this.ComboBox53);
            this.Controls.Add(this.ComboBox54);
            this.Controls.Add(this.ComboBox55);
            this.Controls.Add(this.ComboBox56);
            this.Controls.Add(this.ComboBox17);
            this.Controls.Add(this.ComboBox18);
            this.Controls.Add(this.ComboBox19);
            this.Controls.Add(this.ComboBox20);
            this.Controls.Add(this.Label17);
            this.Controls.Add(this.ComboBox13);
            this.Controls.Add(this.ComboBox14);
            this.Controls.Add(this.ComboBox15);
            this.Controls.Add(this.ComboBox16);
            this.Controls.Add(this.Label16);
            this.Controls.Add(this.Label15);
            this.Controls.Add(this.Label14);
            this.Controls.Add(this.TextBox2);
            this.Controls.Add(this.TextBox1);
            this.Controls.Add(this.ComboBox5);
            this.Controls.Add(this.ComboBox6);
            this.Controls.Add(this.ComboBox7);
            this.Controls.Add(this.ComboBox8);
            this.Controls.Add(this.ComboBox1);
            this.Controls.Add(this.ComboBox2);
            this.Controls.Add(this.ComboBox3);
            this.Controls.Add(this.ComboBox4);
            this.Controls.Add(this.Label12);
            this.Controls.Add(this.Label13);
            this.Controls.Add(this.Label11);
            this.Controls.Add(this.ComboBox12);
            this.Controls.Add(this.ComboBox11);
            this.Controls.Add(this.ComboBox10);
            this.Controls.Add(this.ComboBox9);
            this.Controls.Add(this.HrsCB4);
            this.Controls.Add(this.HrsCB3);
            this.Controls.Add(this.HrsCB2);
            this.Controls.Add(this.HrsCB1);
            this.Controls.Add(this.DivisionCB4);
            this.Controls.Add(this.DivisionCB3);
            this.Controls.Add(this.DivisionCB2);
            this.Controls.Add(this.DivisionCB1);
            this.Controls.Add(this.Curso4CB);
            this.Controls.Add(this.Curso3CB);
            this.Controls.Add(this.Curso2CB);
            this.Controls.Add(this.Curso1CB);
            this.Controls.Add(this.AsigCB4);
            this.Controls.Add(this.AsigCB3);
            this.Controls.Add(this.AsigCB2);
            this.Controls.Add(this.ComboBox64);
            this.Controls.Add(this.ComboBox63);
            this.Controls.Add(this.ComboBox62);
            this.Controls.Add(this.ComboBox61);
            this.Controls.Add(this.ComboBox60);
            this.Controls.Add(this.ComboBox59);
            this.Controls.Add(this.ComboBox58);
            this.Controls.Add(this.ComboBox73);
            this.Controls.Add(this.ComboBox72);
            this.Controls.Add(this.ComboBox71);
            this.Controls.Add(this.ComboBox70);
            this.Controls.Add(this.ComboBox69);
            this.Controls.Add(this.ComboBox68);
            this.Controls.Add(this.ComboBox67);
            this.Controls.Add(this.ComboBox66);
            this.Controls.Add(this.ComboBox57);
            this.Controls.Add(this.AsigCB1);
            this.Controls.Add(this.Label23);
            this.Controls.Add(this.Label10);
            this.Controls.Add(this.Label9);
            this.Controls.Add(this.totalTXT);
            this.Controls.Add(this.Label8);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.Label24);
            this.Controls.Add(this.Label22);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.TextBox3);
            this.Controls.Add(this.DNIsbTXT);
            this.Controls.Add(this.Label21);
            this.Controls.Add(this.Label20);
            this.Controls.Add(this.Label19);
            this.Controls.Add(this.Label18);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Name = "Secundaria_tecnica";
            this.Text = "Secundaria_tecnica";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.Button Button1;
        internal System.Windows.Forms.ComboBox ComboBox21;
        internal System.Windows.Forms.ComboBox ComboBox22;
        internal System.Windows.Forms.ComboBox ComboBox23;
        internal System.Windows.Forms.ComboBox ComboBox24;
        internal System.Windows.Forms.ComboBox ComboBox25;
        internal System.Windows.Forms.ComboBox ComboBox26;
        internal System.Windows.Forms.ComboBox ComboBox27;
        internal System.Windows.Forms.ComboBox ComboBox28;
        internal System.Windows.Forms.ComboBox ComboBox29;
        internal System.Windows.Forms.ComboBox ComboBox30;
        internal System.Windows.Forms.ComboBox ComboBox31;
        internal System.Windows.Forms.ComboBox ComboBox32;
        internal System.Windows.Forms.ComboBox ComboBox33;
        internal System.Windows.Forms.ComboBox ComboBox34;
        internal System.Windows.Forms.ComboBox ComboBox35;
        internal System.Windows.Forms.ComboBox ComboBox36;
        internal System.Windows.Forms.ComboBox ComboBox37;
        internal System.Windows.Forms.ComboBox ComboBox38;
        internal System.Windows.Forms.ComboBox ComboBox39;
        internal System.Windows.Forms.ComboBox ComboBox40;
        internal System.Windows.Forms.ComboBox ComboBox41;
        internal System.Windows.Forms.ComboBox ComboBox42;
        internal System.Windows.Forms.ComboBox ComboBox43;
        internal System.Windows.Forms.ComboBox ComboBox44;
        internal System.Windows.Forms.ComboBox ComboBox45;
        internal System.Windows.Forms.ComboBox ComboBox46;
        internal System.Windows.Forms.ComboBox ComboBox47;
        internal System.Windows.Forms.ComboBox ComboBox48;
        internal System.Windows.Forms.ComboBox ComboBox49;
        internal System.Windows.Forms.ComboBox ComboBox50;
        internal System.Windows.Forms.ComboBox ComboBox51;
        internal System.Windows.Forms.ComboBox ComboBox52;
        internal System.Windows.Forms.ComboBox ComboBox53;
        internal System.Windows.Forms.ComboBox ComboBox54;
        internal System.Windows.Forms.ComboBox ComboBox55;
        internal System.Windows.Forms.ComboBox ComboBox56;
        internal System.Windows.Forms.ComboBox ComboBox17;
        internal System.Windows.Forms.ComboBox ComboBox18;
        internal System.Windows.Forms.ComboBox ComboBox19;
        internal System.Windows.Forms.ComboBox ComboBox20;
        internal System.Windows.Forms.Label Label17;
        internal System.Windows.Forms.ComboBox ComboBox13;
        internal System.Windows.Forms.ComboBox ComboBox14;
        internal System.Windows.Forms.ComboBox ComboBox15;
        internal System.Windows.Forms.ComboBox ComboBox16;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.TextBox TextBox2;
        internal System.Windows.Forms.TextBox TextBox1;
        internal System.Windows.Forms.ComboBox ComboBox5;
        internal System.Windows.Forms.ComboBox ComboBox6;
        internal System.Windows.Forms.ComboBox ComboBox7;
        internal System.Windows.Forms.ComboBox ComboBox8;
        internal System.Windows.Forms.ComboBox ComboBox1;
        internal System.Windows.Forms.ComboBox ComboBox2;
        internal System.Windows.Forms.ComboBox ComboBox3;
        internal System.Windows.Forms.ComboBox ComboBox4;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.ComboBox ComboBox12;
        internal System.Windows.Forms.ComboBox ComboBox11;
        internal System.Windows.Forms.ComboBox ComboBox10;
        internal System.Windows.Forms.ComboBox ComboBox9;
        internal System.Windows.Forms.ComboBox HrsCB4;
        internal System.Windows.Forms.ComboBox HrsCB3;
        internal System.Windows.Forms.ComboBox HrsCB2;
        internal System.Windows.Forms.ComboBox HrsCB1;
        internal System.Windows.Forms.ComboBox DivisionCB4;
        internal System.Windows.Forms.ComboBox DivisionCB3;
        internal System.Windows.Forms.ComboBox DivisionCB2;
        internal System.Windows.Forms.ComboBox DivisionCB1;
        internal System.Windows.Forms.ComboBox Curso4CB;
        internal System.Windows.Forms.ComboBox Curso3CB;
        internal System.Windows.Forms.ComboBox Curso2CB;
        internal System.Windows.Forms.ComboBox Curso1CB;
        internal System.Windows.Forms.ComboBox AsigCB4;
        internal System.Windows.Forms.ComboBox AsigCB3;
        internal System.Windows.Forms.ComboBox AsigCB2;
        internal System.Windows.Forms.ComboBox ComboBox64;
        internal System.Windows.Forms.ComboBox ComboBox63;
        internal System.Windows.Forms.ComboBox ComboBox62;
        internal System.Windows.Forms.ComboBox ComboBox61;
        internal System.Windows.Forms.ComboBox ComboBox60;
        internal System.Windows.Forms.ComboBox ComboBox59;
        internal System.Windows.Forms.ComboBox ComboBox58;
        internal System.Windows.Forms.ComboBox ComboBox73;
        internal System.Windows.Forms.ComboBox ComboBox72;
        internal System.Windows.Forms.ComboBox ComboBox71;
        internal System.Windows.Forms.ComboBox ComboBox70;
        internal System.Windows.Forms.ComboBox ComboBox69;
        internal System.Windows.Forms.ComboBox ComboBox68;
        internal System.Windows.Forms.ComboBox ComboBox67;
        internal System.Windows.Forms.ComboBox ComboBox66;
        internal System.Windows.Forms.ComboBox ComboBox57;
        internal System.Windows.Forms.ComboBox AsigCB1;
        internal System.Windows.Forms.Label Label23;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.TextBox totalTXT;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label24;
        internal System.Windows.Forms.Label Label22;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.TextBox TextBox3;
        internal System.Windows.Forms.TextBox DNIsbTXT;
        internal System.Windows.Forms.Label Label21;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.Label Label18;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
    }
}