﻿namespace Presentismo_2015
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows myCustomForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ModPresTSL1 = new System.Windows.Forms.ToolStripButton();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ToolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.ToolStripButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.ProfesorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cursosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignaturasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.horarioDeClaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripButton3 = new System.Windows.Forms.ToolStripDropDownButton();
            this.ProfesorToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.PlanillaDeFirmasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PorFechaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox7 = new System.Windows.Forms.ToolStripTextBox();
            this.FechaHastaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox8 = new System.Windows.Forms.ToolStripTextBox();
            this.GenerarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ReporteDeBajasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PorPersonaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.GlobalToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.PlanillaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox3 = new System.Windows.Forms.ToolStripTextBox();
            this.GlobalToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox4 = new System.Windows.Forms.ToolStripTextBox();
            this.GenerarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PlanillaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.PorPersonaToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox6 = new System.Windows.Forms.ToolStripTextBox();
            this.GlobalToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox5 = new System.Windows.Forms.ToolStripTextBox();
            this.GenerarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.PlanillaDePorcentajeDeInasistenciaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FechaDesdeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.FechaHastaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.GenerarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.endSession = new System.Windows.Forms.LinkLabel();
            this.ToolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ModPresTSL1
            // 
            this.ModPresTSL1.Enabled = false;
            this.ModPresTSL1.Image = global::Presentismo_2015.Properties.Resources.vcard;
            this.ModPresTSL1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ModPresTSL1.Name = "ModPresTSL1";
            this.ModPresTSL1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ModPresTSL1.Size = new System.Drawing.Size(181, 36);
            this.ModPresTSL1.Text = "&Modificación Presentismo";
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.BackColor = System.Drawing.Color.Honeydew;
            this.ToolStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripLabel2,
            this.ToolStripButton1,
            this.ToolStripSeparator1,
            this.ToolStripButton3,
            this.ToolStripSeparator2,
            this.ToolStripSplitButton1,
            this.ToolStripSeparator3,
            this.ModPresTSL1});
            this.ToolStrip1.Location = new System.Drawing.Point(0, 0);
            this.ToolStrip1.Name = "ToolStrip1";
            this.ToolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ToolStrip1.Size = new System.Drawing.Size(1236, 39);
            this.ToolStrip1.TabIndex = 4;
            this.ToolStrip1.Text = "ToolStrip1";
            this.ToolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.ToolStrip1_ItemClicked);
            // 
            // ToolStripLabel2
            // 
            this.ToolStripLabel2.Name = "ToolStripLabel2";
            this.ToolStripLabel2.Size = new System.Drawing.Size(0, 36);
            // 
            // ToolStripButton1
            // 
            this.ToolStripButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProfesorToolStripMenuItem,
            this.cursosToolStripMenuItem,
            this.asignaturasToolStripMenuItem,
            this.horarioDeClaseToolStripMenuItem});
            this.ToolStripButton1.Image = global::Presentismo_2015.Properties.Resources.add;
            this.ToolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButton1.Name = "ToolStripButton1";
            this.ToolStripButton1.Size = new System.Drawing.Size(73, 36);
            this.ToolStripButton1.Text = "&Alta";
            this.ToolStripButton1.Click += new System.EventHandler(this.ToolStripButton1_Click);
            // 
            // ProfesorToolStripMenuItem
            // 
            this.ProfesorToolStripMenuItem.Name = "ProfesorToolStripMenuItem";
            this.ProfesorToolStripMenuItem.ShowShortcutKeys = false;
            this.ProfesorToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.ProfesorToolStripMenuItem.Text = "&Personal";
            this.ProfesorToolStripMenuItem.ToolTipText = "Alta de Personal";
            this.ProfesorToolStripMenuItem.Click += new System.EventHandler(this.ProfesorToolStripMenuItem_Click);
            // 
            // cursosToolStripMenuItem
            // 
            this.cursosToolStripMenuItem.Name = "cursosToolStripMenuItem";
            this.cursosToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.cursosToolStripMenuItem.Text = "Cursos";
            this.cursosToolStripMenuItem.Click += new System.EventHandler(this.cursosToolStripMenuItem_Click);
            // 
            // asignaturasToolStripMenuItem
            // 
            this.asignaturasToolStripMenuItem.Name = "asignaturasToolStripMenuItem";
            this.asignaturasToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.asignaturasToolStripMenuItem.Text = "Asignaturas";
            this.asignaturasToolStripMenuItem.Click += new System.EventHandler(this.asignaturasToolStripMenuItem_Click);
            // 
            // horarioDeClaseToolStripMenuItem
            // 
            this.horarioDeClaseToolStripMenuItem.Name = "horarioDeClaseToolStripMenuItem";
            this.horarioDeClaseToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.horarioDeClaseToolStripMenuItem.Text = "Horario de clase";
            this.horarioDeClaseToolStripMenuItem.Click += new System.EventHandler(this.horarioDeClaseToolStripMenuItem_Click);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // ToolStripButton3
            // 
            this.ToolStripButton3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProfesorToolStripMenuItem2});
            this.ToolStripButton3.Image = global::Presentismo_2015.Properties.Resources.search_plus;
            this.ToolStripButton3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButton3.Name = "ToolStripButton3";
            this.ToolStripButton3.Size = new System.Drawing.Size(87, 36);
            this.ToolStripButton3.Text = "&Buscar";
            // 
            // ProfesorToolStripMenuItem2
            // 
            this.ProfesorToolStripMenuItem2.Image = global::Presentismo_2015.Properties.Resources.user;
            this.ProfesorToolStripMenuItem2.Name = "ProfesorToolStripMenuItem2";
            this.ProfesorToolStripMenuItem2.Size = new System.Drawing.Size(119, 22);
            this.ProfesorToolStripMenuItem2.Text = "&Personal";
            this.ProfesorToolStripMenuItem2.ToolTipText = "Buscar Personal";
            this.ProfesorToolStripMenuItem2.Click += new System.EventHandler(this.ProfesorToolStripMenuItem2_Click);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // ToolStripSplitButton1
            // 
            this.ToolStripSplitButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PlanillaDeFirmasToolStripMenuItem,
            this.ReporteDeBajasToolStripMenuItem,
            this.PlanillaToolStripMenuItem,
            this.PlanillaToolStripMenuItem1,
            this.PlanillaDePorcentajeDeInasistenciaToolStripMenuItem});
            this.ToolStripSplitButton1.Image = global::Presentismo_2015.Properties.Resources.file_extension_xls;
            this.ToolStripSplitButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ToolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripSplitButton1.Name = "ToolStripSplitButton1";
            this.ToolStripSplitButton1.Size = new System.Drawing.Size(137, 36);
            this.ToolStripSplitButton1.Text = "&Generar Planilla";
            this.ToolStripSplitButton1.ToolTipText = "Generar Reportes";
            // 
            // PlanillaDeFirmasToolStripMenuItem
            // 
            this.PlanillaDeFirmasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PorFechaToolStripMenuItem,
            this.ToolStripTextBox7,
            this.FechaHastaToolStripMenuItem,
            this.ToolStripTextBox8,
            this.GenerarToolStripMenuItem2});
            this.PlanillaDeFirmasToolStripMenuItem.Image = global::Presentismo_2015.Properties.Resources.bullet_orange;
            this.PlanillaDeFirmasToolStripMenuItem.Name = "PlanillaDeFirmasToolStripMenuItem";
            this.PlanillaDeFirmasToolStripMenuItem.Size = new System.Drawing.Size(269, 22);
            this.PlanillaDeFirmasToolStripMenuItem.Text = "&Planilla De Firmas";
            // 
            // PorFechaToolStripMenuItem
            // 
            this.PorFechaToolStripMenuItem.Image = global::Presentismo_2015.Properties.Resources.calendar;
            this.PorFechaToolStripMenuItem.Name = "PorFechaToolStripMenuItem";
            this.PorFechaToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.PorFechaToolStripMenuItem.Text = "Fecha Desde";
            // 
            // ToolStripTextBox7
            // 
            this.ToolStripTextBox7.Name = "ToolStripTextBox7";
            this.ToolStripTextBox7.Size = new System.Drawing.Size(100, 23);
            this.ToolStripTextBox7.Text = "   /   /";
            // 
            // FechaHastaToolStripMenuItem
            // 
            this.FechaHastaToolStripMenuItem.Image = global::Presentismo_2015.Properties.Resources.calendar;
            this.FechaHastaToolStripMenuItem.Name = "FechaHastaToolStripMenuItem";
            this.FechaHastaToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.FechaHastaToolStripMenuItem.Text = "Fecha Hasta";
            // 
            // ToolStripTextBox8
            // 
            this.ToolStripTextBox8.Name = "ToolStripTextBox8";
            this.ToolStripTextBox8.Size = new System.Drawing.Size(100, 23);
            this.ToolStripTextBox8.Text = "   /   /";
            // 
            // GenerarToolStripMenuItem2
            // 
            this.GenerarToolStripMenuItem2.Image = global::Presentismo_2015.Properties.Resources.accept;
            this.GenerarToolStripMenuItem2.Name = "GenerarToolStripMenuItem2";
            this.GenerarToolStripMenuItem2.Size = new System.Drawing.Size(160, 22);
            this.GenerarToolStripMenuItem2.Text = "Generar";
            // 
            // ReporteDeBajasToolStripMenuItem
            // 
            this.ReporteDeBajasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PorPersonaToolStripMenuItem1,
            this.GlobalToolStripMenuItem1});
            this.ReporteDeBajasToolStripMenuItem.Image = global::Presentismo_2015.Properties.Resources.bullet_blue;
            this.ReporteDeBajasToolStripMenuItem.Name = "ReporteDeBajasToolStripMenuItem";
            this.ReporteDeBajasToolStripMenuItem.Size = new System.Drawing.Size(269, 22);
            this.ReporteDeBajasToolStripMenuItem.Text = "&Planilla Presentismo";
            // 
            // PorPersonaToolStripMenuItem1
            // 
            this.PorPersonaToolStripMenuItem1.Image = global::Presentismo_2015.Properties.Resources.user;
            this.PorPersonaToolStripMenuItem1.Name = "PorPersonaToolStripMenuItem1";
            this.PorPersonaToolStripMenuItem1.Size = new System.Drawing.Size(126, 22);
            this.PorPersonaToolStripMenuItem1.Text = "Individual";
            // 
            // GlobalToolStripMenuItem1
            // 
            this.GlobalToolStripMenuItem1.Image = global::Presentismo_2015.Properties.Resources.group;
            this.GlobalToolStripMenuItem1.Name = "GlobalToolStripMenuItem1";
            this.GlobalToolStripMenuItem1.Size = new System.Drawing.Size(126, 22);
            this.GlobalToolStripMenuItem1.Text = "General";
            // 
            // PlanillaToolStripMenuItem
            // 
            this.PlanillaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem3,
            this.ToolStripTextBox3,
            this.GlobalToolStripMenuItem2,
            this.ToolStripTextBox4,
            this.GenerarToolStripMenuItem});
            this.PlanillaToolStripMenuItem.Image = global::Presentismo_2015.Properties.Resources.bullet_red;
            this.PlanillaToolStripMenuItem.Name = "PlanillaToolStripMenuItem";
            this.PlanillaToolStripMenuItem.Size = new System.Drawing.Size(269, 22);
            this.PlanillaToolStripMenuItem.Text = "&Planilla Inasistencia";
            // 
            // ToolStripMenuItem3
            // 
            this.ToolStripMenuItem3.Image = global::Presentismo_2015.Properties.Resources.calendar;
            this.ToolStripMenuItem3.Name = "ToolStripMenuItem3";
            this.ToolStripMenuItem3.Size = new System.Drawing.Size(160, 22);
            this.ToolStripMenuItem3.Text = "Fecha Desde";
            // 
            // ToolStripTextBox3
            // 
            this.ToolStripTextBox3.Name = "ToolStripTextBox3";
            this.ToolStripTextBox3.Size = new System.Drawing.Size(100, 23);
            this.ToolStripTextBox3.Text = "1";
            // 
            // GlobalToolStripMenuItem2
            // 
            this.GlobalToolStripMenuItem2.Image = global::Presentismo_2015.Properties.Resources.calendar;
            this.GlobalToolStripMenuItem2.Name = "GlobalToolStripMenuItem2";
            this.GlobalToolStripMenuItem2.Size = new System.Drawing.Size(160, 22);
            this.GlobalToolStripMenuItem2.Text = "Fecha Hasta";
            // 
            // ToolStripTextBox4
            // 
            this.ToolStripTextBox4.Name = "ToolStripTextBox4";
            this.ToolStripTextBox4.Size = new System.Drawing.Size(100, 23);
            this.ToolStripTextBox4.Text = "   /   /";
            // 
            // GenerarToolStripMenuItem
            // 
            this.GenerarToolStripMenuItem.Image = global::Presentismo_2015.Properties.Resources.accept;
            this.GenerarToolStripMenuItem.Name = "GenerarToolStripMenuItem";
            this.GenerarToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.GenerarToolStripMenuItem.Text = "Generar";
            // 
            // PlanillaToolStripMenuItem1
            // 
            this.PlanillaToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PorPersonaToolStripMenuItem3,
            this.ToolStripTextBox6,
            this.GlobalToolStripMenuItem3,
            this.ToolStripTextBox5,
            this.GenerarToolStripMenuItem1});
            this.PlanillaToolStripMenuItem1.Image = global::Presentismo_2015.Properties.Resources.bullet_green;
            this.PlanillaToolStripMenuItem1.Name = "PlanillaToolStripMenuItem1";
            this.PlanillaToolStripMenuItem1.Size = new System.Drawing.Size(269, 22);
            this.PlanillaToolStripMenuItem1.Text = "&Planilla Por Enfermedad";
            // 
            // PorPersonaToolStripMenuItem3
            // 
            this.PorPersonaToolStripMenuItem3.Image = global::Presentismo_2015.Properties.Resources.calendar;
            this.PorPersonaToolStripMenuItem3.Name = "PorPersonaToolStripMenuItem3";
            this.PorPersonaToolStripMenuItem3.Size = new System.Drawing.Size(160, 22);
            this.PorPersonaToolStripMenuItem3.Text = "Fecha Desde";
            // 
            // ToolStripTextBox6
            // 
            this.ToolStripTextBox6.Name = "ToolStripTextBox6";
            this.ToolStripTextBox6.Size = new System.Drawing.Size(100, 23);
            this.ToolStripTextBox6.Text = "   /   /";
            // 
            // GlobalToolStripMenuItem3
            // 
            this.GlobalToolStripMenuItem3.Image = global::Presentismo_2015.Properties.Resources.calendar;
            this.GlobalToolStripMenuItem3.Name = "GlobalToolStripMenuItem3";
            this.GlobalToolStripMenuItem3.Size = new System.Drawing.Size(160, 22);
            this.GlobalToolStripMenuItem3.Text = "Fecha Hasta";
            // 
            // ToolStripTextBox5
            // 
            this.ToolStripTextBox5.Name = "ToolStripTextBox5";
            this.ToolStripTextBox5.Size = new System.Drawing.Size(100, 23);
            this.ToolStripTextBox5.Text = "   /   /";
            // 
            // GenerarToolStripMenuItem1
            // 
            this.GenerarToolStripMenuItem1.Image = global::Presentismo_2015.Properties.Resources.accept;
            this.GenerarToolStripMenuItem1.Name = "GenerarToolStripMenuItem1";
            this.GenerarToolStripMenuItem1.Size = new System.Drawing.Size(160, 22);
            this.GenerarToolStripMenuItem1.Text = "Generar";
            // 
            // PlanillaDePorcentajeDeInasistenciaToolStripMenuItem
            // 
            this.PlanillaDePorcentajeDeInasistenciaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FechaDesdeToolStripMenuItem,
            this.ToolStripTextBox2,
            this.FechaHastaToolStripMenuItem1,
            this.ToolStripTextBox1,
            this.GenerarToolStripMenuItem3});
            this.PlanillaDePorcentajeDeInasistenciaToolStripMenuItem.Image = global::Presentismo_2015.Properties.Resources.bullet_black;
            this.PlanillaDePorcentajeDeInasistenciaToolStripMenuItem.Name = "PlanillaDePorcentajeDeInasistenciaToolStripMenuItem";
            this.PlanillaDePorcentajeDeInasistenciaToolStripMenuItem.Size = new System.Drawing.Size(269, 22);
            this.PlanillaDePorcentajeDeInasistenciaToolStripMenuItem.Text = "&Planilla De Porcentaje De Inasistencia";
            // 
            // FechaDesdeToolStripMenuItem
            // 
            this.FechaDesdeToolStripMenuItem.Image = global::Presentismo_2015.Properties.Resources.calendar;
            this.FechaDesdeToolStripMenuItem.Name = "FechaDesdeToolStripMenuItem";
            this.FechaDesdeToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.FechaDesdeToolStripMenuItem.Text = "Fecha Desde";
            // 
            // ToolStripTextBox2
            // 
            this.ToolStripTextBox2.Name = "ToolStripTextBox2";
            this.ToolStripTextBox2.Size = new System.Drawing.Size(100, 23);
            this.ToolStripTextBox2.Text = "   /   /";
            // 
            // FechaHastaToolStripMenuItem1
            // 
            this.FechaHastaToolStripMenuItem1.Image = global::Presentismo_2015.Properties.Resources.calendar;
            this.FechaHastaToolStripMenuItem1.Name = "FechaHastaToolStripMenuItem1";
            this.FechaHastaToolStripMenuItem1.Size = new System.Drawing.Size(160, 22);
            this.FechaHastaToolStripMenuItem1.Text = "Fecha Hasta";
            // 
            // ToolStripTextBox1
            // 
            this.ToolStripTextBox1.Name = "ToolStripTextBox1";
            this.ToolStripTextBox1.Size = new System.Drawing.Size(100, 23);
            this.ToolStripTextBox1.Text = "   /   /";
            // 
            // GenerarToolStripMenuItem3
            // 
            this.GenerarToolStripMenuItem3.Image = global::Presentismo_2015.Properties.Resources.accept;
            this.GenerarToolStripMenuItem3.Name = "GenerarToolStripMenuItem3";
            this.GenerarToolStripMenuItem3.Size = new System.Drawing.Size(160, 22);
            this.GenerarToolStripMenuItem3.Text = "Generar";
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel1.Location = new System.Drawing.Point(0, 39);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(1236, 479);
            this.Panel1.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1073, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Admin, Admin";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(1073, 23);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(83, 13);
            this.linkLabel1.TabIndex = 7;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Editar mi cuenta";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // endSession
            // 
            this.endSession.AutoSize = true;
            this.endSession.Location = new System.Drawing.Point(1153, 23);
            this.endSession.Name = "endSession";
            this.endSession.Size = new System.Drawing.Size(68, 13);
            this.endSession.TabIndex = 8;
            this.endSession.TabStop = true;
            this.endSession.Text = "Cerrar sesión";
            this.endSession.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.endSession_LinkClicked);
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1236, 518);
            this.Controls.Add(this.endSession);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.ToolStrip1);
            this.Name = "Principal";
            this.Text = "Principal";
            this.Load += new System.EventHandler(this.Principal_Load);
            this.ToolStrip1.ResumeLayout(false);
            this.ToolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ToolStripButton ModPresTSL1;
        internal System.Windows.Forms.ToolStripMenuItem GlobalToolStripMenuItem2;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox4;
        internal System.Windows.Forms.ToolStripMenuItem GenerarToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem PlanillaToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem PorPersonaToolStripMenuItem3;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox6;
        internal System.Windows.Forms.ToolStripMenuItem GlobalToolStripMenuItem3;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox5;
        internal System.Windows.Forms.ToolStripMenuItem GenerarToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox3;
        internal System.Windows.Forms.ToolStripMenuItem PlanillaDePorcentajeDeInasistenciaToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem FechaDesdeToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox2;
        internal System.Windows.Forms.ToolStripMenuItem FechaHastaToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox1;
        internal System.Windows.Forms.ToolStripMenuItem GenerarToolStripMenuItem3;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem3;
        internal System.Windows.Forms.ToolStrip ToolStrip1;
        internal System.Windows.Forms.ToolStripLabel ToolStripLabel2;
        internal System.Windows.Forms.ToolStripDropDownButton ToolStripButton1;
        internal System.Windows.Forms.ToolStripMenuItem ProfesorToolStripMenuItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripDropDownButton ToolStripButton3;
        internal System.Windows.Forms.ToolStripMenuItem ProfesorToolStripMenuItem2;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripSplitButton ToolStripSplitButton1;
        internal System.Windows.Forms.ToolStripMenuItem PlanillaDeFirmasToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem PorFechaToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox7;
        internal System.Windows.Forms.ToolStripMenuItem FechaHastaToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox8;
        internal System.Windows.Forms.ToolStripMenuItem GenerarToolStripMenuItem2;
        internal System.Windows.Forms.ToolStripMenuItem ReporteDeBajasToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem PorPersonaToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem GlobalToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem PlanillaToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem cursosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignaturasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem horarioDeClaseToolStripMenuItem;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel endSession;
    }
}