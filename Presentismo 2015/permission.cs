﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Presentismo_2015
{
    public class permission
    {
        public int permissionID { get;  set; }
        public String permissionDesc { get;  set; }
        public int isEnabled { get;  set; }

    }
}
