﻿namespace Presentismo_2015
{
    partial class Muestra_asignatura_basica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows myCustomForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AgregarBT = new System.Windows.Forms.Button();
            this.Label15 = new System.Windows.Forms.Label();
            this.Label14 = new System.Windows.Forms.Label();
            this.TextBox2 = new System.Windows.Forms.TextBox();
            this.TextBox1 = new System.Windows.Forms.TextBox();
            this.Label23 = new System.Windows.Forms.Label();
            this.Label10 = new System.Windows.Forms.Label();
            this.TextBox3 = new System.Windows.Forms.TextBox();
            this.DNIsbTXT = new System.Windows.Forms.TextBox();
            this.DataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // AgregarBT
            // 
            this.AgregarBT.BackColor = System.Drawing.Color.White;
            this.AgregarBT.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.AgregarBT.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.AgregarBT.Image = global::Presentismo_2015.Properties.Resources.accept;
            this.AgregarBT.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.AgregarBT.Location = new System.Drawing.Point(601, 445);
            this.AgregarBT.Name = "AgregarBT";
            this.AgregarBT.Size = new System.Drawing.Size(56, 54);
            this.AgregarBT.TabIndex = 232;
            this.AgregarBT.Text = "Aceptar";
            this.AgregarBT.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.AgregarBT.UseVisualStyleBackColor = false;
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Location = new System.Drawing.Point(816, 28);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(44, 13);
            this.Label15.TabIndex = 231;
            this.Label15.Text = "Apellido";
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(648, 28);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(44, 13);
            this.Label14.TabIndex = 230;
            this.Label14.Text = "Nombre";
            // 
            // TextBox2
            // 
            this.TextBox2.Location = new System.Drawing.Point(868, 25);
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Size = new System.Drawing.Size(100, 20);
            this.TextBox2.TabIndex = 229;
            // 
            // TextBox1
            // 
            this.TextBox1.Location = new System.Drawing.Point(698, 25);
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Size = new System.Drawing.Size(100, 20);
            this.TextBox1.TabIndex = 228;
            // 
            // Label23
            // 
            this.Label23.AutoSize = true;
            this.Label23.Location = new System.Drawing.Point(273, 28);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(62, 13);
            this.Label23.TabIndex = 226;
            this.Label23.Text = "DIPREGEP";
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(508, 28);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(26, 13);
            this.Label10.TabIndex = 227;
            this.Label10.Text = "DNI";
            // 
            // TextBox3
            // 
            this.TextBox3.Location = new System.Drawing.Point(341, 25);
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Size = new System.Drawing.Size(42, 20);
            this.TextBox3.TabIndex = 224;
            this.TextBox3.Text = "7275";
            // 
            // DNIsbTXT
            // 
            this.DNIsbTXT.Location = new System.Drawing.Point(540, 25);
            this.DNIsbTXT.Name = "DNIsbTXT";
            this.DNIsbTXT.Size = new System.Drawing.Size(100, 20);
            this.DNIsbTXT.TabIndex = 225;
            // 
            // DataGridView1
            // 
            this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView1.Location = new System.Drawing.Point(259, 82);
            this.DataGridView1.Name = "DataGridView1";
            this.DataGridView1.Size = new System.Drawing.Size(740, 357);
            this.DataGridView1.TabIndex = 223;
            // 
            // Muestra_asignatura_basica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(1258, 524);
            this.Controls.Add(this.AgregarBT);
            this.Controls.Add(this.Label15);
            this.Controls.Add(this.Label14);
            this.Controls.Add(this.TextBox2);
            this.Controls.Add(this.TextBox1);
            this.Controls.Add(this.Label23);
            this.Controls.Add(this.Label10);
            this.Controls.Add(this.TextBox3);
            this.Controls.Add(this.DNIsbTXT);
            this.Controls.Add(this.DataGridView1);
            this.Name = "Muestra_asignatura_basica";
            this.Text = "Muestra_asignatura_basica";
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button AgregarBT;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.TextBox TextBox2;
        internal System.Windows.Forms.TextBox TextBox1;
        internal System.Windows.Forms.Label Label23;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.TextBox TextBox3;
        internal System.Windows.Forms.TextBox DNIsbTXT;
        internal System.Windows.Forms.DataGridView DataGridView1;
    }
}