﻿namespace Presentismo_2015
{
    partial class Modificacion_de_presentismo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows myCustomForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SecuBasicaGBOX = new System.Windows.Forms.GroupBox();
            this.Button3 = new System.Windows.Forms.Button();
            this.Button2 = new System.Windows.Forms.Button();
            this.TextBox19 = new System.Windows.Forms.TextBox();
            this.TextBox26 = new System.Windows.Forms.TextBox();
            this.TextBox10 = new System.Windows.Forms.TextBox();
            this.TextBox18 = new System.Windows.Forms.TextBox();
            this.TextBox25 = new System.Windows.Forms.TextBox();
            this.TextBox9 = new System.Windows.Forms.TextBox();
            this.TextBox17 = new System.Windows.Forms.TextBox();
            this.TextBox24 = new System.Windows.Forms.TextBox();
            this.TextBox7 = new System.Windows.Forms.TextBox();
            this.TextBox16 = new System.Windows.Forms.TextBox();
            this.TextBox23 = new System.Windows.Forms.TextBox();
            this.TextBox6 = new System.Windows.Forms.TextBox();
            this.TextBox15 = new System.Windows.Forms.TextBox();
            this.TextBox22 = new System.Windows.Forms.TextBox();
            this.TextBox5 = new System.Windows.Forms.TextBox();
            this.TextBox14 = new System.Windows.Forms.TextBox();
            this.TextBox21 = new System.Windows.Forms.TextBox();
            this.TextBox4 = new System.Windows.Forms.TextBox();
            this.TextBox13 = new System.Windows.Forms.TextBox();
            this.TextBox20 = new System.Windows.Forms.TextBox();
            this.TextBox2 = new System.Windows.Forms.TextBox();
            this.TextBox12 = new System.Windows.Forms.TextBox();
            this.TextBox3 = new System.Windows.Forms.TextBox();
            this.TextBox1 = new System.Windows.Forms.TextBox();
            this.AporteCB8 = new System.Windows.Forms.ComboBox();
            this.AporteCB7 = new System.Windows.Forms.ComboBox();
            this.AporteCB6 = new System.Windows.Forms.ComboBox();
            this.AporteCB5 = new System.Windows.Forms.ComboBox();
            this.ComboBox8 = new System.Windows.Forms.ComboBox();
            this.CategCB8 = new System.Windows.Forms.ComboBox();
            this.ComboBox7 = new System.Windows.Forms.ComboBox();
            this.CategCB7 = new System.Windows.Forms.ComboBox();
            this.ComboBox6 = new System.Windows.Forms.ComboBox();
            this.CategCB6 = new System.Windows.Forms.ComboBox();
            this.ComboBox5 = new System.Windows.Forms.ComboBox();
            this.CategCB5 = new System.Windows.Forms.ComboBox();
            this.HrFinCB8 = new System.Windows.Forms.ComboBox();
            this.HrFinCB7 = new System.Windows.Forms.ComboBox();
            this.HrFinCB6 = new System.Windows.Forms.ComboBox();
            this.HrFinCB5 = new System.Windows.Forms.ComboBox();
            this.HrIniCB8 = new System.Windows.Forms.ComboBox();
            this.HrIniCB7 = new System.Windows.Forms.ComboBox();
            this.HrIniCB6 = new System.Windows.Forms.ComboBox();
            this.HrIniCB5 = new System.Windows.Forms.ComboBox();
            this.diaCB8 = new System.Windows.Forms.ComboBox();
            this.diaCB7 = new System.Windows.Forms.ComboBox();
            this.diaCB6 = new System.Windows.Forms.ComboBox();
            this.diaCB5 = new System.Windows.Forms.ComboBox();
            this.CantHrsCB8 = new System.Windows.Forms.ComboBox();
            this.CantHrsCB7 = new System.Windows.Forms.ComboBox();
            this.CantHrsCB6 = new System.Windows.Forms.ComboBox();
            this.CantHrsCB5 = new System.Windows.Forms.ComboBox();
            this.DivisionCB8 = new System.Windows.Forms.ComboBox();
            this.DivisionCB7 = new System.Windows.Forms.ComboBox();
            this.DivisionCB6 = new System.Windows.Forms.ComboBox();
            this.DivisionCB5 = new System.Windows.Forms.ComboBox();
            this.Curso8CB = new System.Windows.Forms.ComboBox();
            this.Curso7CB = new System.Windows.Forms.ComboBox();
            this.Curso6CB = new System.Windows.Forms.ComboBox();
            this.Curso5CB = new System.Windows.Forms.ComboBox();
            this.AsigCB8 = new System.Windows.Forms.ComboBox();
            this.AsigCB7 = new System.Windows.Forms.ComboBox();
            this.AsigCB6 = new System.Windows.Forms.ComboBox();
            this.AsigCB5 = new System.Windows.Forms.ComboBox();
            this.AporteCB4 = new System.Windows.Forms.ComboBox();
            this.AporteCB3 = new System.Windows.Forms.ComboBox();
            this.AporteCB2 = new System.Windows.Forms.ComboBox();
            this.AporteCB1 = new System.Windows.Forms.ComboBox();
            this.Label29 = new System.Windows.Forms.Label();
            this.Label23 = new System.Windows.Forms.Label();
            this.Label25 = new System.Windows.Forms.Label();
            this.Label17 = new System.Windows.Forms.Label();
            this.ComboBox4 = new System.Windows.Forms.ComboBox();
            this.CategCB4 = new System.Windows.Forms.ComboBox();
            this.ComboBox3 = new System.Windows.Forms.ComboBox();
            this.CategCB3 = new System.Windows.Forms.ComboBox();
            this.ComboBox2 = new System.Windows.Forms.ComboBox();
            this.CategCB2 = new System.Windows.Forms.ComboBox();
            this.ComboBox1 = new System.Windows.Forms.ComboBox();
            this.CategCB1 = new System.Windows.Forms.ComboBox();
            this.Label26 = new System.Windows.Forms.Label();
            this.Label16 = new System.Windows.Forms.Label();
            this.Label15 = new System.Windows.Forms.Label();
            this.Label14 = new System.Windows.Forms.Label();
            this.ApellidoTXT = new System.Windows.Forms.TextBox();
            this.NombreTXT = new System.Windows.Forms.TextBox();
            this.HrFinCB4 = new System.Windows.Forms.ComboBox();
            this.HrFinCB3 = new System.Windows.Forms.ComboBox();
            this.HrFinCB2 = new System.Windows.Forms.ComboBox();
            this.HrFinCB1 = new System.Windows.Forms.ComboBox();
            this.HrIniCB4 = new System.Windows.Forms.ComboBox();
            this.HrIniCB3 = new System.Windows.Forms.ComboBox();
            this.HrIniCB2 = new System.Windows.Forms.ComboBox();
            this.HrIniCB1 = new System.Windows.Forms.ComboBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.diaCB4 = new System.Windows.Forms.ComboBox();
            this.diaCB3 = new System.Windows.Forms.ComboBox();
            this.DiaCB2 = new System.Windows.Forms.ComboBox();
            this.diaCB1 = new System.Windows.Forms.ComboBox();
            this.CantHrsCB4 = new System.Windows.Forms.ComboBox();
            this.CantHrsCB3 = new System.Windows.Forms.ComboBox();
            this.CantHrsCB2 = new System.Windows.Forms.ComboBox();
            this.CantHrsCB1 = new System.Windows.Forms.ComboBox();
            this.DivisionCB4 = new System.Windows.Forms.ComboBox();
            this.DivisionCB3 = new System.Windows.Forms.ComboBox();
            this.DivisionCB2 = new System.Windows.Forms.ComboBox();
            this.DivisionCB1 = new System.Windows.Forms.ComboBox();
            this.Curso4CB = new System.Windows.Forms.ComboBox();
            this.Curso3CB = new System.Windows.Forms.ComboBox();
            this.Curso2CB = new System.Windows.Forms.ComboBox();
            this.Curso1CB = new System.Windows.Forms.ComboBox();
            this.AsigCB4 = new System.Windows.Forms.ComboBox();
            this.AsigCB3 = new System.Windows.Forms.ComboBox();
            this.AsigCB2 = new System.Windows.Forms.ComboBox();
            this.Situacion8CB = new System.Windows.Forms.ComboBox();
            this.Situacion7CB = new System.Windows.Forms.ComboBox();
            this.Situacion6CB = new System.Windows.Forms.ComboBox();
            this.Situacion5CB = new System.Windows.Forms.ComboBox();
            this.Situacion4CB = new System.Windows.Forms.ComboBox();
            this.Situacion3CB = new System.Windows.Forms.ComboBox();
            this.Situacion2CB = new System.Windows.Forms.ComboBox();
            this.Suplente8CB = new System.Windows.Forms.ComboBox();
            this.Suplente7CB = new System.Windows.Forms.ComboBox();
            this.Suplente6CB = new System.Windows.Forms.ComboBox();
            this.Suplente5CB = new System.Windows.Forms.ComboBox();
            this.Suplente4CB = new System.Windows.Forms.ComboBox();
            this.Suplente3CB = new System.Windows.Forms.ComboBox();
            this.Suplente2CB = new System.Windows.Forms.ComboBox();
            this.Suplente1CB = new System.Windows.Forms.ComboBox();
            this.Situacion1CB = new System.Windows.Forms.ComboBox();
            this.AsigCB1 = new System.Windows.Forms.ComboBox();
            this.Label10 = new System.Windows.Forms.Label();
            this.Label28 = new System.Windows.Forms.Label();
            this.Label27 = new System.Windows.Forms.Label();
            this.TextBox11 = new System.Windows.Forms.TextBox();
            this.TextBox8 = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.totalTXT = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label24 = new System.Windows.Forms.Label();
            this.Label22 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.DNIsbTXT = new System.Windows.Forms.TextBox();
            this.Label21 = new System.Windows.Forms.Label();
            this.Label20 = new System.Windows.Forms.Label();
            this.Label19 = new System.Windows.Forms.Label();
            this.Label18 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.SecuBasicaGBOX.SuspendLayout();
            this.SuspendLayout();
            // 
            // SecuBasicaGBOX
            // 
            this.SecuBasicaGBOX.BackColor = System.Drawing.Color.Honeydew;
            this.SecuBasicaGBOX.Controls.Add(this.Button3);
            this.SecuBasicaGBOX.Controls.Add(this.Button2);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox19);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox26);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox10);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox18);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox25);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox9);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox17);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox24);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox7);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox16);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox23);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox6);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox15);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox22);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox5);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox14);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox21);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox4);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox13);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox20);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox2);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox12);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox3);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox1);
            this.SecuBasicaGBOX.Controls.Add(this.AporteCB8);
            this.SecuBasicaGBOX.Controls.Add(this.AporteCB7);
            this.SecuBasicaGBOX.Controls.Add(this.AporteCB6);
            this.SecuBasicaGBOX.Controls.Add(this.AporteCB5);
            this.SecuBasicaGBOX.Controls.Add(this.ComboBox8);
            this.SecuBasicaGBOX.Controls.Add(this.CategCB8);
            this.SecuBasicaGBOX.Controls.Add(this.ComboBox7);
            this.SecuBasicaGBOX.Controls.Add(this.CategCB7);
            this.SecuBasicaGBOX.Controls.Add(this.ComboBox6);
            this.SecuBasicaGBOX.Controls.Add(this.CategCB6);
            this.SecuBasicaGBOX.Controls.Add(this.ComboBox5);
            this.SecuBasicaGBOX.Controls.Add(this.CategCB5);
            this.SecuBasicaGBOX.Controls.Add(this.HrFinCB8);
            this.SecuBasicaGBOX.Controls.Add(this.HrFinCB7);
            this.SecuBasicaGBOX.Controls.Add(this.HrFinCB6);
            this.SecuBasicaGBOX.Controls.Add(this.HrFinCB5);
            this.SecuBasicaGBOX.Controls.Add(this.HrIniCB8);
            this.SecuBasicaGBOX.Controls.Add(this.HrIniCB7);
            this.SecuBasicaGBOX.Controls.Add(this.HrIniCB6);
            this.SecuBasicaGBOX.Controls.Add(this.HrIniCB5);
            this.SecuBasicaGBOX.Controls.Add(this.diaCB8);
            this.SecuBasicaGBOX.Controls.Add(this.diaCB7);
            this.SecuBasicaGBOX.Controls.Add(this.diaCB6);
            this.SecuBasicaGBOX.Controls.Add(this.diaCB5);
            this.SecuBasicaGBOX.Controls.Add(this.CantHrsCB8);
            this.SecuBasicaGBOX.Controls.Add(this.CantHrsCB7);
            this.SecuBasicaGBOX.Controls.Add(this.CantHrsCB6);
            this.SecuBasicaGBOX.Controls.Add(this.CantHrsCB5);
            this.SecuBasicaGBOX.Controls.Add(this.DivisionCB8);
            this.SecuBasicaGBOX.Controls.Add(this.DivisionCB7);
            this.SecuBasicaGBOX.Controls.Add(this.DivisionCB6);
            this.SecuBasicaGBOX.Controls.Add(this.DivisionCB5);
            this.SecuBasicaGBOX.Controls.Add(this.Curso8CB);
            this.SecuBasicaGBOX.Controls.Add(this.Curso7CB);
            this.SecuBasicaGBOX.Controls.Add(this.Curso6CB);
            this.SecuBasicaGBOX.Controls.Add(this.Curso5CB);
            this.SecuBasicaGBOX.Controls.Add(this.AsigCB8);
            this.SecuBasicaGBOX.Controls.Add(this.AsigCB7);
            this.SecuBasicaGBOX.Controls.Add(this.AsigCB6);
            this.SecuBasicaGBOX.Controls.Add(this.AsigCB5);
            this.SecuBasicaGBOX.Controls.Add(this.AporteCB4);
            this.SecuBasicaGBOX.Controls.Add(this.AporteCB3);
            this.SecuBasicaGBOX.Controls.Add(this.AporteCB2);
            this.SecuBasicaGBOX.Controls.Add(this.AporteCB1);
            this.SecuBasicaGBOX.Controls.Add(this.Label29);
            this.SecuBasicaGBOX.Controls.Add(this.Label23);
            this.SecuBasicaGBOX.Controls.Add(this.Label25);
            this.SecuBasicaGBOX.Controls.Add(this.Label17);
            this.SecuBasicaGBOX.Controls.Add(this.ComboBox4);
            this.SecuBasicaGBOX.Controls.Add(this.CategCB4);
            this.SecuBasicaGBOX.Controls.Add(this.ComboBox3);
            this.SecuBasicaGBOX.Controls.Add(this.CategCB3);
            this.SecuBasicaGBOX.Controls.Add(this.ComboBox2);
            this.SecuBasicaGBOX.Controls.Add(this.CategCB2);
            this.SecuBasicaGBOX.Controls.Add(this.ComboBox1);
            this.SecuBasicaGBOX.Controls.Add(this.CategCB1);
            this.SecuBasicaGBOX.Controls.Add(this.Label26);
            this.SecuBasicaGBOX.Controls.Add(this.Label16);
            this.SecuBasicaGBOX.Controls.Add(this.Label15);
            this.SecuBasicaGBOX.Controls.Add(this.Label14);
            this.SecuBasicaGBOX.Controls.Add(this.ApellidoTXT);
            this.SecuBasicaGBOX.Controls.Add(this.NombreTXT);
            this.SecuBasicaGBOX.Controls.Add(this.HrFinCB4);
            this.SecuBasicaGBOX.Controls.Add(this.HrFinCB3);
            this.SecuBasicaGBOX.Controls.Add(this.HrFinCB2);
            this.SecuBasicaGBOX.Controls.Add(this.HrFinCB1);
            this.SecuBasicaGBOX.Controls.Add(this.HrIniCB4);
            this.SecuBasicaGBOX.Controls.Add(this.HrIniCB3);
            this.SecuBasicaGBOX.Controls.Add(this.HrIniCB2);
            this.SecuBasicaGBOX.Controls.Add(this.HrIniCB1);
            this.SecuBasicaGBOX.Controls.Add(this.Label12);
            this.SecuBasicaGBOX.Controls.Add(this.Label13);
            this.SecuBasicaGBOX.Controls.Add(this.Label11);
            this.SecuBasicaGBOX.Controls.Add(this.diaCB4);
            this.SecuBasicaGBOX.Controls.Add(this.diaCB3);
            this.SecuBasicaGBOX.Controls.Add(this.DiaCB2);
            this.SecuBasicaGBOX.Controls.Add(this.diaCB1);
            this.SecuBasicaGBOX.Controls.Add(this.CantHrsCB4);
            this.SecuBasicaGBOX.Controls.Add(this.CantHrsCB3);
            this.SecuBasicaGBOX.Controls.Add(this.CantHrsCB2);
            this.SecuBasicaGBOX.Controls.Add(this.CantHrsCB1);
            this.SecuBasicaGBOX.Controls.Add(this.DivisionCB4);
            this.SecuBasicaGBOX.Controls.Add(this.DivisionCB3);
            this.SecuBasicaGBOX.Controls.Add(this.DivisionCB2);
            this.SecuBasicaGBOX.Controls.Add(this.DivisionCB1);
            this.SecuBasicaGBOX.Controls.Add(this.Curso4CB);
            this.SecuBasicaGBOX.Controls.Add(this.Curso3CB);
            this.SecuBasicaGBOX.Controls.Add(this.Curso2CB);
            this.SecuBasicaGBOX.Controls.Add(this.Curso1CB);
            this.SecuBasicaGBOX.Controls.Add(this.AsigCB4);
            this.SecuBasicaGBOX.Controls.Add(this.AsigCB3);
            this.SecuBasicaGBOX.Controls.Add(this.AsigCB2);
            this.SecuBasicaGBOX.Controls.Add(this.Situacion8CB);
            this.SecuBasicaGBOX.Controls.Add(this.Situacion7CB);
            this.SecuBasicaGBOX.Controls.Add(this.Situacion6CB);
            this.SecuBasicaGBOX.Controls.Add(this.Situacion5CB);
            this.SecuBasicaGBOX.Controls.Add(this.Situacion4CB);
            this.SecuBasicaGBOX.Controls.Add(this.Situacion3CB);
            this.SecuBasicaGBOX.Controls.Add(this.Situacion2CB);
            this.SecuBasicaGBOX.Controls.Add(this.Suplente8CB);
            this.SecuBasicaGBOX.Controls.Add(this.Suplente7CB);
            this.SecuBasicaGBOX.Controls.Add(this.Suplente6CB);
            this.SecuBasicaGBOX.Controls.Add(this.Suplente5CB);
            this.SecuBasicaGBOX.Controls.Add(this.Suplente4CB);
            this.SecuBasicaGBOX.Controls.Add(this.Suplente3CB);
            this.SecuBasicaGBOX.Controls.Add(this.Suplente2CB);
            this.SecuBasicaGBOX.Controls.Add(this.Suplente1CB);
            this.SecuBasicaGBOX.Controls.Add(this.Situacion1CB);
            this.SecuBasicaGBOX.Controls.Add(this.AsigCB1);
            this.SecuBasicaGBOX.Controls.Add(this.Label10);
            this.SecuBasicaGBOX.Controls.Add(this.Label28);
            this.SecuBasicaGBOX.Controls.Add(this.Label27);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox11);
            this.SecuBasicaGBOX.Controls.Add(this.TextBox8);
            this.SecuBasicaGBOX.Controls.Add(this.Label9);
            this.SecuBasicaGBOX.Controls.Add(this.totalTXT);
            this.SecuBasicaGBOX.Controls.Add(this.Label8);
            this.SecuBasicaGBOX.Controls.Add(this.Label7);
            this.SecuBasicaGBOX.Controls.Add(this.Label6);
            this.SecuBasicaGBOX.Controls.Add(this.Label24);
            this.SecuBasicaGBOX.Controls.Add(this.Label22);
            this.SecuBasicaGBOX.Controls.Add(this.Label5);
            this.SecuBasicaGBOX.Controls.Add(this.DNIsbTXT);
            this.SecuBasicaGBOX.Controls.Add(this.Label21);
            this.SecuBasicaGBOX.Controls.Add(this.Label20);
            this.SecuBasicaGBOX.Controls.Add(this.Label19);
            this.SecuBasicaGBOX.Controls.Add(this.Label18);
            this.SecuBasicaGBOX.Controls.Add(this.Label4);
            this.SecuBasicaGBOX.Controls.Add(this.Label3);
            this.SecuBasicaGBOX.Controls.Add(this.Label2);
            this.SecuBasicaGBOX.Controls.Add(this.Label1);
            this.SecuBasicaGBOX.Location = new System.Drawing.Point(-10, 1);
            this.SecuBasicaGBOX.Name = "SecuBasicaGBOX";
            this.SecuBasicaGBOX.Size = new System.Drawing.Size(1363, 510);
            this.SecuBasicaGBOX.TabIndex = 4;
            this.SecuBasicaGBOX.TabStop = false;
            this.SecuBasicaGBOX.Text = "Modificacion De Presentismo";
            // 
            // Button3
            // 
            this.Button3.AccessibleName = "actualizar";
            this.Button3.BackColor = System.Drawing.Color.White;
            this.Button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Button3.Image = global::Presentismo_2015.Properties.Resources.update;
            this.Button3.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Button3.Location = new System.Drawing.Point(91, 415);
            this.Button3.Name = "Button3";
            this.Button3.Size = new System.Drawing.Size(61, 63);
            this.Button3.TabIndex = 426;
            this.Button3.Text = "Actualizar";
            this.Button3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Button3.UseVisualStyleBackColor = false;
            // 
            // Button2
            // 
            this.Button2.BackColor = System.Drawing.Color.White;
            this.Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Button2.Image = global::Presentismo_2015.Properties.Resources.door_out;
            this.Button2.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Button2.Location = new System.Drawing.Point(1219, 415);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(56, 70);
            this.Button2.TabIndex = 425;
            this.Button2.Text = "Cerrar";
            this.Button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Button2.UseVisualStyleBackColor = false;
            // 
            // TextBox19
            // 
            this.TextBox19.Location = new System.Drawing.Point(1260, 331);
            this.TextBox19.Name = "TextBox19";
            this.TextBox19.Size = new System.Drawing.Size(101, 20);
            this.TextBox19.TabIndex = 386;
            // 
            // TextBox26
            // 
            this.TextBox26.Location = new System.Drawing.Point(815, 331);
            this.TextBox26.Name = "TextBox26";
            this.TextBox26.Size = new System.Drawing.Size(62, 20);
            this.TextBox26.TabIndex = 386;
            // 
            // TextBox10
            // 
            this.TextBox10.Location = new System.Drawing.Point(1153, 331);
            this.TextBox10.Name = "TextBox10";
            this.TextBox10.Size = new System.Drawing.Size(88, 20);
            this.TextBox10.TabIndex = 386;
            // 
            // TextBox18
            // 
            this.TextBox18.Location = new System.Drawing.Point(1260, 369);
            this.TextBox18.Name = "TextBox18";
            this.TextBox18.Size = new System.Drawing.Size(101, 20);
            this.TextBox18.TabIndex = 385;
            // 
            // TextBox25
            // 
            this.TextBox25.Location = new System.Drawing.Point(815, 369);
            this.TextBox25.Name = "TextBox25";
            this.TextBox25.Size = new System.Drawing.Size(62, 20);
            this.TextBox25.TabIndex = 385;
            // 
            // TextBox9
            // 
            this.TextBox9.Location = new System.Drawing.Point(1153, 369);
            this.TextBox9.Name = "TextBox9";
            this.TextBox9.Size = new System.Drawing.Size(88, 20);
            this.TextBox9.TabIndex = 385;
            // 
            // TextBox17
            // 
            this.TextBox17.Location = new System.Drawing.Point(1260, 297);
            this.TextBox17.Name = "TextBox17";
            this.TextBox17.Size = new System.Drawing.Size(101, 20);
            this.TextBox17.TabIndex = 383;
            // 
            // TextBox24
            // 
            this.TextBox24.Location = new System.Drawing.Point(815, 297);
            this.TextBox24.Name = "TextBox24";
            this.TextBox24.Size = new System.Drawing.Size(62, 20);
            this.TextBox24.TabIndex = 383;
            // 
            // TextBox7
            // 
            this.TextBox7.Location = new System.Drawing.Point(1153, 297);
            this.TextBox7.Name = "TextBox7";
            this.TextBox7.Size = new System.Drawing.Size(88, 20);
            this.TextBox7.TabIndex = 383;
            // 
            // TextBox16
            // 
            this.TextBox16.Location = new System.Drawing.Point(1260, 261);
            this.TextBox16.Name = "TextBox16";
            this.TextBox16.Size = new System.Drawing.Size(101, 20);
            this.TextBox16.TabIndex = 382;
            // 
            // TextBox23
            // 
            this.TextBox23.Location = new System.Drawing.Point(815, 261);
            this.TextBox23.Name = "TextBox23";
            this.TextBox23.Size = new System.Drawing.Size(62, 20);
            this.TextBox23.TabIndex = 382;
            // 
            // TextBox6
            // 
            this.TextBox6.Location = new System.Drawing.Point(1153, 261);
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.Size = new System.Drawing.Size(88, 20);
            this.TextBox6.TabIndex = 382;
            // 
            // TextBox15
            // 
            this.TextBox15.Location = new System.Drawing.Point(1260, 224);
            this.TextBox15.Name = "TextBox15";
            this.TextBox15.Size = new System.Drawing.Size(101, 20);
            this.TextBox15.TabIndex = 381;
            // 
            // TextBox22
            // 
            this.TextBox22.Location = new System.Drawing.Point(815, 224);
            this.TextBox22.Name = "TextBox22";
            this.TextBox22.Size = new System.Drawing.Size(62, 20);
            this.TextBox22.TabIndex = 381;
            // 
            // TextBox5
            // 
            this.TextBox5.Location = new System.Drawing.Point(1153, 224);
            this.TextBox5.Name = "TextBox5";
            this.TextBox5.Size = new System.Drawing.Size(88, 20);
            this.TextBox5.TabIndex = 381;
            // 
            // TextBox14
            // 
            this.TextBox14.Location = new System.Drawing.Point(1260, 191);
            this.TextBox14.Name = "TextBox14";
            this.TextBox14.Size = new System.Drawing.Size(101, 20);
            this.TextBox14.TabIndex = 380;
            // 
            // TextBox21
            // 
            this.TextBox21.Location = new System.Drawing.Point(815, 191);
            this.TextBox21.Name = "TextBox21";
            this.TextBox21.Size = new System.Drawing.Size(62, 20);
            this.TextBox21.TabIndex = 380;
            // 
            // TextBox4
            // 
            this.TextBox4.Location = new System.Drawing.Point(1153, 191);
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.Size = new System.Drawing.Size(88, 20);
            this.TextBox4.TabIndex = 380;
            // 
            // TextBox13
            // 
            this.TextBox13.Location = new System.Drawing.Point(1260, 153);
            this.TextBox13.Name = "TextBox13";
            this.TextBox13.Size = new System.Drawing.Size(101, 20);
            this.TextBox13.TabIndex = 379;
            // 
            // TextBox20
            // 
            this.TextBox20.Location = new System.Drawing.Point(815, 153);
            this.TextBox20.Name = "TextBox20";
            this.TextBox20.Size = new System.Drawing.Size(62, 20);
            this.TextBox20.TabIndex = 379;
            this.TextBox20.Text = "7275";
            // 
            // TextBox2
            // 
            this.TextBox2.Location = new System.Drawing.Point(1153, 153);
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Size = new System.Drawing.Size(88, 20);
            this.TextBox2.TabIndex = 379;
            // 
            // TextBox12
            // 
            this.TextBox12.Location = new System.Drawing.Point(1260, 118);
            this.TextBox12.Name = "TextBox12";
            this.TextBox12.Size = new System.Drawing.Size(101, 20);
            this.TextBox12.TabIndex = 378;
            // 
            // TextBox3
            // 
            this.TextBox3.Location = new System.Drawing.Point(815, 118);
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Size = new System.Drawing.Size(62, 20);
            this.TextBox3.TabIndex = 378;
            this.TextBox3.Text = "4491";
            // 
            // TextBox1
            // 
            this.TextBox1.Location = new System.Drawing.Point(1153, 118);
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Size = new System.Drawing.Size(88, 20);
            this.TextBox1.TabIndex = 378;
            // 
            // AporteCB8
            // 
            this.AporteCB8.FormattingEnabled = true;
            this.AporteCB8.Items.AddRange(new object[] {
            "Con Aporte",
            "Sin Aporte"});
            this.AporteCB8.Location = new System.Drawing.Point(960, 369);
            this.AporteCB8.Name = "AporteCB8";
            this.AporteCB8.Size = new System.Drawing.Size(112, 21);
            this.AporteCB8.TabIndex = 377;
            // 
            // AporteCB7
            // 
            this.AporteCB7.FormattingEnabled = true;
            this.AporteCB7.Items.AddRange(new object[] {
            "Con Aporte",
            "Sin Aporte"});
            this.AporteCB7.Location = new System.Drawing.Point(960, 333);
            this.AporteCB7.Name = "AporteCB7";
            this.AporteCB7.Size = new System.Drawing.Size(112, 21);
            this.AporteCB7.TabIndex = 376;
            // 
            // AporteCB6
            // 
            this.AporteCB6.FormattingEnabled = true;
            this.AporteCB6.Items.AddRange(new object[] {
            "Con Aporte",
            "Sin Aporte"});
            this.AporteCB6.Location = new System.Drawing.Point(960, 297);
            this.AporteCB6.Name = "AporteCB6";
            this.AporteCB6.Size = new System.Drawing.Size(112, 21);
            this.AporteCB6.TabIndex = 375;
            // 
            // AporteCB5
            // 
            this.AporteCB5.FormattingEnabled = true;
            this.AporteCB5.Items.AddRange(new object[] {
            "Con Aporte",
            "Sin Aporte"});
            this.AporteCB5.Location = new System.Drawing.Point(960, 261);
            this.AporteCB5.Name = "AporteCB5";
            this.AporteCB5.Size = new System.Drawing.Size(112, 21);
            this.AporteCB5.TabIndex = 374;
            // 
            // ComboBox8
            // 
            this.ComboBox8.FormattingEnabled = true;
            this.ComboBox8.Items.AddRange(new object[] {
            "Presente",
            "Ausente"});
            this.ComboBox8.Location = new System.Drawing.Point(1080, 369);
            this.ComboBox8.Name = "ComboBox8";
            this.ComboBox8.Size = new System.Drawing.Size(63, 21);
            this.ComboBox8.TabIndex = 373;
            // 
            // CategCB8
            // 
            this.CategCB8.FormattingEnabled = true;
            this.CategCB8.Items.AddRange(new object[] {
            "Presente",
            "Ausente"});
            this.CategCB8.Location = new System.Drawing.Point(889, 369);
            this.CategCB8.Name = "CategCB8";
            this.CategCB8.Size = new System.Drawing.Size(63, 21);
            this.CategCB8.TabIndex = 373;
            // 
            // ComboBox7
            // 
            this.ComboBox7.FormattingEnabled = true;
            this.ComboBox7.Items.AddRange(new object[] {
            "Presente",
            "Ausente"});
            this.ComboBox7.Location = new System.Drawing.Point(1080, 333);
            this.ComboBox7.Name = "ComboBox7";
            this.ComboBox7.Size = new System.Drawing.Size(63, 21);
            this.ComboBox7.TabIndex = 372;
            // 
            // CategCB7
            // 
            this.CategCB7.FormattingEnabled = true;
            this.CategCB7.Items.AddRange(new object[] {
            "Presente",
            "Ausente"});
            this.CategCB7.Location = new System.Drawing.Point(889, 333);
            this.CategCB7.Name = "CategCB7";
            this.CategCB7.Size = new System.Drawing.Size(63, 21);
            this.CategCB7.TabIndex = 372;
            // 
            // ComboBox6
            // 
            this.ComboBox6.FormattingEnabled = true;
            this.ComboBox6.Items.AddRange(new object[] {
            "Presente",
            "Ausente"});
            this.ComboBox6.Location = new System.Drawing.Point(1080, 297);
            this.ComboBox6.Name = "ComboBox6";
            this.ComboBox6.Size = new System.Drawing.Size(63, 21);
            this.ComboBox6.TabIndex = 371;
            // 
            // CategCB6
            // 
            this.CategCB6.FormattingEnabled = true;
            this.CategCB6.Items.AddRange(new object[] {
            "Presente",
            "Ausente"});
            this.CategCB6.Location = new System.Drawing.Point(889, 297);
            this.CategCB6.Name = "CategCB6";
            this.CategCB6.Size = new System.Drawing.Size(63, 21);
            this.CategCB6.TabIndex = 371;
            // 
            // ComboBox5
            // 
            this.ComboBox5.FormattingEnabled = true;
            this.ComboBox5.Items.AddRange(new object[] {
            "Presente",
            "Ausente"});
            this.ComboBox5.Location = new System.Drawing.Point(1080, 261);
            this.ComboBox5.Name = "ComboBox5";
            this.ComboBox5.Size = new System.Drawing.Size(63, 21);
            this.ComboBox5.TabIndex = 370;
            // 
            // CategCB5
            // 
            this.CategCB5.FormattingEnabled = true;
            this.CategCB5.Items.AddRange(new object[] {
            "Presente",
            "Ausente"});
            this.CategCB5.Location = new System.Drawing.Point(889, 261);
            this.CategCB5.Name = "CategCB5";
            this.CategCB5.Size = new System.Drawing.Size(63, 21);
            this.CategCB5.TabIndex = 370;
            // 
            // HrFinCB8
            // 
            this.HrFinCB8.FormattingEnabled = true;
            this.HrFinCB8.Location = new System.Drawing.Point(679, 369);
            this.HrFinCB8.Name = "HrFinCB8";
            this.HrFinCB8.Size = new System.Drawing.Size(57, 21);
            this.HrFinCB8.TabIndex = 369;
            // 
            // HrFinCB7
            // 
            this.HrFinCB7.FormattingEnabled = true;
            this.HrFinCB7.Location = new System.Drawing.Point(679, 333);
            this.HrFinCB7.Name = "HrFinCB7";
            this.HrFinCB7.Size = new System.Drawing.Size(57, 21);
            this.HrFinCB7.TabIndex = 368;
            // 
            // HrFinCB6
            // 
            this.HrFinCB6.FormattingEnabled = true;
            this.HrFinCB6.Location = new System.Drawing.Point(679, 297);
            this.HrFinCB6.Name = "HrFinCB6";
            this.HrFinCB6.Size = new System.Drawing.Size(57, 21);
            this.HrFinCB6.TabIndex = 367;
            // 
            // HrFinCB5
            // 
            this.HrFinCB5.FormattingEnabled = true;
            this.HrFinCB5.Location = new System.Drawing.Point(679, 261);
            this.HrFinCB5.Name = "HrFinCB5";
            this.HrFinCB5.Size = new System.Drawing.Size(57, 21);
            this.HrFinCB5.TabIndex = 366;
            // 
            // HrIniCB8
            // 
            this.HrIniCB8.FormattingEnabled = true;
            this.HrIniCB8.Location = new System.Drawing.Point(590, 369);
            this.HrIniCB8.Name = "HrIniCB8";
            this.HrIniCB8.Size = new System.Drawing.Size(57, 21);
            this.HrIniCB8.TabIndex = 365;
            // 
            // HrIniCB7
            // 
            this.HrIniCB7.FormattingEnabled = true;
            this.HrIniCB7.Location = new System.Drawing.Point(590, 333);
            this.HrIniCB7.Name = "HrIniCB7";
            this.HrIniCB7.Size = new System.Drawing.Size(57, 21);
            this.HrIniCB7.TabIndex = 364;
            // 
            // HrIniCB6
            // 
            this.HrIniCB6.FormattingEnabled = true;
            this.HrIniCB6.Location = new System.Drawing.Point(590, 297);
            this.HrIniCB6.Name = "HrIniCB6";
            this.HrIniCB6.Size = new System.Drawing.Size(57, 21);
            this.HrIniCB6.TabIndex = 363;
            // 
            // HrIniCB5
            // 
            this.HrIniCB5.FormattingEnabled = true;
            this.HrIniCB5.Location = new System.Drawing.Point(590, 261);
            this.HrIniCB5.Name = "HrIniCB5";
            this.HrIniCB5.Size = new System.Drawing.Size(57, 21);
            this.HrIniCB5.TabIndex = 362;
            // 
            // diaCB8
            // 
            this.diaCB8.FormattingEnabled = true;
            this.diaCB8.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes"});
            this.diaCB8.Location = new System.Drawing.Point(502, 369);
            this.diaCB8.Name = "diaCB8";
            this.diaCB8.Size = new System.Drawing.Size(73, 21);
            this.diaCB8.TabIndex = 361;
            // 
            // diaCB7
            // 
            this.diaCB7.FormattingEnabled = true;
            this.diaCB7.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes"});
            this.diaCB7.Location = new System.Drawing.Point(502, 333);
            this.diaCB7.Name = "diaCB7";
            this.diaCB7.Size = new System.Drawing.Size(73, 21);
            this.diaCB7.TabIndex = 360;
            // 
            // diaCB6
            // 
            this.diaCB6.FormattingEnabled = true;
            this.diaCB6.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes"});
            this.diaCB6.Location = new System.Drawing.Point(502, 297);
            this.diaCB6.Name = "diaCB6";
            this.diaCB6.Size = new System.Drawing.Size(73, 21);
            this.diaCB6.TabIndex = 359;
            // 
            // diaCB5
            // 
            this.diaCB5.FormattingEnabled = true;
            this.diaCB5.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes"});
            this.diaCB5.Location = new System.Drawing.Point(502, 261);
            this.diaCB5.Name = "diaCB5";
            this.diaCB5.Size = new System.Drawing.Size(73, 21);
            this.diaCB5.TabIndex = 358;
            // 
            // CantHrsCB8
            // 
            this.CantHrsCB8.FormattingEnabled = true;
            this.CantHrsCB8.Items.AddRange(new object[] {
            "0"});
            this.CantHrsCB8.Location = new System.Drawing.Point(759, 372);
            this.CantHrsCB8.Name = "CantHrsCB8";
            this.CantHrsCB8.Size = new System.Drawing.Size(38, 21);
            this.CantHrsCB8.TabIndex = 357;
            this.CantHrsCB8.Text = "0";
            // 
            // CantHrsCB7
            // 
            this.CantHrsCB7.FormattingEnabled = true;
            this.CantHrsCB7.Items.AddRange(new object[] {
            "0"});
            this.CantHrsCB7.Location = new System.Drawing.Point(759, 335);
            this.CantHrsCB7.Name = "CantHrsCB7";
            this.CantHrsCB7.Size = new System.Drawing.Size(38, 21);
            this.CantHrsCB7.TabIndex = 356;
            this.CantHrsCB7.Text = "0";
            // 
            // CantHrsCB6
            // 
            this.CantHrsCB6.FormattingEnabled = true;
            this.CantHrsCB6.Items.AddRange(new object[] {
            "0"});
            this.CantHrsCB6.Location = new System.Drawing.Point(759, 298);
            this.CantHrsCB6.Name = "CantHrsCB6";
            this.CantHrsCB6.Size = new System.Drawing.Size(38, 21);
            this.CantHrsCB6.TabIndex = 355;
            this.CantHrsCB6.Text = "0";
            // 
            // CantHrsCB5
            // 
            this.CantHrsCB5.FormattingEnabled = true;
            this.CantHrsCB5.Items.AddRange(new object[] {
            "0"});
            this.CantHrsCB5.Location = new System.Drawing.Point(759, 261);
            this.CantHrsCB5.Name = "CantHrsCB5";
            this.CantHrsCB5.Size = new System.Drawing.Size(38, 21);
            this.CantHrsCB5.TabIndex = 354;
            this.CantHrsCB5.Text = "0";
            // 
            // DivisionCB8
            // 
            this.DivisionCB8.FormattingEnabled = true;
            this.DivisionCB8.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.DivisionCB8.Location = new System.Drawing.Point(452, 369);
            this.DivisionCB8.Name = "DivisionCB8";
            this.DivisionCB8.Size = new System.Drawing.Size(34, 21);
            this.DivisionCB8.TabIndex = 353;
            // 
            // DivisionCB7
            // 
            this.DivisionCB7.FormattingEnabled = true;
            this.DivisionCB7.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.DivisionCB7.Location = new System.Drawing.Point(452, 333);
            this.DivisionCB7.Name = "DivisionCB7";
            this.DivisionCB7.Size = new System.Drawing.Size(34, 21);
            this.DivisionCB7.TabIndex = 352;
            // 
            // DivisionCB6
            // 
            this.DivisionCB6.FormattingEnabled = true;
            this.DivisionCB6.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.DivisionCB6.Location = new System.Drawing.Point(452, 297);
            this.DivisionCB6.Name = "DivisionCB6";
            this.DivisionCB6.Size = new System.Drawing.Size(34, 21);
            this.DivisionCB6.TabIndex = 351;
            // 
            // DivisionCB5
            // 
            this.DivisionCB5.FormattingEnabled = true;
            this.DivisionCB5.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.DivisionCB5.Location = new System.Drawing.Point(452, 261);
            this.DivisionCB5.Name = "DivisionCB5";
            this.DivisionCB5.Size = new System.Drawing.Size(34, 21);
            this.DivisionCB5.TabIndex = 350;
            // 
            // Curso8CB
            // 
            this.Curso8CB.FormattingEnabled = true;
            this.Curso8CB.Items.AddRange(new object[] {
            "1°",
            "2°",
            "3°",
            "4°",
            "5°",
            "6°",
            "7°",
            "8°",
            "9°"});
            this.Curso8CB.Location = new System.Drawing.Point(399, 369);
            this.Curso8CB.Name = "Curso8CB";
            this.Curso8CB.Size = new System.Drawing.Size(33, 21);
            this.Curso8CB.TabIndex = 349;
            // 
            // Curso7CB
            // 
            this.Curso7CB.FormattingEnabled = true;
            this.Curso7CB.Items.AddRange(new object[] {
            "1°",
            "2°",
            "3°",
            "4°",
            "5°",
            "6°",
            "7°",
            "8°",
            "9°"});
            this.Curso7CB.Location = new System.Drawing.Point(399, 333);
            this.Curso7CB.Name = "Curso7CB";
            this.Curso7CB.Size = new System.Drawing.Size(33, 21);
            this.Curso7CB.TabIndex = 348;
            // 
            // Curso6CB
            // 
            this.Curso6CB.FormattingEnabled = true;
            this.Curso6CB.Items.AddRange(new object[] {
            "1°",
            "2°",
            "3°",
            "4°",
            "5°",
            "6°",
            "7°",
            "8°",
            "9°"});
            this.Curso6CB.Location = new System.Drawing.Point(399, 297);
            this.Curso6CB.Name = "Curso6CB";
            this.Curso6CB.Size = new System.Drawing.Size(33, 21);
            this.Curso6CB.TabIndex = 347;
            // 
            // Curso5CB
            // 
            this.Curso5CB.FormattingEnabled = true;
            this.Curso5CB.Items.AddRange(new object[] {
            "1°",
            "2°",
            "3°",
            "4°",
            "5°",
            "6°",
            "7°",
            "8°",
            "9°"});
            this.Curso5CB.Location = new System.Drawing.Point(399, 261);
            this.Curso5CB.Name = "Curso5CB";
            this.Curso5CB.Size = new System.Drawing.Size(33, 21);
            this.Curso5CB.TabIndex = 346;
            // 
            // AsigCB8
            // 
            this.AsigCB8.FormattingEnabled = true;
            this.AsigCB8.Location = new System.Drawing.Point(266, 369);
            this.AsigCB8.Name = "AsigCB8";
            this.AsigCB8.Size = new System.Drawing.Size(121, 21);
            this.AsigCB8.TabIndex = 345;
            // 
            // AsigCB7
            // 
            this.AsigCB7.FormattingEnabled = true;
            this.AsigCB7.Location = new System.Drawing.Point(266, 333);
            this.AsigCB7.Name = "AsigCB7";
            this.AsigCB7.Size = new System.Drawing.Size(121, 21);
            this.AsigCB7.TabIndex = 344;
            // 
            // AsigCB6
            // 
            this.AsigCB6.FormattingEnabled = true;
            this.AsigCB6.Location = new System.Drawing.Point(266, 297);
            this.AsigCB6.Name = "AsigCB6";
            this.AsigCB6.Size = new System.Drawing.Size(121, 21);
            this.AsigCB6.TabIndex = 343;
            // 
            // AsigCB5
            // 
            this.AsigCB5.FormattingEnabled = true;
            this.AsigCB5.Location = new System.Drawing.Point(266, 261);
            this.AsigCB5.Name = "AsigCB5";
            this.AsigCB5.Size = new System.Drawing.Size(121, 21);
            this.AsigCB5.TabIndex = 342;
            // 
            // AporteCB4
            // 
            this.AporteCB4.FormattingEnabled = true;
            this.AporteCB4.Items.AddRange(new object[] {
            "Con Aporte",
            "Sin Aporte"});
            this.AporteCB4.Location = new System.Drawing.Point(960, 225);
            this.AporteCB4.Name = "AporteCB4";
            this.AporteCB4.Size = new System.Drawing.Size(112, 21);
            this.AporteCB4.TabIndex = 341;
            // 
            // AporteCB3
            // 
            this.AporteCB3.FormattingEnabled = true;
            this.AporteCB3.Items.AddRange(new object[] {
            "Con Aporte",
            "Sin Aporte"});
            this.AporteCB3.Location = new System.Drawing.Point(960, 189);
            this.AporteCB3.Name = "AporteCB3";
            this.AporteCB3.Size = new System.Drawing.Size(112, 21);
            this.AporteCB3.TabIndex = 340;
            // 
            // AporteCB2
            // 
            this.AporteCB2.FormattingEnabled = true;
            this.AporteCB2.Items.AddRange(new object[] {
            "Con Aporte",
            "Sin Aporte",
            "Descuento"});
            this.AporteCB2.Location = new System.Drawing.Point(960, 153);
            this.AporteCB2.Name = "AporteCB2";
            this.AporteCB2.Size = new System.Drawing.Size(112, 21);
            this.AporteCB2.TabIndex = 339;
            this.AporteCB2.Text = "Con aporte";
            // 
            // AporteCB1
            // 
            this.AporteCB1.FormattingEnabled = true;
            this.AporteCB1.Items.AddRange(new object[] {
            "Con Aporte",
            "Sin Aporte",
            "Descuento"});
            this.AporteCB1.Location = new System.Drawing.Point(960, 117);
            this.AporteCB1.Name = "AporteCB1";
            this.AporteCB1.Size = new System.Drawing.Size(112, 21);
            this.AporteCB1.TabIndex = 338;
            this.AporteCB1.Text = "Con aporte";
            // 
            // Label29
            // 
            this.Label29.AutoSize = true;
            this.Label29.Location = new System.Drawing.Point(1270, 80);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(78, 13);
            this.Label29.TabIndex = 337;
            this.Label29.Text = "Observaciones";
            // 
            // Label23
            // 
            this.Label23.AutoSize = true;
            this.Label23.Location = new System.Drawing.Point(815, 80);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(62, 13);
            this.Label23.TabIndex = 337;
            this.Label23.Text = "DIPREGEP";
            // 
            // Label25
            // 
            this.Label25.AutoSize = true;
            this.Label25.Location = new System.Drawing.Point(1161, 80);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(69, 13);
            this.Label25.TabIndex = 337;
            this.Label25.Text = "Responsable";
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Location = new System.Drawing.Point(987, 80);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(32, 13);
            this.Label17.TabIndex = 337;
            this.Label17.Text = "Pago";
            // 
            // ComboBox4
            // 
            this.ComboBox4.FormattingEnabled = true;
            this.ComboBox4.Items.AddRange(new object[] {
            "Presente",
            "Ausente"});
            this.ComboBox4.Location = new System.Drawing.Point(1080, 225);
            this.ComboBox4.Name = "ComboBox4";
            this.ComboBox4.Size = new System.Drawing.Size(63, 21);
            this.ComboBox4.TabIndex = 336;
            // 
            // CategCB4
            // 
            this.CategCB4.FormattingEnabled = true;
            this.CategCB4.Items.AddRange(new object[] {
            "Presente",
            "Ausente"});
            this.CategCB4.Location = new System.Drawing.Point(889, 225);
            this.CategCB4.Name = "CategCB4";
            this.CategCB4.Size = new System.Drawing.Size(63, 21);
            this.CategCB4.TabIndex = 336;
            // 
            // ComboBox3
            // 
            this.ComboBox3.FormattingEnabled = true;
            this.ComboBox3.Items.AddRange(new object[] {
            "Presente",
            "Ausente"});
            this.ComboBox3.Location = new System.Drawing.Point(1080, 189);
            this.ComboBox3.Name = "ComboBox3";
            this.ComboBox3.Size = new System.Drawing.Size(63, 21);
            this.ComboBox3.TabIndex = 335;
            // 
            // CategCB3
            // 
            this.CategCB3.FormattingEnabled = true;
            this.CategCB3.Items.AddRange(new object[] {
            "Presente",
            "Ausente"});
            this.CategCB3.Location = new System.Drawing.Point(889, 189);
            this.CategCB3.Name = "CategCB3";
            this.CategCB3.Size = new System.Drawing.Size(63, 21);
            this.CategCB3.TabIndex = 335;
            // 
            // ComboBox2
            // 
            this.ComboBox2.FormattingEnabled = true;
            this.ComboBox2.Location = new System.Drawing.Point(1080, 153);
            this.ComboBox2.Name = "ComboBox2";
            this.ComboBox2.Size = new System.Drawing.Size(65, 21);
            this.ComboBox2.TabIndex = 334;
            // 
            // CategCB2
            // 
            this.CategCB2.FormattingEnabled = true;
            this.CategCB2.Items.AddRange(new object[] {
            "Presente",
            "Ausente"});
            this.CategCB2.Location = new System.Drawing.Point(889, 153);
            this.CategCB2.Name = "CategCB2";
            this.CategCB2.Size = new System.Drawing.Size(65, 21);
            this.CategCB2.TabIndex = 334;
            this.CategCB2.Text = "Presente";
            // 
            // ComboBox1
            // 
            this.ComboBox1.FormattingEnabled = true;
            this.ComboBox1.Items.AddRange(new object[] {
            ""});
            this.ComboBox1.Location = new System.Drawing.Point(1080, 117);
            this.ComboBox1.Name = "ComboBox1";
            this.ComboBox1.Size = new System.Drawing.Size(65, 21);
            this.ComboBox1.TabIndex = 333;
            // 
            // CategCB1
            // 
            this.CategCB1.FormattingEnabled = true;
            this.CategCB1.Items.AddRange(new object[] {
            "Presente",
            "Ausente"});
            this.CategCB1.Location = new System.Drawing.Point(889, 117);
            this.CategCB1.Name = "CategCB1";
            this.CategCB1.Size = new System.Drawing.Size(65, 21);
            this.CategCB1.TabIndex = 333;
            this.CategCB1.Text = "Ausente";
            // 
            // Label26
            // 
            this.Label26.AutoSize = true;
            this.Label26.Location = new System.Drawing.Point(1085, 80);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(42, 13);
            this.Label26.TabIndex = 332;
            this.Label26.Text = "Articulo";
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Location = new System.Drawing.Point(894, 80);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(55, 13);
            this.Label16.TabIndex = 332;
            this.Label16.Text = "Asistencia";
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Location = new System.Drawing.Point(727, 27);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(44, 13);
            this.Label15.TabIndex = 331;
            this.Label15.Text = "Apellido";
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(559, 27);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(44, 13);
            this.Label14.TabIndex = 330;
            this.Label14.Text = "Nombre";
            // 
            // ApellidoTXT
            // 
            this.ApellidoTXT.Location = new System.Drawing.Point(779, 24);
            this.ApellidoTXT.Name = "ApellidoTXT";
            this.ApellidoTXT.Size = new System.Drawing.Size(100, 20);
            this.ApellidoTXT.TabIndex = 329;
            // 
            // NombreTXT
            // 
            this.NombreTXT.Location = new System.Drawing.Point(609, 24);
            this.NombreTXT.Name = "NombreTXT";
            this.NombreTXT.Size = new System.Drawing.Size(100, 20);
            this.NombreTXT.TabIndex = 328;
            // 
            // HrFinCB4
            // 
            this.HrFinCB4.FormattingEnabled = true;
            this.HrFinCB4.Location = new System.Drawing.Point(679, 225);
            this.HrFinCB4.Name = "HrFinCB4";
            this.HrFinCB4.Size = new System.Drawing.Size(57, 21);
            this.HrFinCB4.TabIndex = 327;
            // 
            // HrFinCB3
            // 
            this.HrFinCB3.FormattingEnabled = true;
            this.HrFinCB3.Location = new System.Drawing.Point(679, 189);
            this.HrFinCB3.Name = "HrFinCB3";
            this.HrFinCB3.Size = new System.Drawing.Size(57, 21);
            this.HrFinCB3.TabIndex = 326;
            // 
            // HrFinCB2
            // 
            this.HrFinCB2.FormattingEnabled = true;
            this.HrFinCB2.Location = new System.Drawing.Point(679, 153);
            this.HrFinCB2.Name = "HrFinCB2";
            this.HrFinCB2.Size = new System.Drawing.Size(57, 21);
            this.HrFinCB2.TabIndex = 325;
            this.HrFinCB2.Text = "12:00";
            // 
            // HrFinCB1
            // 
            this.HrFinCB1.FormattingEnabled = true;
            this.HrFinCB1.Location = new System.Drawing.Point(679, 117);
            this.HrFinCB1.Name = "HrFinCB1";
            this.HrFinCB1.Size = new System.Drawing.Size(57, 21);
            this.HrFinCB1.TabIndex = 324;
            this.HrFinCB1.Text = "9:00";
            // 
            // HrIniCB4
            // 
            this.HrIniCB4.FormattingEnabled = true;
            this.HrIniCB4.Location = new System.Drawing.Point(590, 225);
            this.HrIniCB4.Name = "HrIniCB4";
            this.HrIniCB4.Size = new System.Drawing.Size(57, 21);
            this.HrIniCB4.TabIndex = 323;
            // 
            // HrIniCB3
            // 
            this.HrIniCB3.FormattingEnabled = true;
            this.HrIniCB3.Location = new System.Drawing.Point(590, 189);
            this.HrIniCB3.Name = "HrIniCB3";
            this.HrIniCB3.Size = new System.Drawing.Size(57, 21);
            this.HrIniCB3.TabIndex = 322;
            // 
            // HrIniCB2
            // 
            this.HrIniCB2.FormattingEnabled = true;
            this.HrIniCB2.Location = new System.Drawing.Point(590, 153);
            this.HrIniCB2.Name = "HrIniCB2";
            this.HrIniCB2.Size = new System.Drawing.Size(57, 21);
            this.HrIniCB2.TabIndex = 321;
            this.HrIniCB2.Text = "11:00";
            // 
            // HrIniCB1
            // 
            this.HrIniCB1.FormattingEnabled = true;
            this.HrIniCB1.Location = new System.Drawing.Point(590, 117);
            this.HrIniCB1.Name = "HrIniCB1";
            this.HrIniCB1.Size = new System.Drawing.Size(57, 21);
            this.HrIniCB1.TabIndex = 320;
            this.HrIniCB1.Text = "8:00";
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(666, 80);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(79, 13);
            this.Label12.TabIndex = 317;
            this.Label12.Text = "Horario Finaliza";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(518, 80);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(37, 13);
            this.Label13.TabIndex = 318;
            this.Label13.Text = "Fecha";
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(578, 80);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(69, 13);
            this.Label11.TabIndex = 319;
            this.Label11.Text = "Horario Inicio";
            // 
            // diaCB4
            // 
            this.diaCB4.FormattingEnabled = true;
            this.diaCB4.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes"});
            this.diaCB4.Location = new System.Drawing.Point(502, 225);
            this.diaCB4.Name = "diaCB4";
            this.diaCB4.Size = new System.Drawing.Size(73, 21);
            this.diaCB4.TabIndex = 316;
            // 
            // diaCB3
            // 
            this.diaCB3.FormattingEnabled = true;
            this.diaCB3.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes"});
            this.diaCB3.Location = new System.Drawing.Point(502, 189);
            this.diaCB3.Name = "diaCB3";
            this.diaCB3.Size = new System.Drawing.Size(73, 21);
            this.diaCB3.TabIndex = 315;
            // 
            // DiaCB2
            // 
            this.DiaCB2.FormattingEnabled = true;
            this.DiaCB2.Location = new System.Drawing.Point(502, 153);
            this.DiaCB2.Name = "DiaCB2";
            this.DiaCB2.Size = new System.Drawing.Size(73, 21);
            this.DiaCB2.TabIndex = 314;
            this.DiaCB2.Text = "09/05/2014";
            // 
            // diaCB1
            // 
            this.diaCB1.FormattingEnabled = true;
            this.diaCB1.Location = new System.Drawing.Point(502, 117);
            this.diaCB1.Name = "diaCB1";
            this.diaCB1.Size = new System.Drawing.Size(73, 21);
            this.diaCB1.TabIndex = 313;
            this.diaCB1.Text = "20/10/2014";
            // 
            // CantHrsCB4
            // 
            this.CantHrsCB4.FormattingEnabled = true;
            this.CantHrsCB4.Items.AddRange(new object[] {
            "0"});
            this.CantHrsCB4.Location = new System.Drawing.Point(759, 228);
            this.CantHrsCB4.Name = "CantHrsCB4";
            this.CantHrsCB4.Size = new System.Drawing.Size(38, 21);
            this.CantHrsCB4.TabIndex = 310;
            this.CantHrsCB4.Text = "0";
            // 
            // CantHrsCB3
            // 
            this.CantHrsCB3.FormattingEnabled = true;
            this.CantHrsCB3.Items.AddRange(new object[] {
            "0"});
            this.CantHrsCB3.Location = new System.Drawing.Point(759, 191);
            this.CantHrsCB3.Name = "CantHrsCB3";
            this.CantHrsCB3.Size = new System.Drawing.Size(38, 21);
            this.CantHrsCB3.TabIndex = 309;
            this.CantHrsCB3.Text = "0";
            // 
            // CantHrsCB2
            // 
            this.CantHrsCB2.FormattingEnabled = true;
            this.CantHrsCB2.Items.AddRange(new object[] {
            "0"});
            this.CantHrsCB2.Location = new System.Drawing.Point(759, 154);
            this.CantHrsCB2.Name = "CantHrsCB2";
            this.CantHrsCB2.Size = new System.Drawing.Size(38, 21);
            this.CantHrsCB2.TabIndex = 308;
            this.CantHrsCB2.Text = "1";
            // 
            // CantHrsCB1
            // 
            this.CantHrsCB1.FormattingEnabled = true;
            this.CantHrsCB1.Items.AddRange(new object[] {
            "0"});
            this.CantHrsCB1.Location = new System.Drawing.Point(759, 117);
            this.CantHrsCB1.Name = "CantHrsCB1";
            this.CantHrsCB1.Size = new System.Drawing.Size(38, 21);
            this.CantHrsCB1.TabIndex = 307;
            this.CantHrsCB1.Text = "1";
            // 
            // DivisionCB4
            // 
            this.DivisionCB4.FormattingEnabled = true;
            this.DivisionCB4.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.DivisionCB4.Location = new System.Drawing.Point(452, 225);
            this.DivisionCB4.Name = "DivisionCB4";
            this.DivisionCB4.Size = new System.Drawing.Size(34, 21);
            this.DivisionCB4.TabIndex = 306;
            // 
            // DivisionCB3
            // 
            this.DivisionCB3.FormattingEnabled = true;
            this.DivisionCB3.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.DivisionCB3.Location = new System.Drawing.Point(452, 189);
            this.DivisionCB3.Name = "DivisionCB3";
            this.DivisionCB3.Size = new System.Drawing.Size(34, 21);
            this.DivisionCB3.TabIndex = 305;
            // 
            // DivisionCB2
            // 
            this.DivisionCB2.FormattingEnabled = true;
            this.DivisionCB2.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.DivisionCB2.Location = new System.Drawing.Point(452, 153);
            this.DivisionCB2.Name = "DivisionCB2";
            this.DivisionCB2.Size = new System.Drawing.Size(34, 21);
            this.DivisionCB2.TabIndex = 304;
            this.DivisionCB2.Text = "C";
            // 
            // DivisionCB1
            // 
            this.DivisionCB1.FormattingEnabled = true;
            this.DivisionCB1.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.DivisionCB1.Location = new System.Drawing.Point(452, 117);
            this.DivisionCB1.Name = "DivisionCB1";
            this.DivisionCB1.Size = new System.Drawing.Size(34, 21);
            this.DivisionCB1.TabIndex = 303;
            this.DivisionCB1.Text = "A";
            // 
            // Curso4CB
            // 
            this.Curso4CB.FormattingEnabled = true;
            this.Curso4CB.Items.AddRange(new object[] {
            "1°",
            "2°",
            "3°",
            "4°",
            "5°",
            "6°",
            "7°",
            "8°",
            "9°"});
            this.Curso4CB.Location = new System.Drawing.Point(399, 225);
            this.Curso4CB.Name = "Curso4CB";
            this.Curso4CB.Size = new System.Drawing.Size(33, 21);
            this.Curso4CB.TabIndex = 302;
            // 
            // Curso3CB
            // 
            this.Curso3CB.FormattingEnabled = true;
            this.Curso3CB.Items.AddRange(new object[] {
            "1°",
            "2°",
            "3°",
            "4°",
            "5°",
            "6°",
            "7°",
            "8°",
            "9°"});
            this.Curso3CB.Location = new System.Drawing.Point(399, 189);
            this.Curso3CB.Name = "Curso3CB";
            this.Curso3CB.Size = new System.Drawing.Size(33, 21);
            this.Curso3CB.TabIndex = 301;
            // 
            // Curso2CB
            // 
            this.Curso2CB.FormattingEnabled = true;
            this.Curso2CB.Items.AddRange(new object[] {
            "1°",
            "2°",
            "3°",
            "4°",
            "5°",
            "6°",
            "7°",
            "8°",
            "9°"});
            this.Curso2CB.Location = new System.Drawing.Point(399, 153);
            this.Curso2CB.Name = "Curso2CB";
            this.Curso2CB.Size = new System.Drawing.Size(33, 21);
            this.Curso2CB.TabIndex = 300;
            this.Curso2CB.Text = "2°";
            // 
            // Curso1CB
            // 
            this.Curso1CB.FormattingEnabled = true;
            this.Curso1CB.Items.AddRange(new object[] {
            "1°",
            "2°",
            "3°",
            "4°",
            "5°",
            "6°",
            "7°",
            "8°",
            "9°"});
            this.Curso1CB.Location = new System.Drawing.Point(399, 117);
            this.Curso1CB.Name = "Curso1CB";
            this.Curso1CB.Size = new System.Drawing.Size(33, 21);
            this.Curso1CB.TabIndex = 299;
            this.Curso1CB.Text = "1°";
            // 
            // AsigCB4
            // 
            this.AsigCB4.FormattingEnabled = true;
            this.AsigCB4.Location = new System.Drawing.Point(266, 225);
            this.AsigCB4.Name = "AsigCB4";
            this.AsigCB4.Size = new System.Drawing.Size(121, 21);
            this.AsigCB4.TabIndex = 298;
            // 
            // AsigCB3
            // 
            this.AsigCB3.FormattingEnabled = true;
            this.AsigCB3.Location = new System.Drawing.Point(266, 189);
            this.AsigCB3.Name = "AsigCB3";
            this.AsigCB3.Size = new System.Drawing.Size(121, 21);
            this.AsigCB3.TabIndex = 297;
            // 
            // AsigCB2
            // 
            this.AsigCB2.FormattingEnabled = true;
            this.AsigCB2.Location = new System.Drawing.Point(266, 153);
            this.AsigCB2.Name = "AsigCB2";
            this.AsigCB2.Size = new System.Drawing.Size(121, 21);
            this.AsigCB2.TabIndex = 296;
            this.AsigCB2.Text = "Historia";
            // 
            // Situacion8CB
            // 
            this.Situacion8CB.FormattingEnabled = true;
            this.Situacion8CB.Items.AddRange(new object[] {
            "Titular",
            "Suplente"});
            this.Situacion8CB.Location = new System.Drawing.Point(81, 368);
            this.Situacion8CB.Name = "Situacion8CB";
            this.Situacion8CB.Size = new System.Drawing.Size(68, 21);
            this.Situacion8CB.TabIndex = 284;
            // 
            // Situacion7CB
            // 
            this.Situacion7CB.FormattingEnabled = true;
            this.Situacion7CB.Items.AddRange(new object[] {
            "Titular",
            "Suplente"});
            this.Situacion7CB.Location = new System.Drawing.Point(80, 333);
            this.Situacion7CB.Name = "Situacion7CB";
            this.Situacion7CB.Size = new System.Drawing.Size(68, 21);
            this.Situacion7CB.TabIndex = 283;
            // 
            // Situacion6CB
            // 
            this.Situacion6CB.FormattingEnabled = true;
            this.Situacion6CB.Items.AddRange(new object[] {
            "Titular",
            "Suplente"});
            this.Situacion6CB.Location = new System.Drawing.Point(80, 298);
            this.Situacion6CB.Name = "Situacion6CB";
            this.Situacion6CB.Size = new System.Drawing.Size(68, 21);
            this.Situacion6CB.TabIndex = 286;
            // 
            // Situacion5CB
            // 
            this.Situacion5CB.FormattingEnabled = true;
            this.Situacion5CB.Items.AddRange(new object[] {
            "Titular",
            "Suplente"});
            this.Situacion5CB.Location = new System.Drawing.Point(80, 261);
            this.Situacion5CB.Name = "Situacion5CB";
            this.Situacion5CB.Size = new System.Drawing.Size(68, 21);
            this.Situacion5CB.TabIndex = 285;
            // 
            // Situacion4CB
            // 
            this.Situacion4CB.FormattingEnabled = true;
            this.Situacion4CB.Items.AddRange(new object[] {
            "Titular",
            "Suplente"});
            this.Situacion4CB.Location = new System.Drawing.Point(80, 225);
            this.Situacion4CB.Name = "Situacion4CB";
            this.Situacion4CB.Size = new System.Drawing.Size(68, 21);
            this.Situacion4CB.TabIndex = 280;
            // 
            // Situacion3CB
            // 
            this.Situacion3CB.FormattingEnabled = true;
            this.Situacion3CB.Items.AddRange(new object[] {
            "Titular",
            "Suplente"});
            this.Situacion3CB.Location = new System.Drawing.Point(80, 191);
            this.Situacion3CB.Name = "Situacion3CB";
            this.Situacion3CB.Size = new System.Drawing.Size(68, 21);
            this.Situacion3CB.TabIndex = 279;
            // 
            // Situacion2CB
            // 
            this.Situacion2CB.FormattingEnabled = true;
            this.Situacion2CB.Items.AddRange(new object[] {
            "Titular",
            "Suplente"});
            this.Situacion2CB.Location = new System.Drawing.Point(80, 154);
            this.Situacion2CB.Name = "Situacion2CB";
            this.Situacion2CB.Size = new System.Drawing.Size(68, 21);
            this.Situacion2CB.TabIndex = 282;
            this.Situacion2CB.Text = "Suplente";
            // 
            // Suplente8CB
            // 
            this.Suplente8CB.FormattingEnabled = true;
            this.Suplente8CB.Location = new System.Drawing.Point(154, 369);
            this.Suplente8CB.Name = "Suplente8CB";
            this.Suplente8CB.Size = new System.Drawing.Size(97, 21);
            this.Suplente8CB.TabIndex = 281;
            // 
            // Suplente7CB
            // 
            this.Suplente7CB.FormattingEnabled = true;
            this.Suplente7CB.Location = new System.Drawing.Point(154, 335);
            this.Suplente7CB.Name = "Suplente7CB";
            this.Suplente7CB.Size = new System.Drawing.Size(97, 21);
            this.Suplente7CB.TabIndex = 287;
            // 
            // Suplente6CB
            // 
            this.Suplente6CB.FormattingEnabled = true;
            this.Suplente6CB.Location = new System.Drawing.Point(154, 297);
            this.Suplente6CB.Name = "Suplente6CB";
            this.Suplente6CB.Size = new System.Drawing.Size(97, 21);
            this.Suplente6CB.TabIndex = 293;
            // 
            // Suplente5CB
            // 
            this.Suplente5CB.FormattingEnabled = true;
            this.Suplente5CB.Location = new System.Drawing.Point(154, 261);
            this.Suplente5CB.Name = "Suplente5CB";
            this.Suplente5CB.Size = new System.Drawing.Size(97, 21);
            this.Suplente5CB.TabIndex = 292;
            // 
            // Suplente4CB
            // 
            this.Suplente4CB.FormattingEnabled = true;
            this.Suplente4CB.Location = new System.Drawing.Point(154, 224);
            this.Suplente4CB.Name = "Suplente4CB";
            this.Suplente4CB.Size = new System.Drawing.Size(97, 21);
            this.Suplente4CB.TabIndex = 295;
            // 
            // Suplente3CB
            // 
            this.Suplente3CB.FormattingEnabled = true;
            this.Suplente3CB.Location = new System.Drawing.Point(154, 191);
            this.Suplente3CB.Name = "Suplente3CB";
            this.Suplente3CB.Size = new System.Drawing.Size(97, 21);
            this.Suplente3CB.TabIndex = 294;
            // 
            // Suplente2CB
            // 
            this.Suplente2CB.FormattingEnabled = true;
            this.Suplente2CB.Location = new System.Drawing.Point(154, 154);
            this.Suplente2CB.Name = "Suplente2CB";
            this.Suplente2CB.Size = new System.Drawing.Size(97, 21);
            this.Suplente2CB.TabIndex = 289;
            this.Suplente2CB.Text = "Jorge Marconi";
            // 
            // Suplente1CB
            // 
            this.Suplente1CB.FormattingEnabled = true;
            this.Suplente1CB.Location = new System.Drawing.Point(154, 117);
            this.Suplente1CB.Name = "Suplente1CB";
            this.Suplente1CB.Size = new System.Drawing.Size(97, 21);
            this.Suplente1CB.TabIndex = 288;
            // 
            // Situacion1CB
            // 
            this.Situacion1CB.FormattingEnabled = true;
            this.Situacion1CB.Items.AddRange(new object[] {
            "Titular",
            "Suplente"});
            this.Situacion1CB.Location = new System.Drawing.Point(80, 117);
            this.Situacion1CB.Name = "Situacion1CB";
            this.Situacion1CB.Size = new System.Drawing.Size(68, 21);
            this.Situacion1CB.TabIndex = 291;
            this.Situacion1CB.Text = "Titular";
            // 
            // AsigCB1
            // 
            this.AsigCB1.FormattingEnabled = true;
            this.AsigCB1.Location = new System.Drawing.Point(266, 117);
            this.AsigCB1.Name = "AsigCB1";
            this.AsigCB1.Size = new System.Drawing.Size(121, 21);
            this.AsigCB1.TabIndex = 290;
            this.AsigCB1.Text = "matematica";
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(419, 27);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(26, 13);
            this.Label10.TabIndex = 278;
            this.Label10.Text = "DNI";
            // 
            // Label28
            // 
            this.Label28.AutoSize = true;
            this.Label28.Location = new System.Drawing.Point(578, 468);
            this.Label28.Name = "Label28";
            this.Label28.Size = new System.Drawing.Size(122, 13);
            this.Label28.TabIndex = 276;
            this.Label28.Text = "Porcentaje Inasistencias";
            // 
            // Label27
            // 
            this.Label27.AutoSize = true;
            this.Label27.Location = new System.Drawing.Point(578, 445);
            this.Label27.Name = "Label27";
            this.Label27.Size = new System.Drawing.Size(114, 13);
            this.Label27.TabIndex = 276;
            this.Label27.Text = "Porcentaje Asistencias";
            // 
            // TextBox11
            // 
            this.TextBox11.Location = new System.Drawing.Point(754, 465);
            this.TextBox11.Name = "TextBox11";
            this.TextBox11.Size = new System.Drawing.Size(56, 20);
            this.TextBox11.TabIndex = 275;
            this.TextBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TextBox8
            // 
            this.TextBox8.Location = new System.Drawing.Point(754, 438);
            this.TextBox8.Name = "TextBox8";
            this.TextBox8.Size = new System.Drawing.Size(56, 20);
            this.TextBox8.TabIndex = 275;
            this.TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(604, 415);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(56, 13);
            this.Label9.TabIndex = 276;
            this.Label9.Text = "TOTALES";
            // 
            // totalTXT
            // 
            this.totalTXT.Location = new System.Drawing.Point(754, 415);
            this.totalTXT.Name = "totalTXT";
            this.totalTXT.Size = new System.Drawing.Size(56, 20);
            this.totalTXT.TabIndex = 275;
            this.totalTXT.Text = "2";
            this.totalTXT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(452, 80);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(44, 13);
            this.Label8.TabIndex = 274;
            this.Label8.Text = "Division";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(396, 80);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(39, 13);
            this.Label7.TabIndex = 273;
            this.Label7.Text = "Cursos";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(751, 80);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(60, 13);
            this.Label6.TabIndex = 269;
            this.Label6.Text = "Cant Horas";
            // 
            // Label24
            // 
            this.Label24.AutoSize = true;
            this.Label24.Location = new System.Drawing.Point(162, 80);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(70, 13);
            this.Label24.TabIndex = 271;
            this.Label24.Text = "Suplente de..";
            // 
            // Label22
            // 
            this.Label22.AutoSize = true;
            this.Label22.Location = new System.Drawing.Point(88, 80);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(51, 13);
            this.Label22.TabIndex = 272;
            this.Label22.Text = "Situación";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(293, 80);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(62, 13);
            this.Label5.TabIndex = 270;
            this.Label5.Text = "Asignaturas";
            // 
            // DNIsbTXT
            // 
            this.DNIsbTXT.Location = new System.Drawing.Point(451, 24);
            this.DNIsbTXT.Name = "DNIsbTXT";
            this.DNIsbTXT.Size = new System.Drawing.Size(100, 20);
            this.DNIsbTXT.TabIndex = 268;
            // 
            // Label21
            // 
            this.Label21.AutoSize = true;
            this.Label21.Location = new System.Drawing.Point(61, 374);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(13, 13);
            this.Label21.TabIndex = 263;
            this.Label21.Text = "8";
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Location = new System.Drawing.Point(61, 338);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(13, 13);
            this.Label20.TabIndex = 262;
            this.Label20.Text = "7";
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Location = new System.Drawing.Point(61, 303);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(13, 13);
            this.Label19.TabIndex = 264;
            this.Label19.Text = "6";
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.Location = new System.Drawing.Point(61, 266);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(13, 13);
            this.Label18.TabIndex = 266;
            this.Label18.Text = "5";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(61, 233);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(13, 13);
            this.Label4.TabIndex = 265;
            this.Label4.Text = "4";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(61, 195);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(13, 13);
            this.Label3.TabIndex = 261;
            this.Label3.Text = "3";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(61, 159);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(13, 13);
            this.Label2.TabIndex = 260;
            this.Label2.Text = "2";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(61, 122);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(13, 13);
            this.Label1.TabIndex = 259;
            this.Label1.Text = "1";
            // 
            // Modificacion_de_presentismo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 513);
            this.Controls.Add(this.SecuBasicaGBOX);
            this.Name = "Modificacion_de_presentismo";
            this.Text = "Modificacion_de_presentismo";
            this.SecuBasicaGBOX.ResumeLayout(false);
            this.SecuBasicaGBOX.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.GroupBox SecuBasicaGBOX;
        internal System.Windows.Forms.Button Button3;
        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.TextBox TextBox19;
        internal System.Windows.Forms.TextBox TextBox26;
        internal System.Windows.Forms.TextBox TextBox10;
        internal System.Windows.Forms.TextBox TextBox18;
        internal System.Windows.Forms.TextBox TextBox25;
        internal System.Windows.Forms.TextBox TextBox9;
        internal System.Windows.Forms.TextBox TextBox17;
        internal System.Windows.Forms.TextBox TextBox24;
        internal System.Windows.Forms.TextBox TextBox7;
        internal System.Windows.Forms.TextBox TextBox16;
        internal System.Windows.Forms.TextBox TextBox23;
        internal System.Windows.Forms.TextBox TextBox6;
        internal System.Windows.Forms.TextBox TextBox15;
        internal System.Windows.Forms.TextBox TextBox22;
        internal System.Windows.Forms.TextBox TextBox5;
        internal System.Windows.Forms.TextBox TextBox14;
        internal System.Windows.Forms.TextBox TextBox21;
        internal System.Windows.Forms.TextBox TextBox4;
        internal System.Windows.Forms.TextBox TextBox13;
        internal System.Windows.Forms.TextBox TextBox20;
        internal System.Windows.Forms.TextBox TextBox2;
        internal System.Windows.Forms.TextBox TextBox12;
        internal System.Windows.Forms.TextBox TextBox3;
        internal System.Windows.Forms.TextBox TextBox1;
        internal System.Windows.Forms.ComboBox AporteCB8;
        internal System.Windows.Forms.ComboBox AporteCB7;
        internal System.Windows.Forms.ComboBox AporteCB6;
        internal System.Windows.Forms.ComboBox AporteCB5;
        internal System.Windows.Forms.ComboBox ComboBox8;
        internal System.Windows.Forms.ComboBox CategCB8;
        internal System.Windows.Forms.ComboBox ComboBox7;
        internal System.Windows.Forms.ComboBox CategCB7;
        internal System.Windows.Forms.ComboBox ComboBox6;
        internal System.Windows.Forms.ComboBox CategCB6;
        internal System.Windows.Forms.ComboBox ComboBox5;
        internal System.Windows.Forms.ComboBox CategCB5;
        internal System.Windows.Forms.ComboBox HrFinCB8;
        internal System.Windows.Forms.ComboBox HrFinCB7;
        internal System.Windows.Forms.ComboBox HrFinCB6;
        internal System.Windows.Forms.ComboBox HrFinCB5;
        internal System.Windows.Forms.ComboBox HrIniCB8;
        internal System.Windows.Forms.ComboBox HrIniCB7;
        internal System.Windows.Forms.ComboBox HrIniCB6;
        internal System.Windows.Forms.ComboBox HrIniCB5;
        internal System.Windows.Forms.ComboBox diaCB8;
        internal System.Windows.Forms.ComboBox diaCB7;
        internal System.Windows.Forms.ComboBox diaCB6;
        internal System.Windows.Forms.ComboBox diaCB5;
        internal System.Windows.Forms.ComboBox CantHrsCB8;
        internal System.Windows.Forms.ComboBox CantHrsCB7;
        internal System.Windows.Forms.ComboBox CantHrsCB6;
        internal System.Windows.Forms.ComboBox CantHrsCB5;
        internal System.Windows.Forms.ComboBox DivisionCB8;
        internal System.Windows.Forms.ComboBox DivisionCB7;
        internal System.Windows.Forms.ComboBox DivisionCB6;
        internal System.Windows.Forms.ComboBox DivisionCB5;
        internal System.Windows.Forms.ComboBox Curso8CB;
        internal System.Windows.Forms.ComboBox Curso7CB;
        internal System.Windows.Forms.ComboBox Curso6CB;
        internal System.Windows.Forms.ComboBox Curso5CB;
        internal System.Windows.Forms.ComboBox AsigCB8;
        internal System.Windows.Forms.ComboBox AsigCB7;
        internal System.Windows.Forms.ComboBox AsigCB6;
        internal System.Windows.Forms.ComboBox AsigCB5;
        internal System.Windows.Forms.ComboBox AporteCB4;
        internal System.Windows.Forms.ComboBox AporteCB3;
        internal System.Windows.Forms.ComboBox AporteCB2;
        internal System.Windows.Forms.ComboBox AporteCB1;
        internal System.Windows.Forms.Label Label29;
        internal System.Windows.Forms.Label Label23;
        internal System.Windows.Forms.Label Label25;
        internal System.Windows.Forms.Label Label17;
        internal System.Windows.Forms.ComboBox ComboBox4;
        internal System.Windows.Forms.ComboBox CategCB4;
        internal System.Windows.Forms.ComboBox ComboBox3;
        internal System.Windows.Forms.ComboBox CategCB3;
        internal System.Windows.Forms.ComboBox ComboBox2;
        internal System.Windows.Forms.ComboBox CategCB2;
        internal System.Windows.Forms.ComboBox ComboBox1;
        internal System.Windows.Forms.ComboBox CategCB1;
        internal System.Windows.Forms.Label Label26;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.TextBox ApellidoTXT;
        internal System.Windows.Forms.TextBox NombreTXT;
        internal System.Windows.Forms.ComboBox HrFinCB4;
        internal System.Windows.Forms.ComboBox HrFinCB3;
        internal System.Windows.Forms.ComboBox HrFinCB2;
        internal System.Windows.Forms.ComboBox HrFinCB1;
        internal System.Windows.Forms.ComboBox HrIniCB4;
        internal System.Windows.Forms.ComboBox HrIniCB3;
        internal System.Windows.Forms.ComboBox HrIniCB2;
        internal System.Windows.Forms.ComboBox HrIniCB1;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.ComboBox diaCB4;
        internal System.Windows.Forms.ComboBox diaCB3;
        internal System.Windows.Forms.ComboBox DiaCB2;
        internal System.Windows.Forms.ComboBox diaCB1;
        internal System.Windows.Forms.ComboBox CantHrsCB4;
        internal System.Windows.Forms.ComboBox CantHrsCB3;
        internal System.Windows.Forms.ComboBox CantHrsCB2;
        internal System.Windows.Forms.ComboBox CantHrsCB1;
        internal System.Windows.Forms.ComboBox DivisionCB4;
        internal System.Windows.Forms.ComboBox DivisionCB3;
        internal System.Windows.Forms.ComboBox DivisionCB2;
        internal System.Windows.Forms.ComboBox DivisionCB1;
        internal System.Windows.Forms.ComboBox Curso4CB;
        internal System.Windows.Forms.ComboBox Curso3CB;
        internal System.Windows.Forms.ComboBox Curso2CB;
        internal System.Windows.Forms.ComboBox Curso1CB;
        internal System.Windows.Forms.ComboBox AsigCB4;
        internal System.Windows.Forms.ComboBox AsigCB3;
        internal System.Windows.Forms.ComboBox AsigCB2;
        internal System.Windows.Forms.ComboBox Situacion8CB;
        internal System.Windows.Forms.ComboBox Situacion7CB;
        internal System.Windows.Forms.ComboBox Situacion6CB;
        internal System.Windows.Forms.ComboBox Situacion5CB;
        internal System.Windows.Forms.ComboBox Situacion4CB;
        internal System.Windows.Forms.ComboBox Situacion3CB;
        internal System.Windows.Forms.ComboBox Situacion2CB;
        internal System.Windows.Forms.ComboBox Suplente8CB;
        internal System.Windows.Forms.ComboBox Suplente7CB;
        internal System.Windows.Forms.ComboBox Suplente6CB;
        internal System.Windows.Forms.ComboBox Suplente5CB;
        internal System.Windows.Forms.ComboBox Suplente4CB;
        internal System.Windows.Forms.ComboBox Suplente3CB;
        internal System.Windows.Forms.ComboBox Suplente2CB;
        internal System.Windows.Forms.ComboBox Suplente1CB;
        internal System.Windows.Forms.ComboBox Situacion1CB;
        internal System.Windows.Forms.ComboBox AsigCB1;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Label Label28;
        internal System.Windows.Forms.Label Label27;
        internal System.Windows.Forms.TextBox TextBox11;
        internal System.Windows.Forms.TextBox TextBox8;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.TextBox totalTXT;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label24;
        internal System.Windows.Forms.Label Label22;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.TextBox DNIsbTXT;
        internal System.Windows.Forms.Label Label21;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.Label Label18;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;

    }
}