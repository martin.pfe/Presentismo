﻿namespace Presentismo_2015
{
    partial class editUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows myCustomForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_SchoolEmail2 = new System.Windows.Forms.TextBox();
            this.txt_SchoolEmail = new System.Windows.Forms.TextBox();
            this.lbl_SchoolEmail2 = new System.Windows.Forms.Label();
            this.lbl_SchoolEmail = new System.Windows.Forms.Label();
            this.txt_PersonalEmail2 = new System.Windows.Forms.TextBox();
            this.txt_PersonalEMail = new System.Windows.Forms.TextBox();
            this.txt_NewPassword2 = new System.Windows.Forms.TextBox();
            this.txt_NewPassword = new System.Windows.Forms.TextBox();
            this.txt_OldPassword = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Image = global::Presentismo_2015.Properties.Resources.pencil;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.Location = new System.Drawing.Point(275, 342);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 59);
            this.button1.TabIndex = 0;
            this.button1.Text = "Editar datos";
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Contraseña actual:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Nueva contraseña:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txt_SchoolEmail2);
            this.groupBox1.Controls.Add(this.txt_SchoolEmail);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.lbl_SchoolEmail2);
            this.groupBox1.Controls.Add(this.lbl_SchoolEmail);
            this.groupBox1.Controls.Add(this.txt_PersonalEmail2);
            this.groupBox1.Controls.Add(this.txt_PersonalEMail);
            this.groupBox1.Controls.Add(this.txt_NewPassword2);
            this.groupBox1.Controls.Add(this.txt_NewPassword);
            this.groupBox1.Controls.Add(this.txt_OldPassword);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(377, 407);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ingrese a continuación los datos que desea modificar";
            // 
            // txt_SchoolEmail2
            // 
            this.txt_SchoolEmail2.Location = new System.Drawing.Point(190, 208);
            this.txt_SchoolEmail2.Name = "txt_SchoolEmail2";
            this.txt_SchoolEmail2.Size = new System.Drawing.Size(140, 20);
            this.txt_SchoolEmail2.TabIndex = 15;
            // 
            // txt_SchoolEmail
            // 
            this.txt_SchoolEmail.Location = new System.Drawing.Point(190, 180);
            this.txt_SchoolEmail.Name = "txt_SchoolEmail";
            this.txt_SchoolEmail.Size = new System.Drawing.Size(140, 20);
            this.txt_SchoolEmail.TabIndex = 14;
            // 
            // lbl_SchoolEmail2
            // 
            this.lbl_SchoolEmail2.AutoSize = true;
            this.lbl_SchoolEmail2.Location = new System.Drawing.Point(27, 215);
            this.lbl_SchoolEmail2.Name = "lbl_SchoolEmail2";
            this.lbl_SchoolEmail2.Size = new System.Drawing.Size(133, 13);
            this.lbl_SchoolEmail2.TabIndex = 13;
            this.lbl_SchoolEmail2.Text = "Repita el email del colegio:";
            // 
            // lbl_SchoolEmail
            // 
            this.lbl_SchoolEmail.AutoSize = true;
            this.lbl_SchoolEmail.Location = new System.Drawing.Point(27, 187);
            this.lbl_SchoolEmail.Name = "lbl_SchoolEmail";
            this.lbl_SchoolEmail.Size = new System.Drawing.Size(123, 13);
            this.lbl_SchoolEmail.TabIndex = 12;
            this.lbl_SchoolEmail.Text = "Nuevo email del colegio:";
            // 
            // txt_PersonalEmail2
            // 
            this.txt_PersonalEmail2.Location = new System.Drawing.Point(190, 151);
            this.txt_PersonalEmail2.Name = "txt_PersonalEmail2";
            this.txt_PersonalEmail2.Size = new System.Drawing.Size(140, 20);
            this.txt_PersonalEmail2.TabIndex = 11;
            // 
            // txt_PersonalEMail
            // 
            this.txt_PersonalEMail.Location = new System.Drawing.Point(190, 123);
            this.txt_PersonalEMail.Name = "txt_PersonalEMail";
            this.txt_PersonalEMail.Size = new System.Drawing.Size(140, 20);
            this.txt_PersonalEMail.TabIndex = 10;
            // 
            // txt_NewPassword2
            // 
            this.txt_NewPassword2.Location = new System.Drawing.Point(190, 95);
            this.txt_NewPassword2.Name = "txt_NewPassword2";
            this.txt_NewPassword2.PasswordChar = '°';
            this.txt_NewPassword2.Size = new System.Drawing.Size(140, 20);
            this.txt_NewPassword2.TabIndex = 9;
            // 
            // txt_NewPassword
            // 
            this.txt_NewPassword.Location = new System.Drawing.Point(190, 64);
            this.txt_NewPassword.Name = "txt_NewPassword";
            this.txt_NewPassword.PasswordChar = '°';
            this.txt_NewPassword.Size = new System.Drawing.Size(140, 20);
            this.txt_NewPassword.TabIndex = 8;
            // 
            // txt_OldPassword
            // 
            this.txt_OldPassword.Location = new System.Drawing.Point(190, 38);
            this.txt_OldPassword.Name = "txt_OldPassword";
            this.txt_OldPassword.PasswordChar = '°';
            this.txt_OldPassword.Size = new System.Drawing.Size(140, 20);
            this.txt_OldPassword.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 158);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Repita su email:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Nuevo  email:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Repita su nueva contraseña:";
            // 
            // editUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(401, 431);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Name = "editUser";
            this.Text = "Editar usuario";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_PersonalEmail2;
        private System.Windows.Forms.TextBox txt_PersonalEMail;
        private System.Windows.Forms.TextBox txt_NewPassword2;
        private System.Windows.Forms.TextBox txt_NewPassword;
        private System.Windows.Forms.TextBox txt_OldPassword;
        private System.Windows.Forms.TextBox txt_SchoolEmail2;
        private System.Windows.Forms.TextBox txt_SchoolEmail;
        private System.Windows.Forms.Label lbl_SchoolEmail2;
        private System.Windows.Forms.Label lbl_SchoolEmail;
    }
}