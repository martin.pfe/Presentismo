﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Dapper;

namespace Presentismo_2015
{
    public partial class AltaPersonal : myCustomForm
    {
        Employee logedEmployee;
        public AltaPersonal(Employee actual)
        {
            InitializeComponent();
            logedEmployee = actual;
            int accessType = logedEmployee.accessType;

            if (accessType != 1)
            {
                labelAccessType.Visible = false;
                employeeTypeCb.Visible  = false;
                redLabelAccessType.Visible = false;
            }
            DNITxt.Focus();
        }

        private void GroupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
        private void AltaPersonal_Load(object sender, EventArgs e)
        {

        }
        public void AgregarBT_Click(object sender, EventArgs e)
        {
            string DNI          = DNITxt.Text;
            string name         = nameTxt.Text;
            string lastName     = lastNameTxt.Text;
            string dateEmployee = dateDTP.Value.Date.ToString();
            string address      = addressTxt.Text;
            string city         = cityTxt.Text;
            string stateStr     = stateTxt.Text;
            string zipCode      = zipCodeTxt.Text;
            string country      = countryTxt.Text;
            string phone        = phoneTxt.Text;
            string cellPhone    = cellPhoneTxt.Text;
            string schoolMail   = schoolMailTxt.Text;
            string personalMail = personalMailTxt.Text;
            string legajo       = legajoTxt.Text;
            string ingressDateStr = ingressDate.Text;
            int employeeTypeID = 0;
            int userID          = logedEmployee.employeeID;
            
            if (logedEmployee.accessType == 1) { 
                 employeeTypeID = employeeTypeCb.SelectedIndex;
                //Le sumamos un uno porque el index empieza en 0, villero pero rápido
                employeeTypeID = employeeTypeID + 1;
            }
            else{
                //Por default siempre levantamos usuario sin acceso (profesor ponele)
                employeeTypeID = 3;
            }

            if (DNI == "")
            {
                MessageBox.Show("Por favor, revise el campo DNI");
                return;
            }

            if (name == "")
            {
                MessageBox.Show("Por favor, revise el campo de nombre");
                return;
            }

            if (lastName == "")
            {
                MessageBox.Show("Por favor, revise el campo de apellido");
                return;
            }

            if (dateEmployee == "")
            {
                MessageBox.Show("Por favor, revise el campo de fecha de nacimiento");
                return;
            }

            if (address == "")
            {
                MessageBox.Show("Por favor, revise el campo Dirección");
                return;
            }


            if (city == "")
            {
                MessageBox.Show("Por favor, revise el campo Localidad");
                return;
            }

            if (stateStr == "")
            {
                MessageBox.Show("Por favor, revise el campo Provincia");
                return;
            }

            if (zipCode == "")
            {
                MessageBox.Show("Por favor, revise el campo Código Postal");
                return;
            }

            if (country == "")
            {
                MessageBox.Show("Por favor, revise el campo Nacionalidad");
                return;
            }

            if (phone == "")
            {
                MessageBox.Show("Por favor, revise el campo Teléfono");
                return;
            }

            if (cellPhone == "")
            {
                MessageBox.Show("Por favor, revise el campo Celular");
                return;
            }

            if (schoolMail == "")
            {
                MessageBox.Show("Por favor, revise el campo Mail colegio");
                return;
            }

            if (personalMail == "")
            {
                MessageBox.Show("Por favor, revise el campo Mail personal");
                return;
            }

            if (legajo == "")
            {
                MessageBox.Show("Por favor, revise el campo Legajo");
                return;
            }
            

            //string connectionString = "Server=tcp:oer8py276v.database.windows.net,1433;Database=Presentismo;User ID=proyecto2015@oer8py276v;Password=Colegiojuan23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
            if (DNI != "" && name != "" && lastName != "" && dateEmployee != "" && address != "" && city != "" && stateStr != ""
            && zipCode != "" && country != "" && phone != "" && cellPhone != "" && schoolMail != "" && personalMail != "" && legajo != "" && employeeTypeID > 0)
            {

                try
                {
                    var spParams = new DynamicParameters();
                    spParams.Add("@EmployeeDni", DNI, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                    spParams.Add("@name", name, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                    spParams.Add("@lastName", lastName, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                    spParams.Add("@birthdate", dateEmployee, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                    spParams.Add("@birthCountry", country, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                    spParams.Add("@homePhone", phone, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                    spParams.Add("@cellPhone", cellPhone, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                    spParams.Add("@personalMail", personalMail, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                    spParams.Add("@schoolMail", schoolMail, dbType: DbType.String, direction: ParameterDirection.Input, size: 30);
                    spParams.Add("@hasLicense", false, dbType: DbType.Boolean, direction: ParameterDirection.Input);
                    spParams.Add("@userID", userID, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    spParams.Add("@employeeAddress", address, dbType: DbType.String, direction: ParameterDirection.Input, size: 100);
                    spParams.Add("@employeeLocality", city, dbType: DbType.String, direction: ParameterDirection.Input, size: 100);
                    spParams.Add("@employeeState", stateStr, dbType: DbType.String, direction: ParameterDirection.Input, size: 100);
                    spParams.Add("@employeeZipCode", zipCode, dbType: DbType.String, direction: ParameterDirection.Input, size: 6);
                    spParams.Add("@legajo", legajo, dbType: DbType.String, direction: ParameterDirection.Input, size: 15);
                    spParams.Add("@ingressDate", ingressDateStr, dbType: DbType.String, direction: ParameterDirection.Input, size: 10);
                    spParams.Add("@employeeTypeID", employeeTypeID, dbType: DbType.String, direction: ParameterDirection.Input, size: 100);

                    spParams.Add("@employeeID", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    spParams.Add("@errorDesc", dbType: DbType.String, direction: ParameterDirection.Output, size: 30);
                    spParams.Add("@password", dbType: DbType.String, direction: ParameterDirection.Output, size: 30);
                    spParams.Add("@RETURN_VALUE", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

                    using (var sqlConnection = new SqlConnection(Conection.connectionString))
                    {

                        sqlConnection.Open();

                        sqlConnection.Execute("SP_Employee_Add", spParams, commandType: CommandType.StoredProcedure);
                        int RC              = spParams.Get<int>("@RETURN_VALUE");
                        int employeeIDINT   = spParams.Get<int>("@employeeID");
                        string password     = spParams.Get<string>("@password");
                        string errorDesc    = spParams.Get<string>("@errorDesc");

                        sqlConnection.Close();
                        if (RC != 1)
                        {
                            MessageBox.Show(String.Format("Error: {0} - Error N° :{1}", errorDesc, RC.ToString()));
                        }
                        else
                        {
                            DNITxt.Text             = "";
                            nameTxt.Text            = "";
                            lastNameTxt.Text        = "";
                            dateDTP.Value           = DateTime.Now;
                            addressTxt.Text         = "";
                            cityTxt.Text            = "";
                            stateTxt.Text           = "";
                            zipCodeTxt.Text         = "";
                            countryTxt.Text         = "";
                            phoneTxt.Text           = "";
                            cellPhoneTxt.Text       = "";
                            schoolMailTxt.Text      = "";
                            personalMailTxt.Text    = "";
                            legajoTxt.Text          = "";

                            string employeeName = lastName + ", " + name;
                            MessageBox.Show("El empleado ha sido dado de alta satisfactoriamente! A continuación podrá agregar los cargos del mismo.");
                            Cargos Cargos = new Cargos(employeeIDINT, logedEmployee, employeeName);
                            Cargos.Show();
                            this.Hide();
                            this.Close();
                        }

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    MessageBox.Show("Ha ocurrido un error intentando dar de alta el empleado. Por favor contactese con un administrador.");
                }
            }
            else
                MessageBox.Show("Por favor complete todos los campos.");
        }

        private void ocupationCmb_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CerrarBT_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void addPosition_Click(object sender, EventArgs e)
        {
            //string DNI = DNITxt.Text;
            //Cargos Cargos = new Cargos(DNI);
            //Cargos.Show();
        }

        private void cityCmb_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void zipCodeTxt_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(zipCodeTxt.Text, "  ^ [0-9]"))
            {
                zipCodeTxt.Text = "";
            }
        }
        private void zipCodeTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void legajoTxt_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(legajoTxt.Text, "  ^ [0-9]"))
            {
                legajoTxt.Text = "";
            }
        }
        private void legajoTxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void DNITxt_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(DNITxt.Text, "  ^ [0-9]"))
            {
                DNITxt.Text = "";
            }
        }
        private void DNITxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void employeeTypeCb_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void schoolMailTxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void stateTxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void cerrarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
